#version 420 core

layout(location = 0) out vec4 color_;

layout(location = 0) in vec2 _uv;
layout(location = 1) in vec4 _color;
layout(location = 2) in vec2 _tiling;
layout(location = 3) in float _tex;

layout(binding = 1) uniform sampler2D tex_[32];

void main()
{
    int id = int(_tex + 0.1); // Addition ensures that the input is never something like x.99... which will drop down to x
    color_ = texture(tex_[id], _uv * _tiling) * _color;
}