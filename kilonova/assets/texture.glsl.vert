#version 420 core

layout(location = 0) in vec3 position_;
layout(location = 1) in vec2 uv_;
layout(location = 2) in vec4 color_;
layout(location = 3) in vec2 tiling_;
layout(location = 4) in float tex_;

layout(location = 0) out vec2 _uv;
layout(location = 1) out vec4 _color;
layout(location = 2) out vec2 _tiling;
layout(location = 3) out float _tex;

layout(std140, binding = 0) uniform Globals
{
    mat4 viewProjection_;
};

void main()
{
    gl_Position = viewProjection_ * vec4(position_, 1);

    _uv = uv_;
    _color = color_;
    _tiling = tiling_;
    _tex = tex_;
}