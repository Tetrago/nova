#pragma once

#include <fstream>
#include <filesystem>
#include <nova/nova.h>

namespace nova
{
	class Project
	{
	public:
		Project(const std::filesystem::path& path);

		void setup(Scene& scene);
	private:
		const std::filesystem::path path_;
		Json json_;
		Box<Library> lib_;
	};
}