#include "editor.h"

#include <cmath>

namespace nova
{
	Editor::Editor()
	{
		push(viewport_ = make_ref<Viewport>());
	}

	void Editor::onGui()
	{
		NOVA_DIAG_FUNCTION();

		auto&& viewport = Gui::getMainViewport();
		auto&& xy = viewport.position();

		auto x = xy.x;
		auto y = xy.y;

		auto [w, h] = Program::get().window().size();

		{
			NOVA_DIAG_SCOPE("Dockspace");

			Gui::setNextWindowPosition(x, y);
			Gui::setNextWindowSize(w, h);
			Gui::setNextWindowViewport(viewport);

			Gui::pushStyleVar(Gui::StyleVar::WindowRounding, 0);
			Gui::pushStyleVar(Gui::StyleVar::WindowRounding, 0);
			Gui::pushStyleVar(Gui::StyleVar::WindowPadding, 0, 0);

			Gui::begin("Dockspace", nullptr, Gui::NoDocking | Gui::NoResize | Gui::NoMove | Gui::NoTitleBar | Gui::NoNavFocus | Gui::NoBringToFront | Gui::MenuBar);
			Gui::popStyleVar(3);

			if(Gui::beginMenuBar())
			{
				if(Gui::beginMenu("File"))
				{
					if(Gui::menuItem("Open...", "Ctrl+O"))
					{
						Gui::openFile("Nova Scene (*.scn)\0*.scn\0");
					}

					Gui::endMenu();
				}

				Gui::endMenuBar();
			}

			Gui::dockSpace("Dockspace");

			{
				NOVA_DIAG_SCOPE("Tools");

				for(const auto& tool : tools_)
				{
					tool->onGui();
				}

				Inspector::main().onGui();
			}

			Gui::end();
		}

		{
			NOVA_DIAG_SCOPE("Info Window");

			if(Gui::begin("Info"))
			{
				Gui::text("Graphics");
				Gui::separator();
				Gui::text("Framerate: %d fps", Time::frameRate());
				Gui::text("Deltatime: %f", Time::deltaTime());

				Gui::newLine();
				Gui::text("Renderer2D");
				Gui::separator();

				const auto& stats = Renderer2D::stats();
				Gui::text("Draw Calls: %d", stats.drawCalls);
				Gui::text("Quad Count: %d", stats.quadCount);
			}

			Gui::end();
		}

		{
			NOVA_DIAG_SCOPE("Exit Prompt");

			if(showExitPrompt_)
			{
				Gui::openPopup("Exit");
			}

			Gui::setNextWindowPosition(x + w * 0.5f, y + h * 0.5f, 0.5f, 0.5f);

			if(Gui::beginModal("Exit", Gui::AutoResize))
			{
				Gui::text("Are you sure?");
				Gui::separator();

				if(Gui::button("OK", 120))
				{
					exitPromptAccept_ = true;
					Program::get().stop();
				}

				Gui::sameLine();

				if(Gui::button("Cancel", 120))
				{
					showExitPrompt_ = false;
					Gui::closePopup();
				}

				Gui::endPopup();
			}
		}
	}

	bool Editor::showExitPrompt()
	{
		showExitPrompt_ = true;
		return exitPromptAccept_;
	}
}