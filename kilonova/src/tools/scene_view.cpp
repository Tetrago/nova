#include "scene_view.h"

#include <nova/nova.h>

#include "tools/inspector.h"

namespace nova
{
	SceneView::SceneView(Scene* scene)
		: scene_(scene)
	{}

	void SceneView::onGui()
	{
		NOVA_DIAG_FUNCTION();

		auto& obj = Inspector::main().target();
		bool currentlySelected = obj.type == Inspector::Object::Type::Node;

		if(Gui::begin("Scene View"))
		{
			draw(scene_->root());

			if(Gui::isMouseDown() && Gui::isHovered())
			{
				obj.type = Inspector::Object::Type::None;
			}
		}

		Gui::end();

		if(nodeWindow_.visible)
		{
			Gui::setNextWindowSize(600, 400, Gui::Condition::First);

			if(Gui::begin("Add Node", &nodeWindow_.visible, Gui::NoDocking | Gui::NoCollapse))
			{
				if(Gui::beginChild("Nodes", 0, Gui::getAvailableSpace().y - 25))
				{
					for(const auto& [name, id] : NodeRegistry::get())
					{
						bool selected = nodeWindow_.selected == name;
						if(Gui::selectable(name.c_str(), &selected))
						{
							nodeWindow_.selected = name;
						}
					}

					Gui::endChild();
				}

				if(Gui::button("Add", Gui::getAvailableSpace().x))
				{
					nodeWindow_.target->addByName(nodeWindow_.selected);
					nodeWindow_.visible = false;
				}
			}

			Gui::end();
		}
	}

	void SceneView::draw(Node* node)
	{
		auto& target = Inspector::main().target();
		bool selected = target.type == Inspector::Object::Type::Node && target.node == node;

		uint32_t flags = Gui::OpenOnArrow | Gui::OpenOnDoubleClick;

		if(node->nodes().size() == 0)
		{
			flags |= Gui::Leaf;
		}

		if(selected)
		{
			flags |= Gui::Selected;
		}

		bool treeFuncCalled = false;
		auto treeFunc = [&]()
		{
			if(treeFuncCalled) return;
			treeFuncCalled = true;

			if(Gui::isItemClicked() || Gui::isItemClicked(1))
			{
				target.type = Inspector::Object::Type::Node;
				target.node = node;
			}

			if(Gui::isItemClicked(1))
			{
				Gui::openPopup("Context Menu");
			}
		};

		if(Gui::tree(node->name().c_str(), flags))
		{
			treeFunc();

			for(Node* n : node->nodes())
			{
				draw(n);
			}

			Gui::popTree();
		}

		treeFunc();

		if(Gui::beginPopup("Context Menu"))
		{
			if(Gui::button("Add"))
			{
				nodeWindow_.visible = !nodeWindow_.visible;
			}

			if(Gui::button("Remove"))
			{
				node->parent()->remove(node);
			}

			Gui::endPopup();
		}
	}
}