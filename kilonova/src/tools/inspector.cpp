#include "inspector.h"

#include <glm/gtc/type_ptr.hpp>

#include <nova/nova.h>

namespace nova
{
	void Inspector::onGui()
	{
		NOVA_DIAG_FUNCTION();

		if(Gui::begin("Inspector"))
		{
			switch(target_.type)
			{
			case Object::Type::Node:
				target_.node->onInspectorDraw();
				break;
			}
		}

		Gui::end();
	}

	Inspector& Inspector::main()
	{
		static Inspector inspector;
		return inspector;
	}
}