#pragma once

#include "tool.h"

namespace nova
{
	class Scene;
	class Inspector;
	class Node;

	class SceneView : public Tool
	{
	public:
		SceneView(Scene* scene);

		void onGui() override;
	private:
		void draw(Node* node);

		Scene* scene_;

		struct
		{
			bool visible = false;
			std::string selected{};
			Node* target;
		} nodeWindow_;
	};
}