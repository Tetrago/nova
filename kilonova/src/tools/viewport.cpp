#include "viewport.h"

#include <sstream>

namespace nova
{
	Viewport::Viewport()
	{
		NOVA_DIAG_FUNCTION();

		buildSelectionString();

		auto& renderDevice = Program::get().renderDevice();

		color_ = renderDevice.createTexture();
		color_->create({ 1280, 720, TextureFormat::Rgb8 });

		depth_ = renderDevice.createTexture();
		depth_->create({ 1280, 720, TextureFormat::Depth });

		FramebufferCreateInfo createInfo{};
		createInfo.attachments =
		{
			{ AttachmentType::Color, color_, 0 },
			{ AttachmentType::Depth, depth_ }
		};

		framebuffer_ = renderDevice.createFrambuffer(createInfo);
	}

	void Viewport::onGui()
	{
		NOVA_DIAG_FUNCTION();

		bool forceResize = false;

		Gui::pushStyleVar(Gui::StyleVar::WindowPadding, 0, 0);
		Gui::pushStyleVar(Gui::StyleVar::WindowBorderSize, 0);

		Gui::begin("Viewport", nullptr, Gui::NoCollapse | Gui::MenuBar | Gui::NoScrollbar);
		Gui::popStyleVar(2);

		if(Gui::beginMenuBar())
		{
			if(Gui::beginMenu("Display"))
			{
				auto prevProfile = selectedProfile_;
				Gui::combo("Aspect Ratio", &selectedProfile_, selectionString_.c_str());
				if(selectedProfile_ != prevProfile)
				{
					forceResize = true;
				}

				Gui::slider("Zoom", &zoom_, { 1, 5 }, "%.1f");

				auto prevCamera = selectedCamera_;
				Gui::combo("Camera", &selectedCamera_, "Orthographic\0Perspective\0");
				if(selectedCamera_ != prevCamera)
				{
					switch(selectedCamera_)
					{
					case 0:
						camera_.orthographic();
						break;
					case 1:
						camera_.perspective();
						break;
					}
				}

				Gui::endMenu();
			}

			Gui::endMenuBar();
		}

		auto&& space = Gui::getAvailableSpace();
		float vw = space.x;
		float vh = space.y;

		bool viewportChanged = lastViewportSpace_.x != vw || lastViewportSpace_.y != vh;
		lastViewportSpace_.x = vw;
		lastViewportSpace_.y = vh;

		if(forceResize || viewportChanged)
		{
			if(vw > 0 && vh > 0)
			{
				resize(vw, vh);
			}
		}
		
		auto cursor = Gui::getCursorPosition();
		auto center = cursor + (Gui::getAvailableSpace() - viewportSize_) * 0.5f;
		Gui::setCursorPosition(center.x, center.y);

		Gui::texture(color_, viewportSize_.x, viewportSize_.y, zoom_);

		Gui::end();
	}

	void Viewport::begin()
	{
		camera_.update();

		auto& ctx = Program::get().window().context();
		ctx.setRenderTarget(*framebuffer_);

		if(screenSize_.x > 0 && screenSize_.y > 0)
		{
			ctx.viewport(0, 0, screenSize_.x, screenSize_.y);
		}
	}

	void Viewport::end()
	{
		Program::get().window().context().clearRenderTarget();
	}

	const glm::mat4 Viewport::matrix() const
	{
		return camera_.matrix();
	}

	void Viewport::resize(float vw, float vh)
	{
		const auto& profile = profiles_[selectedProfile_];

		// --- Screen Size

		glm::vec2 lastScreenSize = screenSize_;

		screenSize_.x = vw;
		screenSize_.y = vh;

		if(profile.width != 0 && profile.height != 0) // A resolution is specified
		{
			screenSize_.x = profile.width;
			screenSize_.y = profile.height;
		}
		else if(profile.aspectRatio != 0) // An aspect ration is specified, but, resolution takes priority
		{
			if(vw / profile.aspectRatio < vh && vh != vw / profile.aspectRatio)
			{
				screenSize_.y = vw / profile.aspectRatio;
			}
			else if(vw != vh * profile.aspectRatio)
			{
				screenSize_.x = vh * profile.aspectRatio;
			}
		}

		// --- Viewport Size

		viewportSize_.x = vw;
		viewportSize_.y = vh;

		if(profile.width != 0 && profile.height != 0) // A resolution is specified
		{
			float targetWidth = std::min(vw, (float)profile.width);
			float targetHeight = std::min(vh, (float)profile.height);

			viewportSize_.x = targetWidth;
			viewportSize_.y = targetHeight;

			if(targetWidth / profile.aspectRatio < targetHeight && targetHeight != targetWidth / profile.aspectRatio)
			{
				viewportSize_.y = targetWidth / profile.aspectRatio;
			}
			else if(targetWidth != targetHeight * profile.aspectRatio)
			{
				viewportSize_.x = targetHeight * profile.aspectRatio;
			}
		}
		else if(profile.aspectRatio != 0) // An aspect ration is specified, but, resolution takes priority
		{
			if(vw / profile.aspectRatio < vh && vh != vw / profile.aspectRatio)
			{
				viewportSize_.y = vw / profile.aspectRatio;
			}
			else if(vw != vh * profile.aspectRatio)
			{
				viewportSize_.x = vh * profile.aspectRatio;
			}
		}

		if(screenSize_ != lastScreenSize)
		{
			color_->create({ (uint32_t)screenSize_.x, (uint32_t)screenSize_.y, TextureFormat::Rgb8 });
			depth_->create({ (uint32_t)screenSize_.x, (uint32_t)screenSize_.y, TextureFormat::Depth });

			camera_.resize((uint32_t)screenSize_.x, (uint32_t)screenSize_.y);
		}
	}

	void Viewport::buildSelectionString()
	{
		std::ostringstream stream;

		for(const auto& profile : profiles_)
		{
			stream << profile.name << '\0';
		}

		selectionString_ = stream.str();
	}
}