#pragma once

#include <string>
#include <vector>
#include <nova/nova.h>
#include <glm/vec2.hpp>

#include "camera.h"
#include "tool.h"

namespace nova
{
	class Viewport : public Tool
	{
	public:
		struct LockProfile
		{
			std::string name;
			uint32_t width = 0;
			uint32_t height = 0;
			float aspectRatio = 0;
		};
	public:
		Viewport();

		void onGui() override;

		void begin();
		void end();

		const glm::mat4 matrix() const;
	private:
		void resize(float vw, float vh);
		void buildSelectionString();

		Ref<Texture> color_;
		Ref<Texture> depth_;
		Ref<Framebuffer> framebuffer_;

		glm::vec2 lastViewportSpace_{};
		glm::vec2 viewportSize_{};
		glm::vec2 screenSize_{};
		float zoom_ = 1;

		int selectedCamera_ = 0;
		int selectedProfile_ = 1;
		std::string selectionString_;

		CameraController camera_{};

		std::vector<LockProfile> profiles_
		{
			{ "None" , 0, 0, 0},
			{ "Unlocked       16:9", 0, 0, 16.0f / 9.0f },
			{ "(1280x720)     16:9", 1280, 720, 16.0f / 9.0f },
			{ "(1920x1080)    16:9", 1920, 1080, 16.0f / 9.0f }
		};
	};
}