#pragma once

#include <nova/nova.h>

#include "tool.h"

namespace nova
{
	class Inspector : public Tool
	{
	public:
		struct Object
		{
			enum class Type
			{
				None,
				Node
			};

			Type type;

			union
			{
				Node* node;
			};

			Object()
				: type(Type::None)
			{}
		};
	public:
		void onGui() override;

		Object& target() { return target_; }

		static Inspector& main();
	private:
		Object target_;
	};
}