#include "project.h"

namespace nova
{
	Project::Project(const std::filesystem::path& path)
		: path_(path)
		, json_(std::ifstream(path / "project.json"))
	{
		AssetPipeline::addResolver<FileResolver>(path);
	}

	void Project::setup(Scene& scene)
	{
		auto lib = path_ / json_.value().at("library").get<std::string>();
		lib_ = make_box<Library>(lib.parent_path(), lib.stem().string());

		if(!*lib_)
		{
			NOVA_PROGRAM_LOG(Log::Error, "Could not load library");
		}
		else
		{
			lib_->get<void(*)()>("register_nodes")();
		}

		std::string sceneLocation = json_.value().at("startup_scene").get<std::string>();
		Json* json = ResourceManager::quickload<Json>(sceneLocation);

		if(!json)
		{
			NOVA_PROGRAM_LOG(Log::Error, "Invalid startup scene");
		}
		else
		{
			scene.load(json->value());
		}
	}
}