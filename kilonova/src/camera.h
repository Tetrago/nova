#pragma once

#include <nova/nova.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace nova
{
	class Camera
	{
	public:
		virtual void update() = 0;

		virtual void resize(uint32_t w, uint32_t h) = 0;
		virtual glm::mat4 matrix() const = 0;
	};

	class OrthographicCamera : public Camera
	{
	public:
		void update() override;

		void resize(uint32_t w, uint32_t h) override;
		glm::mat4 matrix() const override;
	private:
		uint32_t width_ = 0;
		uint32_t height_ = 0;
		float aspectRatio_ = 1;
		double mouseX_ = 0;
		double mouseY_ = 0;
		float zoom_ = 1;
		glm::vec2 pos_{};
	};

	class PerspectiveCamera : public Camera
	{
	public:
		void update() override;

		void resize(uint32_t w, uint32_t h) override;
		glm::mat4 matrix() const override;
	private:
		float aspectRatio_ = 1;
		double mouseX_ = 0;
		double mouseY_ = 0;
		glm::quat rot_{};
		glm::vec3 pos_{};
	};

	class CameraController
	{
	public:
		CameraController();

		void update() { camera_->update(); }
		void resize(uint32_t w, uint32_t h);

		void orthographic() { camera_ = &orthographic_; }
		void perspective() { camera_ = &perspective_; }

		glm::mat4 matrix() const { return camera_->matrix(); }
	private:
		Camera* camera_;
		OrthographicCamera orthographic_{};
		PerspectiveCamera perspective_{};
	};
}