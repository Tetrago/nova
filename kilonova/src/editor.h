#pragma once

#include <vector>
#include <nova/nova.h>

#include "tool.h"
#include "tools/inspector.h"
#include "tools/viewport.h"

namespace nova
{
	class Editor
	{
	public:
		Editor();

		void onGui();
		bool showExitPrompt();

		void push(const Ref<Tool>& tool) { tools_.push_back(tool); }

		Viewport& viewport() { return *viewport_; }
		bool pending() { return showExitPrompt_; }
	private:
		std::vector<Ref<Tool>> tools_;

		Ref<Viewport> viewport_;

		bool showExitPrompt_ = false;
		bool exitPromptAccept_ = false;
	};
}