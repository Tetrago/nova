#pragma once

#include <string>

namespace nova
{
	class Tool
	{
	public:
		virtual ~Tool() {}

		virtual void onGui() {}
	};
}