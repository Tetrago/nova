#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

namespace nova
{
	void OrthographicCamera::update()
	{
		NOVA_DIAG_FUNCTION();

		auto dt = Time::deltaTime();
		auto& mouse = Mouse::get();

		double x = mouse.x();
		double y = mouse.y();

		if(mouse.isButtonDown(2))
		{
			pos_.x -= (x - mouseX_) / (width_ * 0.5f / zoom_ / aspectRatio_);
			pos_.y += (y - mouseY_) / (height_ * 0.5f / zoom_);
		}

		zoom_ -= mouse.scrollDelta() * dt * 10 * zoom_;

		mouseX_ = x;
		mouseY_ = y;
	}

	void OrthographicCamera::resize(uint32_t w, uint32_t h)
	{
		aspectRatio_ = (float)w / h;
		width_ = w;
		height_ = h;
	}

	glm::mat4 OrthographicCamera::matrix() const
	{
		return glm::ortho(-aspectRatio_ * zoom_, aspectRatio_ * zoom_, -zoom_, zoom_)
			* glm::inverse(glm::translate(glm::mat4(1), { pos_.x, pos_.y, 0 }));
	}

	void PerspectiveCamera::update()
	{
		NOVA_DIAG_FUNCTION();

		constexpr float speed = 5;
		
		auto dt = Time::deltaTime();
		auto& keyboard = Keyboard::get();

		if(keyboard.isKeyDown(KeyCode::W))
		{
			pos_ -= rot_ * glm::vec3{ 0, 0, dt * speed };
		}
		else if(keyboard.isKeyDown(KeyCode::S))
		{
			pos_ += rot_ * glm::vec3{ 0, 0, dt * speed };
		}

		if(keyboard.isKeyDown(KeyCode::D))
		{
			pos_ += rot_ * glm::vec3{ dt * speed, 0, 0 };
		}
		else if(keyboard.isKeyDown(KeyCode::A))
		{
			pos_ -= rot_ * glm::vec3{ dt * speed, 0, 0 };
		}

		if(keyboard.isKeyDown(KeyCode::E))
		{
			pos_ += rot_ * glm::vec3{ 0, dt * speed, 0 };
		}
		else if(keyboard.isKeyDown(KeyCode::Q))
		{
			pos_ -= rot_ * glm::vec3{ 0, dt * speed, 0 };
		}

		constexpr float turnSpeed = 10;

		auto& mouse = Mouse::get();

		double x = mouse.x();
		double y = mouse.y();

		if(mouse.isButtonDown(1))
		{
			if(mouseX_ != x)
			{
				glm::vec3 e = glm::eulerAngles(rot_);
				e.y -= glm::radians((x - mouseX_) * dt * turnSpeed);
				rot_ = glm::quat(e);
			}

			if(mouseY_ != y)
			{
				glm::vec3 e = glm::eulerAngles(rot_);
				e.x -= glm::radians((y - mouseY_) * dt * turnSpeed);
				rot_ = glm::quat(e);
			}
		}

		mouseX_ = x;
		mouseY_ = y;
	}

	void PerspectiveCamera::resize(uint32_t w, uint32_t h)
	{
		if(w == 0 || h == 0)
		{
			return;
		}

		aspectRatio_ = (float)w / h;
	}

	glm::mat4 PerspectiveCamera::matrix() const
	{
		return glm::perspective(glm::degrees(60.0f), aspectRatio_, 0.1f, 100.0f)
			* glm::inverse(glm::translate(glm::mat4(1), pos_) * (glm::mat4)rot_);
	}

	CameraController::CameraController()
	{
		orthographic();
	}

	void CameraController::resize(uint32_t w, uint32_t h)
	{
		orthographic_.resize(w, h);
		perspective_.resize(w, h);
	}
}