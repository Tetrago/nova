#include <nova/nova.h>

#include "editor.h"
#include "project.h"
#include "tools/scene_view.h"

namespace nova
{
	class Kilonova : public Program
	{
		using Program::Program;
	public:
		~Kilonova()
		{
			ini_.save(std::ofstream("kilonova.ini"));
		}

		void stop() override
		{
			if(editor_->showExitPrompt())
			{
				Program::stop();
			}
		}
	protected:
		void setup() override
		{
			ini_.load(std::ifstream("kilonova.ini"));
			AssetPipeline::addResolver<FileResolver>(std::filesystem::current_path());

			setupGraphics();

			gui_ = GuiLayer::create();
			editor_ = make_box<Editor>();

			static auto init = []() { nova_logger() << Log::Warn << "Deprecated scene view setup" << Log::Done; return 0; }();
			editor_->push(make_ref<SceneView>(&scene_));

			std::filesystem::path path = ini_["project"].get<std::string>("location");
			project_ = make_box<Project>(path);
			project_->setup(scene_);

			{
				NOVA_DIAG_SCOPE("Graphics");

				auto& device = Program::get().renderDevice();

				auto board = device.createTexture();
				board->load(ResourceManager::load<Image>("assets/static/textures/board.png"));
				board_ = SubTexture::from(board);

				auto img = device.createTexture();
				img->load(ResourceManager::load<Image>("assets/static/textures/img.png"));
				scene_.root()->get("Logo")->get<SpriteRenderer>()->texture = SubTexture::from(img);
			}
		}

		void tick() override
		{
			{
				NOVA_DIAG_SCOPE("Update");

				auto& ctx = Program::get().window().context();
				editor_->viewport().begin();

				ctx.setClearColor(0.1f, 0.1f, 0.1f, 1);
				ctx.clear();

				Renderer2D::mark();
				Renderer2D::prepair(editor_->viewport().matrix());

				Renderer2D::DrawDesc desc{};
				desc.position.z = -0.1f;
				desc.size = { 20, 20 };
				desc.tiling = { 20, 20 };

				Renderer2D::drawQuad(*board_, desc);

				if(running_ && !editor_->pending())
				{
					scene_.update();
				}

				scene_.render();
				Renderer2D::flush();

				editor_->viewport().end();
			}

			{
				NOVA_DIAG_SCOPE("Gui");

				gui_->begin();
				editor_->onGui();

				if(Gui::begin("Scene"))
				{
					if(Gui::button(running_ ? "Stop" : "Start"))
					{
						if(running_)
						{
							scene_.stop();
						}
						else
						{
							scene_.start();
						}

						running_ = !running_;
					}
				}

				Gui::end();

				gui_->end();
			}
		}
	private:
		Ini ini_;
		Box<Project> project_;

		Ref<GuiLayer> gui_;
		Box<Editor> editor_;

		Ref<SubTexture> board_;
		bool running_ = false;
		Scene scene_{};
	};
}

NOVA_BOOTSTRAP(nova::Kilonova, { "Kilonova", { 0, 1, 0 } });