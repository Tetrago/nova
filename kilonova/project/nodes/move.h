#pragma once

#include <nova/nova.h>

class NOVA_EXPORT Move : public nova::Rigidbody2D
{
	NOVA_NODE("Move");
public:
	void update() override;

	void onInspectorDraw() override;

	void serialize(nova::Json::Value& json) const override;
	void deserialize(const nova::Json::Value& json) override;
public:
	float speed = 10;
private:
	bool canJump();
};