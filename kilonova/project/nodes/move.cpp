#include "move.h"

void Move::update()
{
	Rigidbody2D::update();

	auto dt = nova::Time::deltaTime();
	auto& keyboard = nova::Keyboard::get();

	if(keyboard.isKeyDown(nova::KeyCode::Up) && canJump())
	{
		velocity.y = speed;
	}

	if(keyboard.isKeyDown(nova::KeyCode::Right))
	{
		velocity.x = speed;
	}
	else if(keyboard.isKeyDown(nova::KeyCode::Left))
	{
		velocity.x = -speed;
	}
	else
	{
		velocity.x = 0;
	}
}

void Move::onInspectorDraw()
{
	Rigidbody2D::onInspectorDraw();
	NOVA_NODE_INSPECTOR();

	nova::Gui::drag("Speed", speed);
}

void Move::serialize(nova::Json::Value& json) const
{
	Rigidbody2D::serialize(json);

	json["speed"] = speed;
}

void Move::deserialize(const nova::Json::Value& json)
{
	Rigidbody2D::deserialize(json);

	json.at("speed").get_to(speed);
}

bool Move::canJump()
{
	nova::RaycastHit2D hit;
	if(nova::World2D::raycast(hit, position(), { 0, -1 }, 0.6f))
	{
		return true;
	}

	return false;
}