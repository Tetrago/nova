#include <nova/nova.h>

#include "move.h"

extern "C" void NOVA_EXPORT register_nodes()
{
	nova::NodeRegistry::registerNode<Move>();
}