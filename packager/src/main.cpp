#include <string>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <nova/nova.h>

int main(int argc, char** argv)
{
	if(argc == 2)
	{
		nova::Package package{};
		package.unpack(std::ifstream{ argv[1], std::ios::binary });

		std::ofstream file{};

		for(const auto& [key, contents] : package)
		{
			file.open(key, std::ios::binary);

			if(file.is_open())
			{
				file.write(contents.data(), contents.size());
				file.close();
			}
			else
			{
				std::cout << "Unkown error writing package" << std::endl;
				return EXIT_FAILURE;
			}
		}

		return EXIT_SUCCESS;
	}
	else if(argc < 3)
	{
		std::cout << "Incorrect number of arguments" << std::endl;
		return EXIT_FAILURE;
	}
	else
	{
		nova::Package package{};
		std::ifstream file{};

		for(int i = 2; i < argc; ++i)
		{
			std::string desc{ argv[i] };

			auto split = desc.find(',');

			std::string name = desc.substr(0, split);
			std::filesystem::path path = desc.substr(split + 1);

			file.open(path, std::ios::binary);

			if(file.is_open())
			{
				std::string data{ std::istreambuf_iterator<char>(file), {} };
				package[name] = data;

				file.close();
			}
			else
			{
				std::cout << "File not found: " << path << std::endl;
			}
		}

		std::ofstream out{ argv[1], std::ios::binary };

		if(out.is_open())
		{
			out << package.pack();
		}
		else
		{
			std::cout << "Invalid output file" << std::endl;
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}
}