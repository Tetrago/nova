#include "npch.h"
#include "glfw.h"

#include "nova/core/diagnostics.h"
#include "nova/core/event.h"
#include "nova/core/log.h"
#include "nova/core/program.h"

#define WINDOW_EVENT(x) WindowEvent e{};\
	e.window = static_cast<Window*>(glfwGetWindowUserPointer(window));\
	e.type = x

namespace nova
{
	namespace platform
	{
		namespace
		{
			void glfw_error_callback(int error, const char* msg)
			{
				nova_logger() << Log::Error << "GLFW error (" << error << "): " << msg << Log::Done;
			}
		}

		GlfwWindow::GlfwWindow(const WindowCreateInfo& createInfo)
		{
			NOVA_DIAG_FUNCTION();

			Platform::get();
			const auto& engineInfo = Program::get().engineInfo();

			glfwDefaultWindowHints();
			glfwWindowHint(GLFW_MAXIMIZED, createInfo.maximized);
			glfwWindowHint(GLFW_RESIZABLE, createInfo.resizable);
			glfwWindowHint(GLFW_VISIBLE, createInfo.visible);
			glfwWindowHint(GLFW_FOCUSED, createInfo.focused);
			glfwWindowHint(GLFW_DECORATED, createInfo.decorated);
			glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_FALSE);

#if defined(NOVA_DEBUG) && defined(NOVA_MODULE_OPENGL)
			if(engineInfo.rendererApi == RendererApi::OpenGL)
			{
				glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
			}
#endif

			GLFWwindow* share = nullptr;
			if(createInfo.share)
			{
				share = static_cast<GlfwWindow*>(createInfo.share)->handle();
			}

			handle_ = glfwCreateWindow(createInfo.width, createInfo.height, createInfo.title_.c_str(), nullptr, share);
			glfwSetWindowPos(handle_, createInfo.x, createInfo.y);
			glfwSetWindowUserPointer(handle_, this);

#if defined(NOVA_MODULE_OPENGL)
			if(engineInfo.rendererApi == RendererApi::OpenGL)
			{
				glfwMakeContextCurrent(handle_);
			}
#endif

			glfwSetWindowCloseCallback(handle_, [](GLFWwindow* window)
				{
					WINDOW_EVENT(WindowEvent::Type::Close);
					EventBus::emit<WindowEvent>(e);
				});

			glfwSetKeyCallback(handle_, [](GLFWwindow* window, int key, int scancode, int action, int mods)
				{
					if(action == GLFW_REPEAT)
					{
						return;
					}

					WINDOW_EVENT(WindowEvent::Type::Key);
					e.key.key = static_cast<KeyCode>(key);
					e.key.status = action == GLFW_PRESS;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetMouseButtonCallback(handle_, [](GLFWwindow* window, int button, int action, int mods)
				{
					WINDOW_EVENT(WindowEvent::Type::Button);
					e.button.button = button;
					e.button.status = action == GLFW_PRESS || action == GLFW_REPEAT;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetCursorPosCallback(handle_, [](GLFWwindow* window, double x, double y)
				{
					WINDOW_EVENT(WindowEvent::Type::Mouse);
					e.mouse.x = x;
					e.mouse.y = y;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetWindowSizeCallback(handle_, [](GLFWwindow* window, int w, int h)
				{
					WINDOW_EVENT(WindowEvent::Type::Resize);
					e.resize.width = w;
					e.resize.height = h;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetWindowPosCallback(handle_, [](GLFWwindow* window, int x, int y)
				{
					WINDOW_EVENT(WindowEvent::Type::Move);
					e.move.x = x;
					e.move.y = y;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetWindowMaximizeCallback(handle_, [](GLFWwindow* window, int maximized)
				{
					WINDOW_EVENT(WindowEvent::Type::Maximize);
					e.maximize.maximized = maximized == GLFW_TRUE;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetScrollCallback(handle_, [](GLFWwindow* window, double x, double y)
				{
					WINDOW_EVENT(WindowEvent::Type::Scroll);
					e.scroll.delta = y;

					EventBus::emit<WindowEvent>(e);
				});

			glfwSetCharCallback(handle_, [](GLFWwindow* window, unsigned int c)
				{
					WINDOW_EVENT(WindowEvent::Type::Character);
					e.character.c = c;

					EventBus::emit<WindowEvent>(e);
				});
		}

		GlfwWindow::~GlfwWindow()
		{
			NOVA_DIAG_FUNCTION();

			glfwDestroyWindow(handle_);
		}

		void GlfwWindow::update()
		{
			glfwPollEvents();
		}

		bool GlfwWindow::isFocused() const
		{
			return glfwGetWindowAttrib(handle_, GLFW_FOCUSED);
		}

		bool GlfwWindow::isHovered() const
		{
			return glfwGetWindowAttrib(handle_, GLFW_HOVERED);
		}

		std::tuple<int, int> GlfwWindow::position() const
		{
			int x, y;
			glfwGetWindowPos(handle_, &x, &y);
			return std::make_tuple(x, y);
		}

		std::tuple<uint32_t, uint32_t> GlfwWindow::size() const
		{
			int w, h;
			glfwGetWindowSize(handle_, &w, &h);
			return std::make_tuple(w, h);
		}

		void GlfwWindow::focus()
		{
			glfwFocusWindow(handle_);
		}

		void GlfwWindow::position(int x, int y)
		{
			glfwSetWindowPos(handle_, x, y);
		}

		void GlfwWindow::size(uint32_t w, uint32_t h)
		{
			glfwSetWindowSize(handle_, w, h);
		}

		void GlfwWindow::title(const std::string& title)
		{
			glfwSetWindowTitle(handle_, title.c_str());
		}

		void GlfwWindow::visible(bool v)
		{
			if(v)
			{
				glfwShowWindow(handle_);
			}
			else
			{
				glfwHideWindow(handle_);
			}
		}

#if defined(NOVA_MODULE_OPENGL)
		void GlfwWindow::gl_MakeCurrent()
		{
			glfwMakeContextCurrent(handle_);
		}

		void GlfwWindow::gl_Swap()
		{
			glfwSwapBuffers(handle_);
		}
#endif

		GlfwPlatform::GlfwPlatform()
		{
			NOVA_DIAG_FUNCTION();

			int status = glfwInit();
			NOVA_CORE_ASSERT(status, "Failed to initialize GLFW");

			glfwSetErrorCallback(glfw_error_callback);
			glfwSetMonitorCallback([](GLFWmonitor*, int) { EventBus::emit<MonitorEvent>({ get().monitors() }); });
		}

		GlfwPlatform::~GlfwPlatform()
		{
			NOVA_DIAG_FUNCTION();

			glfwTerminate();
		}

		std::vector<Platform::Monitor> GlfwPlatform::monitors() const
		{
			NOVA_DIAG_FUNCTION();

			int count;
			GLFWmonitor** glfwMonitors = glfwGetMonitors(&count);

			std::vector<Monitor> monitors(count);

			for(int i = 0; i < count; ++i)
			{
				Monitor& monitor = monitors[i];

				glfwGetMonitorPos(glfwMonitors[i], &monitor.x, &monitor.y);
				
				const GLFWvidmode* mode = glfwGetVideoMode(glfwMonitors[i]);
				monitor.w = mode->width;
				monitor.h = mode->height;

				int x;
				int y;

				glfwGetMonitorWorkarea(glfwMonitors[i], &monitor.workarea.x, &monitor.workarea.y, &x, &y);
				glfwGetMonitorContentScale(glfwMonitors[i], &monitor.dpi, nullptr);

				monitor.workarea.x = x;
				monitor.workarea.y = y;
			}

			return monitors;
		}
	}
}