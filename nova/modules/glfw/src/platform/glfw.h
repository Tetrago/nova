#pragma once

#define GLFW_INCLUDE_NONE

#include <string>
#include <GLFW/glfw3.h>

#include "nova/core/platform.h"
#include "nova/graphics/window.h"

namespace nova
{
	namespace platform
	{
		class GlfwWindow : public Window
		{
		public:
			GlfwWindow(const WindowCreateInfo& createInfo);
			~GlfwWindow();

			GlfwWindow(const GlfwWindow&) = delete;
			GlfwWindow& operator=(const GlfwWindow&) = delete;
			GlfwWindow(GlfwWindow&&) = delete;
			GlfwWindow& operator=(GlfwWindow&&) = delete;

			void update() override;

			bool isFocused() const override;
			bool isHovered() const override;
			std::tuple<int, int> position() const override;
			std::tuple<uint32_t, uint32_t> size() const override;

			void focus() override;
			void position(int x, int y) override;
			void size(uint32_t w, uint32_t h) override;
			void title(const std::string& title) override;
			void visible(bool v) override;

#if defined(NOVA_MODULE_OPENGL)
			void gl_MakeCurrent() override;
			void gl_Swap() override;
#endif

			GLFWwindow* handle() const { return handle_; }
		private:
			GLFWwindow* handle_;
		};

		class GlfwPlatform : public Platform
		{
		public:
			GlfwPlatform();
			~GlfwPlatform();

			std::vector<Monitor> monitors() const override;
		};
	}
}