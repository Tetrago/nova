find_package(OpenGL REQUIRED)
add_subdirectory("${PROJECT_SOURCE_DIR}/vendor/glad")

set(MODULE_LIBRARIES "glad" "spirv-cross-glsl" ${OPENGL_LIBRARIES})
set(MODULE_INCLUDE_DIRS "${MODULE_SOURCE_DIR}/src")

set(MODULE_SOURCES
	"src/gl/gl_buffer.cpp"
	"src/gl/gl_cache.cpp"
	"src/gl/gl_framebuffer.cpp"
	"src/gl/gl_graphics_context.cpp"
	"src/gl/gl_gui_layer.cpp"
	"src/gl/gl_material.cpp"
	"src/gl/gl_pipeline.cpp"
	"src/gl/gl_render_device.cpp"
	"src/gl/gl_texture.cpp")

list(TRANSFORM MODULE_SOURCES PREPEND "${MODULE_SOURCE_DIR}/")