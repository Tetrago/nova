#include "npch.h"
#include "gl_render_device.h"

#include <glad/glad.h>

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"
#include "nova/core/platform.h"
#include "gl_buffer.h"
#include "gl_framebuffer.h"
#include "gl_graphics_context.h"
#include "gl_material.h"
#include "gl_pipeline.h"
#include "gl_texture.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			void GLAPIENTRY debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* msg, const void* userparam)
			{
				if(type == GL_DEBUG_TYPE_ERROR)
				{
					nova_logger() << Log::Error << "[OpenGL] [0x" << std::hex << std::setfill('0') << std::setw(4) << id << "] " << msg << Log::Done;
				}
			}
		}

		GLRenderDevice::GLRenderDevice()
		{
			NOVA_DIAG_FUNCTION();

			bool status = gladLoadGL();
			NOVA_CORE_ASSERT(status, "Failed to load OpenGL");

			nova_logger() << Log::Info << "OpenGL:\n"
				<< "\tVersion:  " << glGetString(GL_VERSION) << '\n'
				<< "\tRenderer: " << glGetString(GL_RENDERER) << '\n'
				<< "\tVendor:   " << glGetString(GL_VENDOR) << Log::Done;

			glEnable(GL_CULL_FACE);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if defined(NOVA_DEBUG)
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(debug_callback, nullptr);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, GL_FALSE);
#endif
		}

		Ref<Buffer> GLRenderDevice::createBuffer(const BufferCreateInfo& info)
		{
			return make_ref<GLBuffer>(info);
		}

		Ref<Framebuffer> GLRenderDevice::createFrambuffer(const FramebufferCreateInfo& info)
		{
			return make_ref<GLFrambuffer>(info);
		}

		Box<GraphicsContext> GLRenderDevice::createGraphicsContext(Window* window)
		{
			return make_box<GLGraphicsContext>(window);
		}

		Ref<Material> GLRenderDevice::createMaterial(const Ref<Pipeline>& pipeline)
		{
			return make_ref<GLMaterial>(cast_ref<GLPipeline>(pipeline));
		}

		Ref<Pipeline> GLRenderDevice::createPipeline(const PipelineCreateInfo& info)
		{
			return make_ref<GLPipeline>(info);
		}

		Ref<Texture> GLRenderDevice::createTexture()
		{
			return make_ref<GLTexture>();
		}
	}
}