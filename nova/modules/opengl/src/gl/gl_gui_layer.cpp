#include "npch.h"
#include "gl_gui_layer.h"

#define GLFW_INCLUDE_NONE

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include <examples/imgui_impl_opengl3.cpp>

#include <imgui.h>

#include "nova/core/diagnostics.h"
#include "nova/core/program.h"

namespace nova
{
	namespace gl
	{
		GLGuiLayer::GLGuiLayer()
		{
			NOVA_DIAG_FUNCTION();

			ImGui_ImplOpenGL3_Init("#version 130");
		}

		GLGuiLayer::~GLGuiLayer()
		{
			NOVA_DIAG_FUNCTION();

			ImGui_ImplOpenGL3_Shutdown();
		}

		void GLGuiLayer::begin()
		{
			NOVA_DIAG_FUNCTION();

			ImGui_ImplOpenGL3_NewFrame();
			backend_.newFrame();

			ImGui::NewFrame();
		}

		void GLGuiLayer::end()
		{
			NOVA_DIAG_FUNCTION();

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

			backend_.updateViewports();
		}
	}
}