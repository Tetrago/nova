#pragma once

#include <glad/glad.h>

#include "nova/graphics/framebuffer.h"

namespace nova
{
	namespace gl
	{
		class GLFrambuffer : public Framebuffer
		{
		public:
			GLFrambuffer(const FramebufferCreateInfo& info);
			~GLFrambuffer();

			GLuint handle() const { return id_; }
		private:
			GLuint id_;
			std::vector<Ref<Texture>> textures_;
		};
	}
}