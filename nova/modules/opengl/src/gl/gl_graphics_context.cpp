#include "npch.h"
#include "gl_graphics_context.h"

#include <glad/glad.h>

#include "nova/core/event.h"
#include "nova/core/program.h"
#include "nova/graphics/window.h"
#include "gl_buffer.h"
#include "gl_framebuffer.h"
#include "gl_cache.h"
#include "gl_pipeline.h"
#include "gl_texture.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			constexpr GLenum find_cull_mode(const CullMode& mode)
			{
				switch(mode)
				{
				default:
				case CullMode::None: return GL_NONE;
				case CullMode::Back: return GL_BACK;
				case CullMode::Front: return GL_FRONT;
				case CullMode::Both: return GL_FRONT_AND_BACK;
				}
			}
			
			constexpr GLenum find_fill_mode(const FillMode& mode)
			{
				switch(mode)
				{
				default:
				case FillMode::Fill: return GL_FILL;
				case FillMode::Line: return GL_LINE;
				case FillMode::Point: return GL_POINT;
				}
			}

			constexpr GLenum find_front_face(const FrontFace& face)
			{
				switch(face)
				{
				default:
				case FrontFace::Counterclockwise: return GL_CCW;
				case FrontFace::Clockwise: return GL_CW;
				}
			}

			constexpr GLenum find_topology(const PrimitiveTopologyType& type)
			{
				switch(type)
				{
				case PrimitiveTopologyType::PointList: return GL_POINTS;
				case PrimitiveTopologyType::LineList: return GL_LINES;
				case PrimitiveTopologyType::LineStrip: return GL_LINE_STRIP;
				default:
				case PrimitiveTopologyType::TriangleList: return GL_TRIANGLES;
				case PrimitiveTopologyType::TriangleStrip: return GL_TRIANGLE_STRIP;
				}
			}
		}

		GLGraphicsContext::GLGraphicsContext(Window* window)
			: window_(window)
		{}

		void GLGraphicsContext::use()
		{
			NOVA_DIAG_FUNCTION();

			window_->gl_MakeCurrent();
		}

		void GLGraphicsContext::setRenderTarget(const Framebuffer& target)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, static_cast<const GLFrambuffer&>(target).handle());
		}

		void GLGraphicsContext::clearRenderTarget()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		void GLGraphicsContext::viewport(int x, int y, int width, int height)
		{
			glViewport(x, y, width, height);
		}

		void GLGraphicsContext::setPipeline(const Pipeline& pipeline)
		{
			NOVA_DIAG_FUNCTION();

			const GLPipeline& pl = static_cast<const GLPipeline&>(pipeline);
			glUseProgram(pl.handle());

			auto& info = pl.info();
			glCullFace(find_cull_mode(info.rasterizer.cullMode));
			glPolygonMode(GL_FRONT_AND_BACK, find_fill_mode(info.rasterizer.fillMode));
			glFrontFace(find_front_face(info.rasterizer.frontFace));

			pipeline_ = &pl;
		}

		void GLGraphicsContext::setVertexBuffer(const Buffer& buffer)
		{
			glBindVertexArray(GLCache::get({ static_cast<const GLBuffer*>(&buffer), pipeline_ }));
		}

		void GLGraphicsContext::setIndexBuffer(const Buffer& buffer)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, static_cast<const GLBuffer&>(buffer).handle());
		}

		void GLGraphicsContext::setClearColor(float r, float g, float b, float a)
		{
			glClearColor(r, g, b, a);
		}

		void GLGraphicsContext::applyTextures(const Texture** textures, uint32_t count)
		{
			for(int i = 0; i < count; ++i)
			{
				glActiveTexture(GL_TEXTURE0 + i);
				glBindTexture(GL_TEXTURE_2D, static_cast<const GLTexture*>(textures[i])->handle());
			}
		}

		void GLGraphicsContext::present()
		{
			NOVA_DIAG_FUNCTION();

			window_->gl_Swap();
		}

		void GLGraphicsContext::clear()
		{
			glClear(GL_COLOR_BUFFER_BIT
				| GL_DEPTH_BUFFER_BIT
				| GL_STENCIL_BUFFER_BIT);
		}

		void GLGraphicsContext::drawIndexed(uint32_t count)
		{
			NOVA_DIAG_FUNCTION();

			glBindBufferBase(GL_UNIFORM_BUFFER, 0, pipeline_->uniformBuffer()->handle());
			glDrawElements(find_topology(pipeline_->info().primitiveTopology), count, GL_UNSIGNED_INT, nullptr);
			glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
		}
	}
}