#pragma once

#include <string>
#include <unordered_map>
#include <glad/glad.h>

#include "nova/graphics/material.h"
#include "gl/gl_pipeline.h"

namespace nova
{
	namespace gl
	{
		class GLMaterial : public Material
		{
		public:
			GLMaterial(const Ref<GLPipeline>& pipeline);

			void set(const std::string& name, const glm::mat3& v) override;
			void set(const std::string& name, const glm::mat4& v) override;

			void set(const std::string& name, float v) override;
			void set(const std::string& name, const glm::vec2& v) override;
			void set(const std::string& name, const glm::vec3& v) override;
			void set(const std::string& name, const glm::vec4& v) override;

			void set(const std::string& name, int v) override;
			void set(const std::string& name, const glm::ivec2& v) override;
			void set(const std::string& name, const glm::ivec3& v) override;
			void set(const std::string& name, const glm::ivec4& v) override;

			void setTexture(const std::string& name, int v) override;
			void setTexture(const std::string& name, int* v, uint32_t count) override;

			const Pipeline& pipeline() const override { return *pipeline_; }
		private:
			template<typename T>
			void load(const T& value, std::tuple<uint32_t, size_t> so);

			std::tuple<uint32_t, size_t> lookupUniform(const std::string& name);
			int32_t lookupTexture(const std::string& name);
			
			Ref<GLPipeline> pipeline_;
		};
	}
}