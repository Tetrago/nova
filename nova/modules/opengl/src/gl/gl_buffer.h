#pragma once

#include <glad/glad.h>

#include "nova/graphics/buffer.h"

namespace nova
{
	namespace gl
	{
		class GLBuffer : public Buffer
		{
		public:
			GLBuffer(const BufferCreateInfo& info);
			~GLBuffer();

			void load(const void* data, size_t size, uint32_t offset) override;

			size_t size() const override { return size_; }

			GLuint handle() const { return id_; }
		private:
			const size_t size_;
			GLuint id_ = 0;
		};
	}
}