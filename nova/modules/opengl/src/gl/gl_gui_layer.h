#pragma once

#include "nova/graphics/gui_layer.h"
#include "platform/imgui_backend.h"

namespace nova
{
	namespace gl
	{
		class GLGuiLayer : public GuiLayer
		{
		public:
			GLGuiLayer();
			~GLGuiLayer();

			void begin() override;
			void end() override;
		private:
			platform::ImGuiBackend backend_;
		};
	}
}