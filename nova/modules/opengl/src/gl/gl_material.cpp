#include "npch.h"
#include "gl_material.h"

#include <glm/gtc/type_ptr.hpp>

#include "nova/core/diagnostics.h"
#include "gl_buffer.h"
#include "gl_pipeline.h"

namespace nova
{
	namespace gl
	{
		GLMaterial::GLMaterial(const Ref<GLPipeline>& pipeline)
			: pipeline_(pipeline)
		{}

		void GLMaterial::set(const std::string& name, const glm::mat3& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::mat4& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, float v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::vec2& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::vec3& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::vec4& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, int v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::ivec2& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::ivec3& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::set(const std::string& name, const glm::ivec4& v)
		{
			load(v, lookupUniform(name));
		}

		void GLMaterial::setTexture(const std::string& name, int v)
		{
			glUniform1i(lookupTexture(name), v);
		}
		
		void GLMaterial::setTexture(const std::string& name, int* v, uint32_t count)
		{
			glUniform1iv(lookupTexture(name), count, v);
		}

		template<typename T>
		void GLMaterial::load(const T& value, std::tuple<uint32_t, size_t> so)
		{
			auto& [offset, size] = so;
			pipeline_->uniformBuffer()->load(static_cast<const void*>(&value), size, offset);
		}

		std::tuple<uint32_t, size_t> GLMaterial::lookupUniform(const std::string& name)
		{
			const auto& uniforms = pipeline_->uniforms();

			auto res = uniforms.find(name);
			if(res != uniforms.end())
			{
				return std::make_tuple(res->second.offset, res->second.size);
			}
			else
			{
				nova_logger() << Log::Warn << "Attempting to assign non existent uniform: " << name << Log::Done;
				return std::make_tuple(0, 0);
			}
		}

		int32_t GLMaterial::lookupTexture(const std::string& name)
		{
			const auto& textures = pipeline_->textures();

			auto res = textures.find(name);
			if(res != textures.end())
			{
				return res->second;
			}
			else
			{
				nova_logger() << Log::Warn << "Attempting to assign non existent texture: " << name << Log::Done;
				return 0;
			}
		}
	}
};