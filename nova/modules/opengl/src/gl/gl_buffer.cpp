#include "npch.h"
#include "gl_buffer.h"

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"
#include "gl_cache.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			GLenum find_usage(const BufferUsage& usage)
			{
				switch(usage)
				{
				case BufferUsage::Static: return GL_STATIC_DRAW;
				case BufferUsage::Dynamic: return GL_DYNAMIC_DRAW;
				}
			}
		}

		GLBuffer::GLBuffer(const BufferCreateInfo& info)
			: size_(info.size)
		{
			NOVA_DIAG_FUNCTION();

			glGenBuffers(1, &id_);
			glBindBuffer(GL_ARRAY_BUFFER, id_);
			glBufferData(GL_ARRAY_BUFFER, size_, info.data, find_usage(info.usage));
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		GLBuffer::~GLBuffer()
		{
			NOVA_DIAG_FUNCTION();

			glDeleteBuffers(1, &id_);
			GLCache::onDelete(this);
		}

		void GLBuffer::load(const void* data, size_t size, uint32_t offset)
		{
			NOVA_DIAG_FUNCTION();

			NOVA_CORE_ASSERT(size + offset <= size_, "Attempting to load more data than buffer can hold");

			glBindBuffer(GL_ARRAY_BUFFER, id_);
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
	}
}