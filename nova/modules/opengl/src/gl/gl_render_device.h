#pragma once

#include "nova/graphics/render_device.h"

namespace nova
{
	namespace gl
	{
		class GLRenderDevice : public RenderDevice
		{
		public:
			GLRenderDevice();

			Ref<Buffer> createBuffer(const BufferCreateInfo& info) override;
			Ref<Framebuffer> createFrambuffer(const FramebufferCreateInfo& info) override;
			Box<GraphicsContext> createGraphicsContext(Window* window) override;
			Ref<Material> createMaterial(const Ref<Pipeline>& pipeline) override;
			Ref<Pipeline> createPipeline(const PipelineCreateInfo& info) override;
			Ref<Texture> createTexture() override;
		};
	}
}