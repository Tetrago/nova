#pragma once

#include <unordered_map>
#include <glad/glad.h>

namespace nova
{
	namespace gl
	{
		class GLBuffer;
		class GLPipeline;

		struct GLCacheKey
		{
			const GLBuffer* buffer;
			const GLPipeline* pipeline;

			bool operator==(const GLCacheKey& key) const
			{
				return buffer == key.buffer && pipeline == key.pipeline;
			}

			struct Hash
			{
				std::size_t operator()(const GLCacheKey& key) const
				{
					return std::hash<const GLBuffer*>()(key.buffer) ^ std::hash<const GLPipeline*>()(key.pipeline);
				}
			};
		};

		class GLCache
		{
		public:
			static GLuint get(const GLCacheKey& key);

			static void onDelete(void* addr);
		private:
			static std::unordered_map<GLCacheKey, GLuint, GLCacheKey::Hash> vaoMap_;
		};
	}
}