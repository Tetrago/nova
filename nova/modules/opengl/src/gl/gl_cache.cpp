#include "npch.h"
#include "gl_cache.h"

#include "nova/core/diagnostics.h"
#include "gl_buffer.h"
#include "gl_pipeline.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			constexpr GLenum find_value_type(const ValueType& type)
			{
				switch(type)
				{
				default:
					nova_logger() << Log::Error << "Unsupported value type" << Log::Done;
					return GL_NONE;
				case ValueType::Float: return GL_FLOAT;
				case ValueType::Int: return GL_INT;
				}
			}
		}

		std::unordered_map<GLCacheKey, GLuint, GLCacheKey::Hash> GLCache::vaoMap_;

		GLuint GLCache::get(const GLCacheKey& key)
		{
			NOVA_DIAG_FUNCTION();

			auto res = vaoMap_.find(key);
			if(res != vaoMap_.end())
			{
				return res->second;
			}

			GLuint id;
			glGenVertexArrays(1, &id);

			glBindVertexArray(id);
			glBindBuffer(GL_ARRAY_BUFFER, key.buffer->handle());

			auto& elements = key.pipeline->elements();

			int stride = 0;
			for(const auto& element : elements)
			{
				stride += element.size();
			}

			int offset = 0;
			int i = 0;

			for(const auto& element : elements)
			{
				glEnableVertexAttribArray(i);
				glVertexAttribPointer(i, element.count, find_value_type(element.valueType), GL_FALSE, stride, reinterpret_cast<void*>(offset));

				offset += element.size();
				++i;
			}

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			vaoMap_.emplace(key, id);
			return id;
		}

		void GLCache::onDelete(void* addr)
		{
			for(auto i = vaoMap_.begin(); i != vaoMap_.end();)
			{
				if(i->first.buffer == addr
					|| i->first.pipeline == addr)
				{
					glDeleteVertexArrays(1, &i->second);
					i = vaoMap_.erase(i);
				}
				else
				{
					++i;
				}
			}
		}
	}
}