#pragma once

#include "nova/graphics/graphics_context.h"

namespace nova
{
	class Window;

	namespace gl
	{
		class GLPipeline;

		class GLGraphicsContext : public GraphicsContext
		{
		public:
			GLGraphicsContext(Window* window);

			void use() override;

			void setRenderTarget(const Framebuffer& target) override;
			void clearRenderTarget() override;
			void viewport(int x, int y, int width, int height) override;

			void setPipeline(const Pipeline& pipeline) override;
			void setVertexBuffer(const Buffer& buffer) override;
			void setIndexBuffer(const Buffer& buffer) override;
			void setClearColor(float r, float g, float b, float a) override;

			void applyTextures(const Texture** textures, uint32_t count) override;

			void present() override;
			void clear() override;
			void drawIndexed(uint32_t count) override;
		private:
			Window* window_;
			const GLPipeline* pipeline_;
		};
	}
}