#include "npch.h"
#include "gl_framebuffer.h"

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"
#include "gl_texture.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			constexpr GLenum find_attachment_type(const AttachmentType& type)
			{
				switch(type)
				{
				case AttachmentType::Color: return GL_COLOR_ATTACHMENT0;
				case AttachmentType::Depth: return GL_DEPTH_ATTACHMENT;
				}
			}
		}

		GLFrambuffer::GLFrambuffer(const FramebufferCreateInfo& info)
		{
			NOVA_DIAG_FUNCTION();

			glGenFramebuffers(1, &id_);
			glBindFramebuffer(GL_FRAMEBUFFER, id_);

			textures_.reserve(info.attachments.size());
			for(const auto& attachment : info.attachments)
			{
				GLenum type = find_attachment_type(attachment.type) + attachment.index;
				GLuint handle = cast_ref<GLTexture>(attachment.texture)->handle();

				glFramebufferTexture2D(GL_FRAMEBUFFER, type, GL_TEXTURE_2D, handle, 0);

				textures_.push_back(attachment.texture);
			}

			if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				nova_logger() << Log::Error << "Incomplete frambuffer" << Log::Done;
			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		GLFrambuffer::~GLFrambuffer()
		{
			NOVA_DIAG_FUNCTION();

			glDeleteFramebuffers(1, &id_);
		}
	}
}