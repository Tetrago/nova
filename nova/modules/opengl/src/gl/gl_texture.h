#pragma once

#include <glad/glad.h>

#include "nova/graphics/texture.h"

namespace nova
{
	class Image;

	namespace gl
	{
		class GLTexture : public Texture
		{
		public:
			GLTexture();
			~GLTexture();

			GLTexture(const GLTexture&) = delete;
			GLTexture& operator=(const GLTexture&) = delete;
			GLTexture(GLTexture&&) = delete;
			GLTexture& operator=(GLTexture&&) = delete;

			void create(const TextureCreateInfo& info) override;
			void load(ResourceID image) override;

			uint32_t width() const override { return width_; }
			uint32_t height() const override { return height_; }

			GLuint handle() const { return id_; }
		private:
			uint32_t width_ = 0;
			uint32_t height_ = 0;
			GLuint id_ = 0;
		};
	}
}