#pragma once

#include <tuple>
#include <vector>
#include <glad/glad.h>

#include "nova/resource/shader.h"
#include "nova/graphics/buffer.h"
#include "nova/graphics/pipeline.h"

namespace nova
{
	namespace gl
	{
		class GLBuffer;

		class GLPipeline : public Pipeline
		{
			using Dictionary = std::unordered_map<std::string, std::pair<uint32_t, size_t>>;
		public:
			GLPipeline(const PipelineCreateInfo& info);
			~GLPipeline();

			GLPipeline(const GLPipeline&) = delete;
			GLPipeline& operator=(const GLPipeline&) = delete;
			GLPipeline(GLPipeline&&) = delete;
			GLPipeline& operator=(GLPipeline&&) = delete;

			const PipelineCreateInfo& info() const { return info_; }

			GLuint handle() const { return id_; }
			const Ref<GLBuffer>& uniformBuffer() const { return uniformBuffer_; }
			const std::vector<LayoutElement>& elements() const { return elements_; }
			const Shader::UniformMap& uniforms() const { return uniforms_; }
			const Shader::TextureMap& textures() const { return textures_; }
		private:
			void buildProgram(Shader* shader);

			const PipelineCreateInfo info_;
			GLuint id_;
			Ref<GLBuffer> uniformBuffer_;

			std::vector<LayoutElement> elements_;
			Shader::UniformMap uniforms_;
			Shader::TextureMap textures_;
		};
	}
}