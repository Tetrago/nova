#include "npch.h"
#include "gl_texture.h"

#include <tuple>

#include "nova/core/diagnostics.h"
#include "nova/resource/image.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			constexpr std::tuple<GLenum, GLenum> parse_format(const TextureFormat& format)
			{
				switch(format)
				{
				case TextureFormat::Depth: return std::make_tuple(GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT);
				case TextureFormat::Rgb8: return std::make_tuple(GL_RGB, GL_RGB8);
				case TextureFormat::Rgba8: return std::make_tuple(GL_RGBA, GL_RGBA8);
				}
			}

			constexpr std::tuple<GLenum, GLenum> find_format(int channels)
			{
				switch(channels)
				{
				default:
				case 3: return std::make_tuple(GL_RGB, GL_RGB8);
				case 4: return std::make_tuple(GL_RGBA, GL_RGBA8);
				}
			}
		}

		GLTexture::GLTexture()
		{
			NOVA_DIAG_FUNCTION();

			glGenTextures(1, &id_);
		}

		GLTexture::~GLTexture()
		{
			NOVA_DIAG_FUNCTION();

			glDeleteTextures(1, &id_);
		}

		void GLTexture::create(const TextureCreateInfo& info)
		{
			NOVA_DIAG_FUNCTION();

			glBindTexture(GL_TEXTURE_2D, id_);

			auto [format, internalFormat] = parse_format(info.format);
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, info.width, info.height, 0, format, GL_UNSIGNED_BYTE, info.data);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			glBindTexture(GL_TEXTURE_2D, 0);

			width_ = info.width;
			height_ = info.height;
		}

		void GLTexture::load(ResourceID image)
		{
			Image* ptr = ResourceManager::get<Image>(image);

			NOVA_DIAG_FUNCTION();

			glBindTexture(GL_TEXTURE_2D, id_);

			auto [format, internalFormat] = find_format(ptr->channels());
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width_ = ptr->width(), height_ = ptr->height(), 0, format, GL_UNSIGNED_BYTE, ptr->data());

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}
}