#include "npch.h"
#include "gl_pipeline.h"

#include "nova/core/diagnostics.h"
#include "nova/core/program.h"
#include "nova/graphics/render_device.h"
#include "gl_buffer.h"
#include "gl_cache.h"

namespace nova
{
	namespace gl
	{
		namespace
		{
			constexpr GLenum find_shader_stage(const Shader::Stage& stage)
			{
				switch(stage)
				{
				default: return GL_NONE;
				case Shader::Stage::Vertex:		return GL_VERTEX_SHADER;
				case Shader::Stage::Fragment:	return GL_FRAGMENT_SHADER;
				}
			}
		}

		GLPipeline::GLPipeline(const PipelineCreateInfo& info)
			: info_(info)
		{
			NOVA_DIAG_FUNCTION();

			buildProgram(ResourceManager::get<Shader>(info.shader));
		}

		GLPipeline::~GLPipeline()
		{
			NOVA_DIAG_FUNCTION();

			glDeleteProgram(id_);
			GLCache::onDelete(this);
		}

		void GLPipeline::buildProgram(Shader* shader)
		{
			NOVA_DIAG_FUNCTION();
			
			const auto& glslModules = (*shader)[Shader::Language::Glsl];

			GLuint* shaders = static_cast<GLuint*>(alloca(glslModules.size() * sizeof(GLuint)));
			int index = 0;

			id_ = glCreateProgram();

			for(const Shader::Module& mod : glslModules)
			{
				GLuint shader = glCreateShader(find_shader_stage(mod.stage));

				const GLchar* data = static_cast<const GLchar*>(mod.source.c_str());
				const GLint size = static_cast<GLint>(mod.source.size());

				glShaderSource(shader, 1, &data, &size);
				glCompileShader(shader);

				int success;
				glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
				if(!success)
				{
					char log[256];
					glGetShaderInfoLog(shader, sizeof(log), nullptr, log);

					nova_logger() << Log::Error << "Failed to compile shader: " << log << Log::Done;
				}

				glAttachShader(id_, shader);
				shaders[index++] = shader;
			}

			glLinkProgram(id_);

			int success;
			glGetProgramiv(id_, GL_LINK_STATUS, &success);
			if(!success)
			{
				char log[256];
				glGetProgramInfoLog(id_, sizeof(log), nullptr, log);

				nova_logger() << Log::Error << "Failed to link program: " << log << Log::Done;
			}

			for(uint32_t i = 0; i < index; ++i)
			{
				glDeleteShader(shaders[i]);
			}

			elements_ = shader->elements();
			uniforms_ = shader->uniforms();
			textures_ = shader->textures();

			size_t size = 0;
			for(const auto& uniform : uniforms_)
			{
				size += uniform.second.size;
			}

			BufferCreateInfo createInfo{};
			createInfo.size = size;
			createInfo.usage = BufferUsage::Dynamic;
			createInfo.views = { BufferView::Uniform };

			uniformBuffer_ = cast_ref<GLBuffer>(Program::get().renderDevice().createBuffer(createInfo));
		}
	}
}