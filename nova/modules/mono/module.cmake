set(MODULE_INCLUDE_DIRS "${MODULE_SOURCE_DIR}/src")

set(MONO_FOUND FALSE)

option(NOVA_MONO_MANUAL "Manually specify Mono information" OFF)
if(NOVA_MONO_MANUAL)
	set(MONO_FOUND TRUE)

	set(NOVA_MONO_INCLUDE CACHE PATH "Mono include path")
	set(NOVA_MONO_LIBS CACHE STRING "Semicolon separated library paths")

	list(APPEND MODULE_INCLUDE_DIRS "${NOVA_MONO_INCLUDE}")
	list(APPEND MODULE_LIBRARIES ${NOVA_MONO_LIBS})
endif()

find_package(PkgConfig)
if(PKG_CONFIG_FOUND)
	pkg_check_modules(NOVA_PKG_MONO IMPORTED_TARGET mono-2)

	if(NOVA_PKG_MONO_FOUND)
		set(MONO_FOUND TRUE)

		list(APPEND MODULE_LIBRARIES PkgConfig::NOVA_PKG_MONO)
	endif()
endif()

if(NOT MONO_FOUND)
	message(FATAL_ERROR "Could not find Mono")
endif()

set(MODULE_SOURCES
	"src/mono/domain.cpp")

list(TRANSFORM MODULE_SOURCES PREPEND "${MODULE_SOURCE_DIR}/")

set(MODULE_PUBLIC_INCLUDE_DIRS "${MODULE_SOURCE_DIR}/include")