#include "npch.h"
#include "win32.h"

#include <vector>
#include <windowsx.h>
#include <commdlg.h>

#include "nova/core/program.h"

namespace nova
{
	namespace platform
	{
		namespace
		{
			constexpr const char CLASS_NAME[] = "win32_impl_nova";
			constexpr uint32_t DECORATIONS = WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

			KeyCode get_keycode(int scancode)
			{
				static KeyCode codes[512];

				static bool initialized = false;
				if(!initialized)
				{
					initialized = true;

					codes[0x00B] = KeyCode::Alpha0;
					codes[0x002] = KeyCode::Alpha1;
					codes[0x003] = KeyCode::Alpha2;
					codes[0x004] = KeyCode::Alpha3;
					codes[0x005] = KeyCode::Alpha4;
					codes[0x006] = KeyCode::Alpha5;
					codes[0x007] = KeyCode::Alpha6;
					codes[0x008] = KeyCode::Alpha7;
					codes[0x009] = KeyCode::Alpha8;
					codes[0x00A] = KeyCode::Alpha9;
					codes[0x01E] = KeyCode::A;
					codes[0x030] = KeyCode::B;
					codes[0x02E] = KeyCode::C;
					codes[0x020] = KeyCode::D;
					codes[0x012] = KeyCode::E;
					codes[0x021] = KeyCode::F;
					codes[0x022] = KeyCode::G;
					codes[0x023] = KeyCode::H;
					codes[0x017] = KeyCode::I;
					codes[0x024] = KeyCode::J;
					codes[0x025] = KeyCode::K;
					codes[0x026] = KeyCode::L;
					codes[0x032] = KeyCode::M;
					codes[0x031] = KeyCode::N;
					codes[0x018] = KeyCode::O;
					codes[0x019] = KeyCode::P;
					codes[0x010] = KeyCode::Q;
					codes[0x013] = KeyCode::R;
					codes[0x01F] = KeyCode::S;
					codes[0x014] = KeyCode::T;
					codes[0x016] = KeyCode::U;
					codes[0x02F] = KeyCode::V;
					codes[0x011] = KeyCode::W;
					codes[0x02D] = KeyCode::X;
					codes[0x015] = KeyCode::Y;
					codes[0x02C] = KeyCode::Z;

					codes[0x028] = KeyCode::Apostrophe;
					codes[0x02B] = KeyCode::Backslash;
					codes[0x033] = KeyCode::Comma;
					codes[0x00D] = KeyCode::Equal;
					codes[0x029] = KeyCode::GraveAccent;
					codes[0x01A] = KeyCode::LeftBracket;
					codes[0x00C] = KeyCode::Minus;
					codes[0x034] = KeyCode::Period;
					codes[0x01B] = KeyCode::RightBracket;
					codes[0x027] = KeyCode::Semicolon;
					codes[0x035] = KeyCode::Slash;
					codes[0x056] = KeyCode::World2;

					codes[0x00E] = KeyCode::Backspace;
					codes[0x153] = KeyCode::Delete;
					codes[0x14F] = KeyCode::End;
					codes[0x01C] = KeyCode::Enter;
					codes[0x001] = KeyCode::Escape;
					codes[0x147] = KeyCode::Home;
					codes[0x152] = KeyCode::Insert;
					codes[0x15D] = KeyCode::Menu;
					codes[0x151] = KeyCode::PageDown;
					codes[0x149] = KeyCode::PageUp;
					codes[0x045] = KeyCode::Pause;
					codes[0x146] = KeyCode::Pause;
					codes[0x039] = KeyCode::Space;
					codes[0x00F] = KeyCode::Tab;
					codes[0x03A] = KeyCode::CapsLock;
					codes[0x145] = KeyCode::NumLock;
					codes[0x046] = KeyCode::ScrollLock;
					codes[0x03B] = KeyCode::F1;
					codes[0x03C] = KeyCode::F2;
					codes[0x03D] = KeyCode::F3;
					codes[0x03E] = KeyCode::F4;
					codes[0x03F] = KeyCode::F5;
					codes[0x040] = KeyCode::F6;
					codes[0x041] = KeyCode::F7;
					codes[0x042] = KeyCode::F8;
					codes[0x043] = KeyCode::F9;
					codes[0x044] = KeyCode::F10;
					codes[0x057] = KeyCode::F11;
					codes[0x058] = KeyCode::F12;
					codes[0x064] = KeyCode::F13;
					codes[0x065] = KeyCode::F14;
					codes[0x066] = KeyCode::F15;
					codes[0x067] = KeyCode::F16;
					codes[0x068] = KeyCode::F17;
					codes[0x069] = KeyCode::F18;
					codes[0x06A] = KeyCode::F19;
					codes[0x06B] = KeyCode::F20;
					codes[0x06C] = KeyCode::F21;
					codes[0x06D] = KeyCode::F22;
					codes[0x06E] = KeyCode::F23;
					codes[0x076] = KeyCode::F24;
					codes[0x038] = KeyCode::LeftAlt;
					codes[0x01D] = KeyCode::LeftControl;
					codes[0x02A] = KeyCode::LeftShift;
					codes[0x15B] = KeyCode::LeftSuper;
					codes[0x137] = KeyCode::PrintScreen;
					codes[0x138] = KeyCode::RightAlt;
					codes[0x11D] = KeyCode::RightControl;
					codes[0x036] = KeyCode::RightShift;
					codes[0x15C] = KeyCode::RightSuper;
					codes[0x150] = KeyCode::Down;
					codes[0x14B] = KeyCode::Left;
					codes[0x14D] = KeyCode::Right;
					codes[0x148] = KeyCode::Up;

					codes[0x052] = KeyCode::Keypad0;
					codes[0x04F] = KeyCode::Keypad1;
					codes[0x050] = KeyCode::Keypad2;
					codes[0x051] = KeyCode::Keypad3;
					codes[0x04B] = KeyCode::Keypad4;
					codes[0x04C] = KeyCode::Keypad5;
					codes[0x04D] = KeyCode::Keypad6;
					codes[0x047] = KeyCode::Keypad7;
					codes[0x048] = KeyCode::Keypad8;
					codes[0x049] = KeyCode::Keypad9;
					codes[0x04E] = KeyCode::KeypadAdd;
					codes[0x053] = KeyCode::KeypadDecimal;
					codes[0x135] = KeyCode::KeypadDivide;
					codes[0x11C] = KeyCode::KeypadEnter;
					codes[0x059] = KeyCode::KeypadEqual;
					codes[0x037] = KeyCode::KeypadMultiply;
					codes[0x04A] = KeyCode::KeypadSubtract;
				}

				return codes[scancode];
			}

			BOOL WINAPI monitor_enum(HMONITOR monitor, HDC hdc, LPRECT rect, LPARAM user)
			{
				std::vector<Platform::Monitor>* monitors = reinterpret_cast<std::vector<Platform::Monitor>*>(user);
				Platform::Monitor pm{};

				MONITORINFO info{};
				info.cbSize = sizeof(info);

				BOOL status = GetMonitorInfo(monitor, &info);
				NOVA_CORE_ASSERT(status, "Could not fetch monitor info");

				pm.x = info.rcMonitor.left;
				pm.y = info.rcMonitor.top;
				pm.w = info.rcMonitor.bottom - info.rcMonitor.top;
				pm.h = info.rcMonitor.right - info.rcMonitor.left;

				pm.workarea.x = info.rcWork.left;
				pm.workarea.y = info.rcWork.top;
				pm.workarea.w = info.rcWork.bottom - info.rcWork.top;
				pm.workarea.h = info.rcWork.right - info.rcWork.left;

				const HDC dc = GetDC(nullptr);
				pm.dpi = GetDeviceCaps(dc, LOGPIXELSX);
				ReleaseDC(nullptr, dc);

				monitors->push_back(pm);
				return TRUE;
			}

			void fill_file_dialogue(OPENFILENAME& ofn, HWND handle, const char* filter, char* file, size_t size)
			{
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = handle;
				ofn.lpstrFile = file;
				ofn.nMaxFile = sizeof(file);
				ofn.lpstrFilter = filter;
				ofn.nFilterIndex = 1;
				ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;
			}
		}

		Win32Window::Win32Window(const WindowCreateInfo& createInfo)
		{
			NOVA_DIAG_FUNCTION();

			Platform::get();

			RECT wr{};
			wr.left = 0;
			wr.right = createInfo.width + wr.left;
			wr.top = 0;
			wr.bottom = createInfo.height + wr.top;

			status_.style = DECORATIONS;
			if(createInfo.decorated)
			{
				status_.style |= WS_CAPTION;

				if(createInfo.resizable)
				{
					status_.style |= WS_MAXIMIZEBOX | WS_THICKFRAME;
				}
			}
			else
			{
				status_.style |= WS_POPUP;
			}
			if(createInfo.maximized)
			{
				status_.style |= WS_MAXIMIZE;
				status_.maximized = true;
			}

			BOOL status = AdjustWindowRect(&wr, status_.style, FALSE);
			NOVA_CORE_ASSERT(status, "Failed to create rect");

			handle_ = CreateWindow(CLASS_NAME, createInfo.title_.c_str(), status_.style,
				createInfo.x, createInfo.y, wr.right - wr.left, wr.bottom - wr.top,
				nullptr, nullptr, GetModuleHandle(nullptr), this);

			NOVA_CORE_ASSERT(handle_, "Failed to create window");
			hdc_ = GetDC(handle_);

			if(createInfo.visible)
			{
				ShowWindow(handle_, SW_SHOWNA);
			}
			if(createInfo.focused)
			{
				focus();
			}

#if defined(NOVA_MODULE_OPENGL)
			if(Program::get().engineInfo().rendererApi == RendererApi::OpenGL)
			{
				wgl_CreateContext();

				if(createInfo.share)
				{
					wgl_ShareContext(static_cast<Win32Window*>(createInfo.share)->wgl_);
				}

				gl_MakeCurrent();
			}
#endif
		}

		Win32Window::~Win32Window()
		{
			NOVA_DIAG_FUNCTION();

#if defined(NOVA_MODULE_OPENGL)
			if(wgl_)
			{
				gl_MakeCurrent();
				wglDeleteContext(wgl_);
			}
#endif

			ReleaseDC(handle_, hdc_);
			DestroyWindow(handle_);
		}

		void Win32Window::update()
		{
			NOVA_DIAG_FUNCTION();

			MSG msg;
			while(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		bool Win32Window::isFocused() const
		{
			return handle_ == GetActiveWindow();
		}

		bool Win32Window::isHovered() const
		{
			RECT area{};
			POINT pos{};

			if(!GetCursorPos(&pos) || WindowFromPoint(pos) != handle_)
			{
				return false;
			}

			GetClientRect(handle_, &area);
			ClientToScreen(handle_, reinterpret_cast<POINT*>(&area.left));
			ClientToScreen(handle_, reinterpret_cast<POINT*>(&area.right));

			return PtInRect(&area, pos);
		}

		std::tuple<int, int> Win32Window::position() const
		{
			POINT pos{};
			ClientToScreen(handle_, &pos);

			return std::make_tuple(pos.x, pos.y);
		}

		std::tuple<uint32_t, uint32_t> Win32Window::size() const
		{
			RECT rect{};
			GetClientRect(handle_, &rect);

			return std::make_tuple(rect.right, rect.bottom);
		}

		void Win32Window::focus()
		{
			BringWindowToTop(handle_);
			SetForegroundWindow(handle_);
			SetFocus(handle_);
		}

		void Win32Window::position(int x, int y)
		{
			RECT rect{ x, y, x, y };

			BOOL status = AdjustWindowRect(&rect, status_.style, FALSE);
			NOVA_CORE_ASSERT(status, "Failed to adjust rect");

			SetWindowPos(handle_, nullptr,
				rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,
				SWP_NOACTIVATE | SWP_NOZORDER | SWP_NOSIZE);
		}

		void Win32Window::size(uint32_t w, uint32_t h)
		{
			RECT rect{ 0, 0, w, h };

			BOOL status = AdjustWindowRect(&rect, status_.style, FALSE);
			NOVA_CORE_ASSERT(status, "Failed to adjust rect");

			SetWindowPos(handle_, HWND_TOP, 0, 0,
				rect.right - rect.left, rect.bottom - rect.top,
				SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOZORDER);
		}

		void Win32Window::title(const std::string& title)
		{
			SetWindowText(handle_, title.c_str());
		}

		void Win32Window::visible(bool v)
		{
			ShowWindow(handle_, v ? SW_SHOWNA : SW_HIDE);
		}

#if defined(NOVA_MODULE_OPENGL)
		void Win32Window::wgl_CreateContext()
		{
			NOVA_DIAG_FUNCTION();

			PIXELFORMATDESCRIPTOR pfd{};

			pfd.nSize = sizeof(pfd);
			pfd.nVersion = 1;
			pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pfd.iPixelType = PFD_TYPE_RGBA;
			pfd.cColorBits = 32;

			int pixelFormat = ChoosePixelFormat(hdc_, &pfd);
			NOVA_CORE_ASSERT(pixelFormat, "Failed to find a suitable pixel format");

			int status = SetPixelFormat(hdc_, pixelFormat, &pfd);
			NOVA_CORE_ASSERT(status, "Failed to set pixel format");

			wgl_ = wglCreateContext(hdc_);
		}

		void Win32Window::wgl_ShareContext(HGLRC ctx)
		{
			NOVA_DIAG_FUNCTION();

			BOOL status = wglShareLists(ctx, wgl_);
			NOVA_CORE_ASSERT(status, "Could not share contexts");
		}

		void Win32Window::gl_MakeCurrent()
		{
			NOVA_DIAG_FUNCTION();

			wglMakeCurrent(hdc_, wgl_);
		}

		void Win32Window::gl_Swap()
		{
			NOVA_DIAG_FUNCTION();

			SwapBuffers(hdc_);
		}
#endif

		LRESULT WINAPI Win32Window::initializeMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			NOVA_DIAG_FUNCTION();

			if(msg == WM_NCCREATE)
			{
				SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
				SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&Win32Window::forwardMsg));
			}

			return DefWindowProc(hWnd, msg, wParam, lParam);
		}

		LRESULT WINAPI Win32Window::forwardMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			return reinterpret_cast<Win32Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA))->handleMsg(hWnd, msg, wParam, lParam);
		}
		
		LRESULT WINAPI Win32Window::handleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			NOVA_DIAG_FUNCTION();

			WindowEvent e{ this };
			switch(msg)
			{
			case WM_CLOSE:
				e.type = WindowEvent::Type::Close;
				EventBus::emit<WindowEvent>(e);
				return 0;
			case WM_KEYUP:
			case WM_KEYDOWN:
				e.type = WindowEvent::Type::Key;

				e.key.status = msg == WM_KEYDOWN;

				{
					int scancode = (HIWORD(lParam) & (KF_EXTENDED | 0xff));
					if(!scancode)
					{
						scancode = MapVirtualKey(wParam, MAPVK_VK_TO_VSC);
					}

					e.key.key = get_keycode(scancode);
				}

				EventBus::emit<WindowEvent>(e);
				break;
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
			case WM_XBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_MBUTTONUP:
			case WM_XBUTTONUP:
				e.type = WindowEvent::Type::Button;

				e.button.status = msg == WM_LBUTTONDOWN || msg == WM_RBUTTONDOWN
					|| msg == WM_MBUTTONDOWN || msg == WM_XBUTTONDOWN;

				e.button.button = [&]()
				{
					switch(msg)
					{
					case WM_LBUTTONDOWN:
					case WM_LBUTTONUP:
						return 0;
					case WM_RBUTTONDOWN:
					case WM_RBUTTONUP:
						return 1;
					case WM_MBUTTONDOWN:
					case WM_MBUTTONUP:
						return 2;
					case WM_XBUTTONDOWN:
					case WM_XBUTTONUP:
						if(GET_XBUTTON_WPARAM(wParam) == XBUTTON1)
						{
							return 4;
						}
					default:
						return 5;
					}
				}();

				if(!status_.buttons)
				{
					SetCapture(handle_);
				}

				EventBus::emit<WindowEvent>(e);

				if(e.button.status) { status_.buttons |= e.button.button + 1; }
				else { status_.buttons &= ~e.button.button + 1; }

				if(!status_.buttons)
				{
					ReleaseCapture();
				}

				break;
			case WM_MOUSEMOVE:
				e.type = WindowEvent::Type::Mouse;

				e.mouse.x = GET_X_LPARAM(lParam);
				e.mouse.y = GET_Y_LPARAM(lParam);

				EventBus::emit<WindowEvent>(e);
				break;
			case WM_SIZE:
				if(wParam == SIZE_RESTORED && status_.maximized)
				{
					e.type = WindowEvent::Type::Maximize;
					e.maximize.maximized = status_.maximized = false;
					EventBus::emit<WindowEvent>(e);
				}
				
				if(wParam != SIZE_MAXIMIZED)
				{
					e.type = WindowEvent::Type::Resize;

					e.resize.width = LOWORD(lParam);
					e.resize.height = HIWORD(lParam);

					if(wParam == SIZE_MINIMIZED)
					{
						e.resize.width = e.resize.height = 0;
					}
				}
				else if(!status_.maximized)
				{
					e.type = WindowEvent::Type::Maximize;
					e.maximize.maximized = status_.maximized = true;
				}

				EventBus::emit<WindowEvent>(e);
				break;
			case WM_MOVE:
				e.type = WindowEvent::Type::Move;

				e.move.x = GET_X_LPARAM(lParam);
				e.move.y = GET_Y_LPARAM(lParam);

				EventBus::emit<WindowEvent>(e);
				break;
			case WM_MOUSEWHEEL:
				e.type = WindowEvent::Type::Scroll;
				e.scroll.delta = (SHORT)HIWORD(wParam) / (double)WHEEL_DELTA;

				EventBus::emit<WindowEvent>(e);
				break;
			case WM_CHAR:
				e.type = WindowEvent::Type::Character;
				e.character.c = wParam;

				EventBus::emit<WindowEvent>(e);
				break;
			}

			return DefWindowProc(hWnd, msg, wParam, lParam);
		}

		Win32Platform::Win32Platform()
		{
			NOVA_DIAG_FUNCTION();

			WNDCLASSEX wc{};
			wc.cbSize = sizeof(wc);
			wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
			wc.lpfnWndProc = &Win32Window::initializeMsg;
			wc.hInstance = GetModuleHandle(nullptr);
			wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
			wc.lpszClassName = CLASS_NAME;
			RegisterClassEx(&wc);
		}

		Win32Platform::~Win32Platform()
		{
			NOVA_DIAG_FUNCTION();

			UnregisterClass(CLASS_NAME, GetModuleHandle(nullptr));
		}

		std::vector<Platform::Monitor> Win32Platform::monitors() const
		{
			NOVA_DIAG_FUNCTION();

			std::vector<Monitor> monitors;
			EnumDisplayMonitors(nullptr, nullptr, monitor_enum, reinterpret_cast<LPARAM>(&monitors));
			return monitors;
		}

		std::optional<std::filesystem::path> Win32Platform::openFileDialogue(const Window& window, const char* filter) const
		{
			OPENFILENAME ofn{};
			char file[256]{};
			fill_file_dialogue(ofn, static_cast<const Win32Window&>(window).handle_, filter, file, sizeof(file));

			if(GetOpenFileName(&ofn))
			{
				return ofn.lpstrFile;
			}

			return std::nullopt;
		}

		std::optional<std::filesystem::path> Win32Platform::saveFileDialogue(const Window& window, const char* filter) const
		{
			OPENFILENAME ofn{};
			char file[256]{};
			fill_file_dialogue(ofn, static_cast<const Win32Window&>(window).handle_, filter, file, sizeof(file));

			if(GetSaveFileName(&ofn))
			{
				return ofn.lpstrFile;
			}

			return std::nullopt;
		}
	}
}