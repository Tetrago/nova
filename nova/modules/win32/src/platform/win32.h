#pragma once

#include <string>
#include <Windows.h>

#include "nova/core/platform.h"
#include "nova/core/input.h"
#include "nova/graphics/window.h"

namespace nova
{
	namespace platform
	{
		class Win32Window : public Window
		{
			friend class Win32Platform;
		public:
			Win32Window(const WindowCreateInfo& createInfo);
			~Win32Window();

			void update() override;

			bool isFocused() const override;
			bool isHovered() const override;
			std::tuple<int, int> position() const override;
			std::tuple<uint32_t, uint32_t> size() const override;

			void focus() override;
			void position(int x, int y) override;
			void size(uint32_t w, uint32_t h) override;
			void title(const std::string& title) override;
			void visible(bool v) override;

#if defined(NOVA_MODULE_OPENGL)
			void wgl_CreateContext();
			void wgl_ShareContext(HGLRC ctx);
			void gl_MakeCurrent() override;
			void gl_Swap() override;
#endif
		private:
			static LRESULT WINAPI initializeMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			static LRESULT WINAPI forwardMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			LRESULT WINAPI handleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

			struct
			{
				bool maximized = false;
				uint32_t buttons = 0;
				uint32_t style = 0;
			} status_;

			HWND handle_;
			HDC hdc_ = nullptr;
#if defined(NOVA_MODULE_OPENGL)
			HGLRC wgl_ = nullptr;
#endif
		};

		class Win32Platform : public Platform
		{
		public:
			Win32Platform();
			~Win32Platform();

			std::vector<Monitor> monitors() const override;

			std::optional<std::filesystem::path> openFileDialogue(const Window& window, const char* filter) const override;
			std::optional<std::filesystem::path> saveFileDialogue(const Window& window, const char* filter) const override;
		};
	}
}