if(NOT WIN32)
	message(FATAL_ERROR "Cannot enable NOVA_MODULE_WIN32 on non-windows system")
endif()

set(MODULE_SOURCES
	"src/platform/win32.cpp")

list(TRANSFORM MODULE_SOURCES PREPEND "${MODULE_SOURCE_DIR}/")
set(MODULE_INCLUDE_DIRS "${MODULE_SOURCE_DIR}/src")