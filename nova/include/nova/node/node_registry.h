#pragma once

#include <string>
#include <unordered_map>

#include "node.h"
#include "node_storage.h"

namespace nova
{
	class NOVA_API NodeRegistry
	{
		using Registry = std::unordered_map<std::string, Box<NodePool>(*)()>;
	public:
		/// <summary>
		/// Registers node of <typeparamref name="T"/> given it has called <see cref="NOVA_NODE"/>.
		/// </summary>
		/// <typeparam name="T">Node to register.</typeparam>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		static void registerNode()
		{
			map_[T::NODE_NAME] = []() -> Box<NodePool> { return make_box<typename TemplateNodePool<T>>(); };
		}

		static const Registry& get() { return map_; }
	private:
		static Registry map_;
	};

	void register_nova_nodes();
}