#pragma once

#include <string>
#include <vector>

#include "scene.h"
#include "nova/core/base.h"
#include "nova/graphics/gui.h"
#include "nova/resource/json.h"

/// <summary>
/// Must be called in node class declartions to work properly.
/// </summary>
/// <param name="name">Name of the node.</param>
#define NOVA_NODE(name) public: static constexpr const char* NODE_NAME = name; const char* registryName() const override { return NODE_NAME; }

/// <summary>
/// Called in <see cref="Node"/> inspector draw to setup uniform sections.
/// </summary>
#define NOVA_NODE_INSPECTOR() ::nova::Gui::newLine(); ::nova::Gui::text(NODE_NAME); ::nova::Gui::separator()

namespace nova
{
	/// <summary>
	/// Base class of all nodes.
	/// </summary>
	class NOVA_API Node
	{
	public:
		static constexpr const char* NODE_NAME{ "Node" };
	private:
		friend class Scene;
	public:
		virtual ~Node();

		void operator delete(void*) = delete;
		void operator delete[](void*) = delete;

		/// <summary>
		/// Gets child node by name.
		/// </summary>
		/// <param name="name">Name of node to find.</param>
		/// <returns>Generic pointer of node or <c>nullptr</c>.</returns>
		Node* get(const std::string& name);

		/// <summary>
		/// Gets child node by name and casts to type <typeparamref name="T"/>.
		/// </summary>
		/// <param name="name">Name of node to find.</param>
		/// <typeparam name="T">Type to return.</typeparam>
		/// <returns>Node pointer of type <typeparamref name="T"/> or <c>nullptr</c>.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		T* get(const std::string& name)
		{
			return dynamic_cast<T*>(get(name));
		}

		/// <summary>
		/// Gets child node and casts to type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T">Type to return.</typeparam>
		/// <returns>Node pointer of type <typeparamref name="T"/> or <c>nullptr</c>.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		T* get()
		{
			return get<T>(T::NODE_NAME);
		}

		/// <summary>
		/// Adds a node of type <typeparamref name="T"/> with <paramref name="name"/>.
		/// </summary>
		/// <param name="name">Name of node to add.</param>
		/// <typeparam name="T">Type of node to add.</typeparam>
		/// <returns>Added node of type <typeparamref name="T"/>.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		T* add(const std::string& name)
		{
			return static_cast<T*>(addByName(T::NAME, name));
		}

		/// <summary>
		/// Adds a node of type <typeparamref name="T"/> with name <see cref="registryName"/>.
		/// </summary>
		/// <typeparam name="T">Type of node to add.</typeparam>
		/// <returns>Added node of type <typeparamref name="T"/>.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		T* add()
		{
			return add<T>(T::NAME);
		}

		/// <summary>
		/// Removes a child node by pointer.
		/// </summary>
		/// <param name="node">Pointer of node to remove.</param>
		void remove(Node* node);

		/// <summary>
		/// Adds a node by name setup in <see cref="NOVA_NODE"/>.
		/// </summary>
		/// <param name="name">Name to find.</param>
		/// <returns>Added node or <c>nullptr</c>.</returns>
		Node* addByName(const std::string& name);

		/// <summary>
		/// Adds a node by name setup in <see cref="NOVA_NODE"/>.
		/// </summary>
		/// <param name="registryName">Name to find.</param>
		/// <param name="name">Name of node in scene.</param>
		/// <returns>Added node or <c>nullptr</c>.</returns>
		Node* addByName(const std::string& registryName, const std::string& name);

		/// <summary>
		/// Called before all <see cref="start"/> calls.
		/// </summary>
		virtual void awake() {}

		/// <summary>
		/// Called at the start of a scene.
		/// </summary>
		virtual void start() {}

		/// <summary>
		/// Called at the end of a scene.
		/// </summary>
		virtual void stop() {}

		/// <summary>
		/// Called every frame if the scene is running.
		/// </summary>
		virtual void update() {}

		/// <summary>
		/// Called after all <see cref="update"/> calls.
		/// </summary>
		virtual void lateUpdate() {}

		/// <summary>
		/// Called a fixed number of times per second.
		/// </summary>
		virtual void fixedUpdate() {}

		/// <summary>
		/// Called every frame regardless of whether the scene is running.
		/// </summary>
		virtual void render() {}

		/// <summary>
		/// Called after the node is properly initialized and can all <see cref="add"/> and <see cref="get"/>.
		/// </summary>
		virtual void onCreate() {}

		/// <summary>
		/// Called when the node is effectively destroyed, but not necessarily at the same time as it it destructed.
		/// </summary>
		virtual void onDestroy() {}

		/// <summary>
		/// Called to display properties.
		/// </summary>
		virtual void onInspectorDraw();

		/// <summary>
		/// Called to save scene data.
		/// </summary>
		/// <param name="json"><see cref="Json"/> object to store data to.</param>
		virtual void serialize(Json::Value& json) const {}

		/// <summary>
		/// Called to load scene data.
		/// </summary>
		/// <param name="json"><see cref="Json"/> object to read data from.</param>
		virtual void deserialize(const Json::Value& json) {}

		/// <summary>
		/// Renames node.
		/// </summary>
		/// <param name="name">New name.</param>
		void name(const std::string& name) { name_ = name; }

		virtual const char* registryName() const { return NODE_NAME; }
		const std::string& name() const { return name_; }
		Node* parent() const { return parent_; }
		const std::vector<Node*>& nodes() const { return nodes_; }

		static void destroy(Node* node);
	private:
		std::string name_;
		Node* parent_ = nullptr;
		Scene* scene_ = nullptr;
		std::vector<Node*> nodes_{};
	};
}