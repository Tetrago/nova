#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "nova/graphics/sub_texture.h"
#include "node.h"

namespace nova
{
	/// <summary>
	/// <see cref="Node"/> with 2D transform.
	/// </summary>
	class NOVA_API Node2D : public Node
	{
		NOVA_NODE("Node2D");
	public:
		void onInspectorDraw() override;

		void serialize(Json::Value& json) const override;
		void deserialize(const Json::Value& json) override;

		/// <summary>
		/// Gets the current position accounting for the parent node's position, rotation, and scale.
		/// </summary>
		/// <returns>Global position.</returns>
		glm::vec2 position();

		/// <summary>
		/// Gets the current rotation accounting for the parent node's rotation.
		/// </summary>
		/// <returns>Global rotation in degrees.</returns>
		float rotation();

		/// <summary>
		/// Gets the current scale accounting for the parent node's scale.
		/// </summary>
		/// <returns>Global scale.</returns>
		glm::vec2 scale();
	public:
		/// <value>The local position relative to the parent node.</value>
		glm::vec2 localPosition{};

		/// <value>The local rotation (in degrees) relative to the parent node.</value>
		float localRotation = 0;

		/// <value>The local scale relative to the parent node.</value>
		glm::vec2 localScale{ 1 };
	};

	/// <summary>
	/// <see cref="Node2D"/> with sprite renderering capabilities.
	/// </summary>
	class NOVA_API SpriteRenderer : public Node2D
	{
		NOVA_NODE("SpriteRenderer");
	public:
		void render() override;

		void onInspectorDraw() override;

		void serialize(Json::Value& json) const override;
		void deserialize(const Json::Value& json) override;
	public:
		/// <value>Draw color.</value>
		glm::vec4 color{ 1 };

		/// <value>Draw texture, or none.</value>
		Ref<SubTexture> texture{ nullptr };

		/// <value>Texture tiling.</value>
		glm::vec2 tiling{ 1 };
	};
}