#pragma once

#include <list>
#include <unordered_map>
#include <string>

#include "nova/core/base.h"

namespace nova
{
	class Node;

	/// <summary>
	/// <see cref="Node"/> pool that contains like nodes.
	/// </summary>
	class NOVA_API NodePool
	{
	public:
		virtual ~NodePool() {}

		/// <summary>
		/// Create a <see cref="Node"/> within pool.
		/// </summary>
		/// <returns>Created node.</returns>
		virtual Node* create() = 0;

		/// <summary>
		/// Release a <see cref="Node"/> from this pool.
		/// </summary>
		/// <param name="mem">Node to release.</param>
		virtual void release(Node* mem) = 0;

		/// <summary>
		/// Calls <paramref name="func"/> for each <see cref="Node"/> in pool.
		/// </summary>
		/// <param name="func">Callback function with <see cref="Node"/>.</param>
		virtual void each(void(*func)(Node*)) = 0;
	};

	/// <summary>
	/// Templated <see cref="NodePool"/> made out of blocks.
	/// </summary>
	template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
	class TemplateNodePool : public NodePool
	{
		const uint32_t BLOCK_COUNT = 10;

		/// <summary>
		/// A section of <typeparamref name="T"/> in memory.
		/// </summary>
		class Block
		{
			friend class TemplateNodePool;
		public:
			/// <summary>
			/// Allocates <paramref name="count"/> elements, but does not initialize them.
			/// </summary>
			/// <param name="count">Number of elements to allocate.</param>
			Block(uint32_t count)
				: count_(count)
			{
				ptr_ = base_ = static_cast<T*>(std::malloc(count * sizeof(T)));
			}

			/// <summary>
			/// Assumes control over memory from <paramref name="base"/> of <paramref name="count"/>.
			/// </summary>
			/// <param name="base">Started of pre-allocated memory.</param>
			/// <param name="ptr">Current location within memory with initialized objects on left and unintialized here on.</param>
			/// <param name="count">The total number of elements from <paramref name="base"/>.</param>
			Block(T* base, T* ptr, uint32_t count)
				: base_(base)
				, ptr_(ptr)
				, count_(count)
			{}

			~Block()
			{
				if(owner_)
				{
					std::free(base_);
				}
			}

			Block(const Block&) = delete;
			Block& operator=(const Block&) = delete;
			Block& operator=(Block&& other) = delete;

			Block(Block&& other) noexcept
			{
				owner_ = other.owner_;
				base_ = other.base_;
				ptr_ = other.ptr_;
				count_ = other.count_;
				blocks_ = std::move(other.blocks_);

				other.base_ = nullptr;
			}

			/// <summary>
			/// Gets memory for a new object and steps, if memory is available.
			/// </summary>
			/// <returns>Usable memory, or <c>nullptr</c>.</returns>
			T* allocate()
			{
				if(ptr_ < base_ + count_)
				{
					return ptr_++;
				}

				return nullptr;
			}

			/// <summary>
			/// Frees memory from this block.
			/// </summary>
			/// <para>
			/// The memory freed will be reused. If it is at <see cref="ptr_"/> - 1, then <see cref="ptr_"/> will be stepped back.
			/// If the memory is in the middle, the block will be split. Ownership will be given to the left block, but <see cref="count_"/> will be reduced.
			/// </para>
			/// <param name="mem">Mem to free.</param>
			bool free(T* mem)
			{
				if(base_ <= mem && mem < base_ + count_)
				{
					if(mem + 1 == ptr_)
					{
						--ptr_;
					}
					else
					{
						for(auto i = blocks_->begin(); i != blocks_->end(); ++i)
						{
							if(&(*i) == this)
							{
								Block block(base_, mem, mem - base_ + 1);
								block.blocks_ = blocks_;

								blocks_->insert(i, std::move(block));
								owner_ = false;
								break;
							}
						}
					}

					return true;
				}

				return false;
			}

			/// <summary>
			/// Calls <paramref name="func"/> for each <typeparamref name="T"/> in block.
			/// </summary>
			/// <param name="func">Callback function with <see cref="Node"/>.</param>
			void each(void(*func)(Node*))
			{
				for(T* i = base_; i < ptr_; ++i)
				{
					func(i);
				}
			}
		private:
			bool owner_ = true;
			T* base_;
			T* ptr_;
			uint32_t count_;
			Ref<std::list<Block>> blocks_;
		};
	public:
		TemplateNodePool()
			: blocks_(make_ref<std::list<Block>>())
		{}

		/// <summary>
		/// Creates <see cref="Node"/> with placement new from last block, or creates one.
		/// </summary>
		/// <returns>Constructed node.</returns>
		Node* create() override
		{
			Node* mem = nullptr;

			for(Block& block : *blocks_)
			{
				if(mem = block.allocate())
				{
					break;
				}
			}

			if(!mem)
			{
				uint32_t count = (blocks_->size() + 1) * BLOCK_COUNT;

				Block block(count);
				block.blocks_ = blocks_;
				mem = block.allocate();

				blocks_->push_back(std::move(block));
			}

			return new(mem) T;
		}

		/// <summary>
		/// Destructs <see cref="Node"/> within this pool and returns memory.
		/// </summary>
		void release(Node* node) override
		{
			node->~Node();

			for(Block& block : *blocks_)
			{
				if(block.free(static_cast<T*>(node)))
				{
					break;
				}
			}
		}

		void each(void(*func)(Node*)) override
		{
			for(Block& block : *blocks_)
			{
				block.each(func);
			}
		}
	private:
		Ref<std::list<Block>> blocks_;
	};

	/// <summary>
	/// Can construct and destruct any <see cref="Node"/> type.
	/// </summary>
	class NOVA_API NodeStorage
	{
	public:
		NodeStorage();

		/// <summary>
		/// Constructs <see cref="Node"/> of type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T"><see cref="Node"/> type.</typeparam>
		/// <returns><see cref="Node"/> of type <typeparamref name="T"/>.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Node, T>::value>::type>
		T* create()
		{
			if(!pools_.count(T::NODE_NAME))
			{
				pools_.emplace(T::NODE_NAME, make_box<typename TemplateNodePool<T>>());
			}

			return static_cast<T*>(pools_[T::NAME]->create());
		}

		/// <summary>
		/// Constructs <see cref="Node"/> from <paramref name="name"/>.
		/// </summary>
		/// <param name="name">Registry name from <see cref="NodeRegistry"/>.</param>
		/// <param name="create">Create function if there is no pool of the specified type. Retrieved from <see cref="NodeRegistry"/>.</param>
		/// <returns>Generic pointer to constructed <see cref="Node"/>.</returns>
		Node* create(const std::string& name, Box<NodePool>(*create)());

		/// <summary>
		/// Deconstructs <see cref="Node"/>.
		/// </summary>
		/// <param name="node">Node to destroy.</param>
		void destroy(Node* node);
		
		/// <summary>
		/// Calls <paramref name="func"/> for each node in storage.
		/// </summary>
		/// <param name="func">Callback function with <see cref="Node"/>.</param>
		void each(void(*func)(Node*));
	private:
		Box<std::unordered_map<std::string, Box<NodePool>>> pools_;
	};
}