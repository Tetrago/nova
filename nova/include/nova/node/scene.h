#pragma once

#include "nova/core/base.h"
#include "node_storage.h"
#include "nova/resource/json.h"

namespace nova
{
	/// <summary>
	/// <see cref="Node"/> logic manager.
	/// </summary>
	class NOVA_API Scene
	{
		friend class Node;
	public:
		Scene();
		~Scene();

		void load(const Json::Value& json);
		Json::Value save() const;

		/// <summary>
		/// Calls <see cref="Node"/> awake and start methods.
		/// </summary>
		void start();

		/// <summary>
		/// Calls <see cref="Node"/> stop method.
		/// </summary>
		void stop();

		/// <summary>
		/// Calls <see cref="Node"/> update and lateUpdate methods.
		/// </summary>
		void update();

		/// <summary>
		/// Calls <see cref="Node"/> render method.
		/// </summary>
		void render();

		/// <summary>
		/// Gets the scene's root node.
		/// </summary>
		/// <returns>Generic <see cref="Node"/> pointer to the root object.</returns>
		Node* root() const { return root_; }
	private:
		/// <summary>
		/// Constructs <see cref="Node"/> by name.
		/// </summary>
		/// <param name="name">Name of <see cref="Node"/> to create.</param>
		Node* create(const std::string& name);

		/// <summary>
		/// Deconstructs <see cref="Node"/>.
		/// </summary>
		/// <param name="node"><see cref="Node"/> to destroy.</param>
		void destroy(Node* node);

		Node* root_ = nullptr;
		NodeStorage storage_{};
		time_t current_;
		double accumulator_ = 0;
	};
}