#pragma once

// core
#include "core/base.h"
#include "core/bootstrap.h"
#include "core/diagnostics.h"
#include "core/engine_prefs.h"
#include "core/event.h"
#include "core/input.h"
#include "core/log.h"
#include "core/platform.h"
#include "core/program.h"
#include "core/time.h"

// graphics
#include "graphics/buffer.h"
#include "graphics/framebuffer.h"
#include "graphics/graphics_context.h"
#include "graphics/gui.h"
#include "graphics/gui_layer.h"
#include "graphics/material.h"
#include "graphics/pipeline.h"
#include "graphics/render_device.h"
#include "graphics/renderer.h"
#include "graphics/sub_texture.h"
#include "graphics/texture.h"
#include "graphics/window.h"

// node
#include "node/basic_nodes.h"
#include "node/node.h"
#include "node/node_registry.h"
#include "node/node_storage.h"
#include "node/scene.h"

// physics
#include "physics/physics_nodes.h"
#include "physics/raycast.h"
#include "physics/world.h"

// resource
#include "resource/image.h"
#include "resource/ini.h"
#include "resource/json.h"
#include "resource/library.h"
#include "resource/package.h"
#include "resource/resource.h"
#include "resource/shader.h"