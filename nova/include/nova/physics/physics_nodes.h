#pragma once

#include "nova/node/basic_nodes.h"

class b2Shape;
class b2Body;

namespace nova
{
	/// <summary>
	/// 2D collider base class. Not used as node, merely as a generic collider.
	/// </summary>
	class NOVA_API Collider2D : public Node2D
	{
		friend class Rigidbody2D;
	protected:
		/// <summary>
		/// Retreives collider shape.
		/// </summary>
		/// <returns>Collider shape as <see cref="Shape2D"/>.</returns>
		virtual Box<b2Shape> shape() const = 0;
	};

	/// <summary>
	/// 2D box collider.
	/// </summary>
	class NOVA_API BoxCollider2D : public Collider2D
	{
		NOVA_NODE("BoxCollider2D");
	private:
		void onInspectorDraw() override;

		void serialize(Json::Value& json) const override;
		void deserialize(const Json::Value& json) override;
	protected:
		Box<b2Shape> shape() const override;
	public:
		/// <value>Extent of box in as vector.</value>
		glm::vec2 extent{ 0.5f, 0.5f };
	};

	/// <summary>
	/// 2D rigidbody that uses child <see cref="Collider2D"/>(s) to add geometry.
	/// </summary>
	class NOVA_API Rigidbody2D : public Node2D
	{
		friend class World2D;
		NOVA_NODE("Rigidbody2D");
	public:
		enum class Type
		{
			/// <value>Fully functioning physics.</value>
			Dynamic,

			/// <value><see cref="Static"/>, but moves at velocity.</value>
			Kinematic,

			/// <value>Immovable object, does not react to collisions.</value>
			Static
		};
	public:
		Rigidbody2D() {}
		~Rigidbody2D();

		Rigidbody2D(const Rigidbody2D&) = delete;
		Rigidbody2D& operator=(const Rigidbody2D&) = delete;
		Rigidbody2D(Rigidbody2D&&) = delete;
		Rigidbody2D& operator=(Rigidbody2D&&) = delete;

		void start() override;
		void stop() override;

		/// <summary>
		/// Recreates body.
		/// </summary>
		void dirty();

		void onInspectorDraw() override;

		void serialize(Json::Value& json) const override;
		void deserialize(const Json::Value& json) override;
	public:
		/// <value>Type of physics body.</value>
		Type type = Type::Dynamic;

		/// <value>Whether to freeze rotation.</value>
		bool freezeRoation = false;

		/// <value>Density of each <see cref="Shape2D"/> within.</value>
		float density = 1.0f;

		/// <value>Friction of each <see cref="Shape2D"/> within.</value>
		float friction = 0.3f;

		/// <value>Velocity of body.</value>
		glm::vec2 velocity{ 0, 0 };
	private:
		/// <summary>
		/// Updates body for physics step.
		/// </summary>
		void preStep();

		/// <summary>
		/// Updates body after physics step.
		/// </summary>
		void postStep();

		b2Body* body_ = nullptr;
	};
}