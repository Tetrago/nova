#pragma once

#include <glm/glm.hpp>

#include "nova/core/base.h"

class b2Body;
struct b2BodyDef;

namespace nova
{
	constexpr float GRAVITY = -9.8f;

	struct RaycastHit2D;

	/// <summary>
	/// 2D physics manager.
	/// </summary>
	class NOVA_API World2D
	{
		friend class Scene;
		friend class Rigidbody2D;
	public:
		/// <summary>
		/// Casts a ray within world.
		/// </summary>
		/// <param name="hit">The output hit information.</param>
		/// <param name="origin">The origin position of the ray.</param>
		/// <param name="direction">The direction to cast in.</param>
		/// <param name="distance">The distance in <paramref name="direction"/> to cast.</param>
		/// <returns>Whether an <see cref="Body2D"/> was hit.</returns>
		static bool raycast(RaycastHit2D& hit, const glm::vec2& origin, const glm::vec2& direction, float distance);
	private:
		/// <summary>
		/// Steps the world physics relative to the framerate.
		/// </summary>
		static void step(int32_t velocityIter = 6, int32_t positionIter = 2);

		/// <summary>
		/// Creates empty body.
		/// </summary>
		/// <param name="def">Body properties.</param>
		/// <returns>Constructed body.</returns>
		static b2Body* create(const b2BodyDef* def);

		/// <summary>
		/// Destroys existing body.
		/// </summary>
		/// <param name="body">Body to destroy.</param>
		static void destroy(b2Body* body);
	};
}