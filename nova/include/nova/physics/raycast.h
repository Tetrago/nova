#pragma once

#include <glm/glm.hpp>

#include "nova/core/base.h"

namespace nova
{
	/// <summary>
	/// Used to describe the hit of a raycast.
	/// </summary>
	/// <seealso cref="World2D"/>
	struct NOVA_API RaycastHit2D
	{
		/// <value>Point of collision.</value>
		glm::vec2 point{};
		
		/// <value>Distance from the raycast origin.</value>
		float distance = 0.0f;

		/// <value><see cref="Rigidbody2D"/> collided with.</value>
		Rigidbody2D* body = nullptr;
	};
}