#pragma once

#include <filesystem>

#include "nova/core/base.h"

#if defined(NOVA_API_EXPORT)
#define NOVA_EXPORT NOVA_API_EXPORT
#else
#define NOVA_EXPORT
#endif

namespace nova
{
	/// <summary>
	/// Native library handler.
	/// </summary>
	class NOVA_API Library
	{
	public:
		/// <summary>
		/// Loads library by <paramref name="name"/> at <paramref name="dir"/>.
		/// </summary>
		/// <param name="dir">Directory of library.</param>
		/// <param name="name">Name of library with no extension or prefix.</param>
		Library(const std::filesystem::path& dir, const std::string& name);
		~Library();

		/// <summary>
		/// Reloads currently specified library. Cannot change library.
		/// </summary>
		void reload();

		/// <summary>
		/// Gets a proc address from library.
		/// </summary>
		/// <param name="name">Proc address name.</param>
		/// <returns>Proc address.</returns>
		void* get(const std::string& name) const;

		/// <summary>
		/// Gets a proc address from library and casts it to <typeparamref name="T"/>.
		/// </summary>
		/// <param name="name">Proc address name.</param>
		/// <returns>Proc address.</returns>
		/// <typeparam name="T">Type to get.</typeparam>
		template<typename T>
		T get(const std::string& name) const
		{
			
			return static_cast<T>(get(name));
		}

		operator bool() { return handle_; }
	private:
		std::filesystem::path path_;
		void* handle_;
	};
}