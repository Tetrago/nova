#pragma once

#include <filesystem>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "nova/core/base.h"
#include "nova/core/diagnostics.h"
#include "nova/core/log.h"

/// <summary>
/// Specializes <see cref="nova_resource_loader"/> to support <paramref name="type"/>.
/// </summary>
/// <para>
/// Allows use of input parameter stream to return loaded <see cref="Box"/> of specified <paramref name="type"/>.
/// </para>
/// <param name="type">Type to specialize for.</param>
#define NOVA_RESOURCE_LOADER(type) template<> inline Box<type> nova_resource_loader<type>(std::istream& stream)

namespace nova
{
	/// <summary>
	/// Base class of any data that can be loaded.
	/// </summary>
	class NOVA_API Resource
	{
	public:
		virtual ~Resource() {}
	};

	/// <summary>
	/// Resource loading function.
	/// </summary>
	/// <para>
	/// Should be specialized for each type of <see cref="Resource"/>. The default function simply forwards <paramref name="stream"/>.
	/// </para>
	/// <param name="stream">Input stream.</param>
	/// <returns><see cref="Box"/> of resource <typeparamref name="T"/>.</returns>
	/// <seealso cref="NOVA_RESOURCE_LOADER"/>
	template<typename T, typename = std::enable_if<std::is_base_of<Resource, T>::value>::type>
	inline Box<T> nova_resource_loader(std::istream& stream)
	{
		return make_box<T>(stream);
	}

	/// <summary>
	/// ID given to every loaded <see cref="Resource"/>.
	/// </summary>
	/// <seealso cref="ResourceManager"/>
	using ResourceID = uint32_t;

	/// <summary>
	/// Used to location resources.
	/// </summary>
	class NOVA_API AssetPipeline
	{
	public:
		/// <summary>
		/// Used to resolve resources from a location.
		/// </summary>
		class Resolver
		{
		public:
			virtual Box<std::istream> resolve(const std::string& location) = 0;
		};
	public:
		/// <summary>
		/// Adds a <see cref="Resolver"/> to the list.
		/// </summary>
		/// <typeparam name="T">Resolver type.</typeparam>
		/// <typeparam name="Args">Arguments to resolver.</typeparam>
		template<typename T, typename... Args, typename = std::enable_if<std::is_base_of<Resolver, T>::value>::type>
		static void addResolver(Args&&... args)
		{
			resolvers_.push_back(make_box<T>(std::forward<Args>(args)...));
		}

		/// <summary>
		/// Resolves a location using list of <see cref="Resolver"/>s.
		/// </summary>
		/// <param name="location">Location to look for.</param>
		/// <returns>Input stream pointer or <c>nullptr</c>.</returns>
		static Box<std::istream> resolve(const std::string& location);
	private:
		inline static std::vector<Box<Resolver>> resolvers_;
	};

	/// <summary>
	/// Basic file system resolver.
	/// </summary>
	class NOVA_API FileResolver : public AssetPipeline::Resolver
	{
	public:
		/// <summary>
		/// Constructs resolver.
		/// </summary>
		/// <param name="base">Base path to search in.</param>
		FileResolver(const std::filesystem::path& base);

		Box<std::istream> resolve(const std::string& location) override;
	private:
		const std::filesystem::path path_;
	};

	/// <summary>
	/// Package asset resolver.
	/// </summary>
	class NOVA_API PackageResolver : public AssetPipeline::Resolver
	{
	public:
		/// <summary>
		/// Constructs resolver.
		/// </summary>
		/// <param name="package">Resource ID of package to search in.</param>
		PackageResolver(ResourceID package);

		Box<std::istream> resolve(const std::string& location) override;
	private:
		const ResourceID package_;
	};

	/// <summary>
	/// Manages all resource loading and retrieving.
	/// </summary>
	class NOVA_API ResourceManager
	{
	public:
		/// <summary>
		/// Loads resource of type <typeparamref name="T"/> using <see cref="nova_resource_loader"/>.
		/// </summary>
		/// <param name="location">Location of resource.</param>
		/// <typeparam name="T">Resource type.</typeparam>
		/// <returns><see cref="ResourceID"/> of resource.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Resource, T>::value>::type>
		static ResourceID load(const std::string& location)
		{
			auto res = map_.find(location);
			if(res != map_.end())
			{
				return res->second;
			}

			auto stream = AssetPipeline::resolve(location);
			if(!stream)
			{
				nova_logger() << Log::Error << "Failed to resolve: " << location << Log::Done;
			}

			resources_.push_back(std::move(nova_resource_loader<T>(*stream)));

			nova_logger() << Log::Trace << "Loaded resource: \"" << location << '"' << Log::Done;

			ResourceID id = resources_.size() - 1;
			map_.emplace(location, id);

			return id;
		}

		/// <summary>
		/// Gets resource from <paramref name="id"/>.
		/// </summary>
		/// <param name="id"><see cref="ResourceID"/> of data.</param>
		/// <typeparam name="T">Resource type.</typeparam>
		/// <returns>Pointer to <typeparamref name="T"/> resource.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Resource, T>::value>::type>
		static T* get(ResourceID id)
		{
			return static_cast<T*>(resources_[id].get());
		}

		/// <summary>
		/// Directly reteives resource.
		/// </summary>
		/// <param name="location">Location of resource.</param>
		/// <typeparam name="T">Resource type.</typeparam>
		/// <returns>Pointer to loaded <typeparamref name="T"/> resource.</returns>
		template<typename T, typename = std::enable_if<std::is_base_of<Resource, T>::value>::type>
		static T* quickload(const std::string& location)
		{
			return get<T>(load<T>(location));
		}
	private:
		/// <value>Map of locations to IDs.</value>
		inline static std::unordered_map<std::string, ResourceID> map_;

		/// <value>List of resources with IDs as indexes.</value>
		inline static std::vector<Box<Resource>> resources_;
	};
}