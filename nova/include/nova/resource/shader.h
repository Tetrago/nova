#pragma once

#include "resource.h"
#include "package.h"
#include "nova/graphics/pipeline.h"

namespace nova
{
	/// <summary>
	/// Collection of shader stag.es
	/// </summary>
	/// <para>
	/// Must come with Spirv stages and/or language specific shaders.
	/// Each Sstage in the package will be named like: [lanugage].[stage]
	/// 
	/// Stages:
	/// <list type="table">
	/// <listheader>
	/// <term>Name</term>
	/// <term>Abbreviation</term>
	/// </listheader>
	/// <item>
	/// <term>Vertex</term>
	/// <term>vert</term>
	/// </item>
	/// <item>
	/// <term>Fragment</term>
	/// <term>frag</term>
	/// </item>
	/// </list>
	/// 
	/// Languages:
	/// <list type="table">
	/// <listheader>
	/// <term>Name</term>
	/// <term>Abbreviation</term>
	/// </listheader>
	/// <item>
	/// <term>Spirv</term>
	/// <term>spv</term>
	/// </item>
	/// <item>
	/// <term>Glsl</term>
	/// <term>glsl</term>
	/// </item>
	/// </list>
	/// </para>
	class NOVA_API Shader : public Resource
	{
	public:
		enum class Language
		{
			Glsl,
			Spirv
		};

		enum class Stage
		{
			Vertex,
			Fragment
		};

		struct NOVA_API Module
		{
			Stage stage;
			std::string source;
		};

		struct NOVA_API Uniform
		{
			uint32_t offset;
			size_t size;
		};

		using Group = std::vector<Module>;
		using UniformMap = std::unordered_map<std::string, Uniform>;
		using TextureMap = std::unordered_map<std::string, int32_t>;
	public:
		Shader(const Package& package);

		const Group& operator[](const Language& language);

		const std::vector<LayoutElement>& elements() const { return elements_; }
		const UniformMap& uniforms() const { return uniforms_; }
		const TextureMap& textures() const { return textures_; }
	private:
		/// <summary>
		/// Fills element, uniform, and texture data from reflection.
		/// </summary>
		void populateStorage();

		/// <summary>
		/// Compiles <paramref name="lanuage"/> from Spirv.
		/// </summary>
		/// <param name="lanugage"><see cref="Language"/> to compile to.</param>
		/// <returns><see cref="Group"/> of compiled code.</returns>
		const Group& compile(const Language& language);

		std::unordered_map<Language, Group> groups_;
		std::vector<LayoutElement> elements_;
		UniformMap uniforms_;
		TextureMap textures_;
	};

	NOVA_RESOURCE_LOADER(Shader)
	{
		Package package{};
		package.unpack(stream);

		return make_box<Shader>(package);
	}
}