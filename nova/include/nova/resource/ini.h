#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include "resource.h"

namespace nova
{
	/// <summary>
	/// Stores Ini file properties.
	/// </summary>
	class NOVA_API Ini : public Resource
	{
	public:
		/// <summary>
		/// Stores properties of a section within the Ini data.
		/// </summary>
		struct NOVA_API Section
		{
			/// <summary>
			/// Checks if the data has a property.
			/// </summary>
			/// <param name="name">Property to check for.</param>
			/// <returns>If <paramref name="name"/> exists.</returns>
			bool has(const std::string& name) const
			{
				return values.count(name);
			}

			template<typename T>
			void set(const std::string& name, const T& value)
			{
				values[name] = std::to_string(value);
			}

			template<>
			void set<std::string>(const std::string& name, const std::string& value)
			{
				values[name] = value;
			}

			template<>
			void set<uint32_t>(const std::string& name, const uint32_t& value)
			{
				values[name] = std::to_string((unsigned)value);
			}

			template<typename T>
			T get(const std::string& name)
			{
				static_assert(false, "Invalid type to get");
			}

#define GET(x) template<> x get<x>(const std::string& name)

			GET(std::string)
			{
				return values[name];
			}

			GET(int)
			{
				return std::stoi(values[name]);
			}

			GET(float)
			{
				return std::stof(values[name]);
			}

			GET(double)
			{
				return std::stod(values[name]);
			}

			GET(long)
			{
				return std::stol(values[name]);
			}

			GET(uint32_t)
			{
				return std::stoi(values[name]);
			}

#undef GET

			auto begin() { return values.begin(); }
			auto end() { return values.end(); }
			auto begin() const { return values.begin(); }
			auto end() const { return values.end(); }
		private:
			std::unordered_map<std::string, std::string> values;
		};
	public:
		void load(std::istream& stream);
		void save(std::ostream& stream);

		/// <summary>
		/// Checks if the data has a section.
		/// </summary>
		/// <param name="section">Section to check for.</param>
		/// <returns>If <paramref name="section"/> exists.</returns>
		bool has(const std::string& section) const
		{
			return map_.count(section);
		}

		/// <summary>
		/// Gets a section.
		/// </summary>
		/// <param name="section">Name of section.</param>
		/// <returns>Found or empty <see cref="Section"/>.</returns>
		Section& operator[](const std::string& section)
		{
			return map_[section];
		}

		auto begin() { return map_.begin(); }
		auto end() { return map_.end(); }
		auto begin() const { return map_.begin(); }
		auto end() const { return map_.end(); }
	private:
		std::unordered_map<std::string, Section> map_;
	};

	NOVA_RESOURCE_LOADER(Ini)
	{
		Box<Ini> ini = make_box<Ini>();
		ini->load(stream);

		return ini;
	}
}