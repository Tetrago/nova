#pragma once

#include <nlohmann/json.hpp>
#include <glm/glm.hpp>
#include <string>

#include "resource.h"

namespace nlohmann
{
	template<>
	struct adl_serializer<glm::vec2>
	{
		static void to_json(json& j, const glm::vec2& v)
		{
			j["x"] = v.x;
			j["y"] = v.y;
		}

		static void from_json(const json& j, glm::vec2& v)
		{
			j.at("x").get_to(v.x);
			j.at("y").get_to(v.y);
		}
	};

	template<>
	struct adl_serializer<glm::vec3>
	{
		static void to_json(json& j, const glm::vec3& v)
		{
			j["x"] = v.x;
			j["y"] = v.y;
			j["z"] = v.z;
		}

		static void from_json(const json& j, glm::vec3& v)
		{
			j.at("x").get_to(v.x);
			j.at("y").get_to(v.y);
			j.at("z").get_to(v.z);
		}
	};

	template<>
	struct adl_serializer<glm::vec4>
	{
		static void to_json(json& j, const glm::vec4& v)
		{
			j["x"] = v.x;
			j["y"] = v.y;
			j["z"] = v.z;
			j["w"] = v.w;
		}

		static void from_json(const json& j, glm::vec4& v)
		{
			j.at("x").get_to(v.x);
			j.at("y").get_to(v.y);
			j.at("z").get_to(v.z);
			j.at("w").get_to(v.w);
		}
	};
}

namespace nova
{
	class Json : public Resource
	{
	public:
		using Value = nlohmann::json;
	public:
		Json(std::istream& stream)
		{
			value_ << stream;
		}

		Value& value() { return value_; }
		const Value& value() const { return value_; }
	private:
		Value value_;
	};

	NOVA_RESOURCE_LOADER(Json)
	{
		return make_box<Json>(stream);
	}
}