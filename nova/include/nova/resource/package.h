#pragma once

#include "nova/core/log.h"
#include "resource.h"

namespace nova
{
	/// <summary>
	/// Holds a collection of files.
	/// </summary>
	class NOVA_API Package : public Resource
	{
	public:
		/// <summary>
		/// Unpacks data into map from <paramref name="stream"/>.
		/// </summary>
		/// <param name="stream">Input stream.</param>
		void unpack(std::istream& stream);

		/// <summary>
		/// Packs data into string.
		/// </summary>
		/// <returns>Packed files from map.</returns>
		std::string pack() const;

		/// <summary>
		/// Gets file from <paramref name="key"/>.
		/// </summary>
		/// <param name="key">File name.</param>
		std::string& operator[](const std::string& key);

		auto begin() { return data_.begin(); }
		auto end() { return data_.end(); }
		auto begin() const { return data_.cbegin(); }
		auto end() const { return data_.cend(); }
	private:
		std::unordered_map<std::string, std::string> data_;
	};

	NOVA_RESOURCE_LOADER(Package)
	{
		Box<Package> package = make_box<Package>();
		package->unpack(stream);
		return package;
	}
}