#pragma once

#include "resource.h"

namespace nova
{
	/// <summary>
	/// Stores raw image data.
	/// </summary>
	class NOVA_API Image : public Resource
	{
	public:
		Image(std::istream& stream);
		~Image();

		Image(const Image&) = delete;
		Image& operator=(const Image&) = delete;
		Image(Image&&) = delete;
		Image& operator=(Image&&) = delete;

		uint32_t width() const { return width_; }
		uint32_t height() const { return height_; }
		uint32_t channels() const { return channels_; }
		void* data() const { return data_; }
	private:
		uint32_t width_, height_, channels_;
		void* data_;
	};

	NOVA_RESOURCE_LOADER(Image)
	{
		return make_box<Image>(stream);
	}
}