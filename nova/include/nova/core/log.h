#pragma once

#include <functional>
#include <sstream>
#include <unordered_map>

#include "base.h"
#include "event.h"

#if NOVA_ASSERT
/// <summary>
/// Checks if expression evaluates to true at runtime, then logs a message.
/// </summary>
/// <param name="exp">The expression to evaluate.</param>
#define NOVA_CORE_ASSERT(exp, ...) if(!(exp)) { ::nova::nova_logger() << ::nova::Log::Fatal << "Assert failed: " << __VA_ARGS__ << ::nova::Log::Done; }
#else
#define  NOVA_CORE_ASSERT(exp, ...)
#endif

namespace nova
{
	/// <summary>
	/// Logging level.
	/// </summary>
	enum class Log
	{
		/// <summary>Debug</summary>
		Trace,

		/// <summary>General</summary>
		Info,

		/// <summary>Warning</summary>
		Warn,

		/// <summary>Non-Critical Error</summary>
		Error,

		/// <summary>Terminating Error</summary>
		Fatal,

		/// <summary>End of Message</summary>
		Done
	};

	/// <summary>
	/// Log event used in <see cref="Event"/>
	/// </summary>
	struct NOVA_API LogEvent
	{
		NOVA_EVENT("Log")

		/// <value>Name of process attempting to log.</value>
		std::string name;

		/// <value>Level to log at.</value>
		Log log;

		/// <value>Message to log.</value>
		std::string msg;
	};

	/// <summary>
	/// The main logging method used by Nova.
	/// </summary>
	class NOVA_API Logger
	{
	public:
		/// <summary>
		/// Constructs the <see cref="Logger"/>
		/// </summary>
		/// <param name="name">Name of process that is shown on output.</param>
		Logger(const std::string& name)
			: name_(name)
			, log_(Log::Error)
		{}

		/// <summary>
		/// Logs any type except <see cref="Log"/>, which sets the log level.
		/// </summary>
		/// <param name="val">Value to log.</param>
		/// <typeparam name="T">Type of data to log. <see cref="Log"/> is specialized.</typeparam>
		template<typename T>
		Logger& operator<<(const T& val)
		{
			LogEvent e;
			stream_ << val;
			return *this;
		}

		template<>
		Logger& operator<<<Log>(const Log& log)
		{
			if(log == Log::Done)
			{
				EventBus::emit<LogEvent>({ name_, log_, stream_.str() });
				stream_.str(" ");

				if(log_ == Log::Fatal)
				{
					throw std::exception("Logged fatal error");
				}
			}

			log_ = log;
			return *this;
		}

		const std::string& name() const { return name_; }
	private:
		std::string name_;
		Log log_;
		std::ostringstream stream_;
	};

	/// <summary>
	/// Gets main Nova <see cref="Logger"/>. This is for core logging, <see cref="Program"/>'s logger is used for client logging.
	/// </summary>
	inline Logger& nova_logger()
	{
		static Logger logger{ "nova" };
		return logger;
	}
}