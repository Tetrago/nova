#pragma once

#include <filesystem>
#include <cstdint>
#include <fstream>

#include "base.h"
#include "nova/resource/ini.h"

namespace nova
{
	/// <summary>
	/// Stores persistent data on engine preferences.
	/// </summary>
	/// <para>
	/// Storage includes:
	/// <list>
	/// <item>Window Position</item>
	/// <item>Window Size</item>
	/// <item>Window Options</item>
	/// </list>
	/// </para>
	class NOVA_API EnginePrefs
	{
	private:
		inline static const std::filesystem::path PATH = "nova.ini";

		struct NOVA_API Storage
		{
			Storage()
			{
				std::ifstream file{ PATH };
				if(file.is_open())
				{
					ini_.load(file);
				}
			}

			~Storage()
			{
				std::ofstream file{ PATH };
				if(file.is_open())
				{
					ini_.save(file);
				}
			}

			bool has(const std::string& section) const
			{
				return ini_.has(section);
			}

			Ini::Section& operator[](const std::string& section)
			{
				return ini_[section];
			}
		private:
			Ini ini_;
		};

		static Storage& get()
		{
			static Storage instance;
			return instance;
		}

		template<typename T, T Default, typename = std::enable_if<std::is_pod<T>::value>::type>
		class Property
		{
		public:
			Property(const std::string& section, const std::string& name)
				: section_(section)
				, name_(name)
			{
				if(!(EnginePrefs::get().has(section) && EnginePrefs::get()[section].has(name)))
				{
					EnginePrefs::get()[section].set(name, Default);
				}
			}

			void set(const T& value)
			{
				EnginePrefs::get()[section_].set(name_, value);
			}

			T get() const
			{
				return EnginePrefs::get()[section_].get<T>(name_);
			}
		private:
			std::string section_;
			std::string name_;
		};
	public:
		inline static Property<uint32_t, 50> DISPLAY_X{ "display", "x" };
		inline static Property<uint32_t, 50> DISPLAY_Y{ "display", "y" };
		inline static Property<uint32_t, 1024> DISPLAY_WIDTH{ "display", "width" };
		inline static Property<uint32_t, 576> DISPLAY_HEIGHT{ "display", "height" };
		inline static Property<int, 0> DISPLAY_MAXIMIZED{ "display", "maximized" };
	};
}