#pragma once

#include "base.h"
#include "diagnostics.h"
#include "program.h"

#define NOVA_BOOTSTRAP(type, ...) int main(int argc, char** argv) { return ::nova::Bootstrap::process<type>(__VA_ARGS__, argc, argv); }

namespace nova
{
	class NOVA_API Bootstrap
	{
	public:
		/// <summary>
		/// Runs a program.
		/// </summary>
		/// <param name="info">Program specific info.</param>
		/// <param name="argc">Argument count.</param>
		/// <param name="argv">Arguments.</param>
		/// <typeparam name="T">Program type.</typeparam>
		template<typename T, typename = std::enable_if<std::is_base_of<Program, T>::value>::type>
		static int process(const ProgramInfo& info, int argc, char** argv)
		{
			setupLogging();

			NOVA_DIAG_BEGIN("nova_startup.json");
			auto program = Program::create<T>(parseArguments(argc, argv), info);
			NOVA_DIAG_END();

			NOVA_DIAG_BEGIN("nova_runtime.json");
			program->start();
			NOVA_DIAG_END();

			NOVA_DIAG_BEGIN("nova_shutdown.json");
			delete program;
			NOVA_DIAG_END();

			return 0;
		}
	private:
		/// <summary>
		/// Parses command line arguments.
		/// </summary>
		/// <param name="argc">Argument count.</param>
		/// <param name="argv">Arguments.</param>
		/// <returns>Constructed <see cref="EngineInfo"/>.</returns>
		static EngineInfo parseArguments(int argc, char** argv);

		/// <summary>
		/// Sets up <see cref="LogEvent"/> listener.
		/// </summary>
		static void setupLogging();
	};
}