#pragma once

#include "base.h"
#include "log.h"
#include "nova/graphics/render_device.h"
#include "nova/node/node_registry.h"

/// <summary>
/// Gets program and begins logging.
/// </summary>
/// <param name="x">Log level as <see cref="Log"/>.</param>
#define NOVA_PROGRAM_LOG(level, ...) ::nova::Program::get().logger() << (level) << __VA_ARGS__ << ::nova::Log::Done

namespace nova
{
	class Window;
	struct WindowEvent;
	class RenderDevice;

	struct NOVA_API ProgramInfo
	{
		std::string name = "nova";
		Version version{};
	};

	struct NOVA_API EngineInfo
	{
		PlatformApi platformApi = PlatformApi::None;
		RendererApi rendererApi = RendererApi::None;
	};

	class NOVA_API Program
	{
	public:
		virtual ~Program();

		Program(const Program&) = delete;
		Program& operator=(const Program&) = delete;
		Program(Program&&) = delete;
		Program& operator=(Program&&) = delete;

		/// <summary>
		/// Starts program.
		/// </summary>
		virtual void start();

		/// <summary>
		/// Stops program.
		/// </summary>
		virtual void stop();

		const ProgramInfo& programInfo() const { return programInfo_; }
		const EngineInfo& engineInfo() const { return engineInfo_; }
		RenderDevice& renderDevice() const { return *renderDevice_; }
		Window& window() { return *window_; }
		Logger& logger() { return *logger_; }

		/// <summary>
		/// Gets program instance.
		/// </summary>
		/// <returns>Instance.</returns>
		/// <remarks>
		/// Set on the creation of a program.
		/// </remarks>
		static Program& get();

		/// <summary>
		/// Constructs a program using info.
		/// </summary>
		/// <param name="engineInfo">Engine information.</param>
		/// <param name="programInfo">Program information.</param>
		template<typename T, typename = std::enable_if<std::is_base_of<Program, T>::value>::type>
		static Program* create(const EngineInfo& engineInfo, const ProgramInfo& programInfo)
		{
			NOVA_CORE_ASSERT(!instance_, "Attempting to redefine program instance");

			instance_ = new T(engineInfo, programInfo);
			instance_->setup();
			return instance_;
		}
	protected:
		/// <summary>
		/// Sets up program graphics.
		/// </summary>
		void setupGraphics();

		virtual void setup() = 0;
		virtual void tick() = 0;
	private:
		Program(const EngineInfo& engineInfo, const ProgramInfo& programInfo);

		const ProgramInfo programInfo_;
		const EngineInfo engineInfo_;

		Box<RenderDevice> renderDevice_;
		Box<Window> window_;
		Box<Logger> logger_;
		bool running_;
		bool paused_ = false;

		static Program* instance_;
	};
}