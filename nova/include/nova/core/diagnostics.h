#pragma once

#include <mutex>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <string>

#include "base.h"

#if NOVA_DIAG
#define NOVA_DIAG_BEGIN(path) ::nova::diag::Instrumentor::get().begin(path)
#define NOVA_DIAG_END() ::nova::diag::Instrumentor::get().end()
#define NOVA_DIAG_SCOPE(name) ::nova::diag::ScopeInstrumentor session##__LINE__{ name }
#define NOVA_DIAG_FUNCTION() NOVA_DIAG_SCOPE(__FUNCSIG__)
#else
#define NOVA_DIAG_BEGIN(name, path)
#define NOVA_DIAG_END()
#define NOVA_DIAG_SCOPE(name)
#define NOVA_DIAG_FUNCTION()
#endif

namespace nova
{
	namespace diag
	{
		/// <summary>
		/// Manages diagnostics logging.
		/// </summary>
		/// <remarks>
		/// All relevant methods are threadsafe.
		/// </remarks>
		class NOVA_API Instrumentor
		{
		public:
			/// <summary>
			/// Holds information about an instrumentor sesson.
			/// </summary>
			struct Session
			{
				std::string name;
			};

			/// <summary>
			/// Holds information about a particular timeframe.
			/// </summary>
			struct Result
			{
				std::string name;
				time_t start;
				time_t end;
			};
		public:
			/// <summary>
			/// Starts session at <paramref name="path"/>.
			/// </summary>
			/// <param name="path">Path of new session.</param>
			/// <remarks>
			/// Will close a pre-existing session.
			/// </remarks>
			void begin(const std::filesystem::path& path);

			/// <summary>
			/// Ends current session.
			/// </summary>
			/// <remarks>
			/// Will be ignored if there is no current session.
			/// </remarks>
			void end();

			/// <summary>
			/// Writes to session from <paramref name="result"/>.
			/// </summary>
			/// <param name="result">Result to write information from.</param>
			void write(const Result& result);

			/// <summary>
			/// Gets <see cref="Instrumentor"/> instance.
			/// </summary>
			/// <returns>Instance.</returns>
			static Instrumentor& get();
		private:
			/// <summary>
			/// Writes Json header.
			/// </summary>
			void writeHeader();

			/// <summary>
			/// Writes Json footer.
			/// </summary>
			void writeFooter();

			int count_ = 0;
			std::ofstream output_;
			std::mutex mutex_;
		};

		/// <summary>
		/// Logs to the <see cref="Instrumentor"/> on destruction.
		/// </summary>
		class NOVA_API ScopeInstrumentor
		{
		public:
			ScopeInstrumentor(const char* name);
			~ScopeInstrumentor();
		private:
			const char* const name_;
			std::chrono::time_point<std::chrono::high_resolution_clock> startTimePoint_;
		};
	}
}