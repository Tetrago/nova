#pragma once

#include <unordered_map>
#include <functional>
#include <mutex>

#include "base.h"
#include "diagnostics.h"

/// <summary>
/// Used in <see cref="Event"/> declaration to properly use it.
/// </summary>
#define NOVA_EVENT(x) static constexpr const char* EVENT_NAME = x;

namespace nova
{
	/// <summary>
	/// Event bus.
	/// </summary
	class NOVA_API EventBus
	{
		struct NOVA_API CallbackList
		{
			virtual ~CallbackList() {}
		};

		template<typename T>
		struct TemplateCallbackList : public CallbackList
		{
			using Fn = typename std::function<void(const T&)>;
			std::vector<Fn> functions;
		};
	public:
		/// <summary>
		/// Adds <paramref name="callback"/> to listeners list, of which will be called upon event emission.
		/// </summary>
		/// <param name="callback">Event value callback.</param>
		/// <typeparam name="T">Event type.</typeparam>
		template<typename T>
		static void subscribe(const typename TemplateCallbackList<T>::Fn& callback)
		{
			NOVA_DIAG_FUNCTION();
			const std::lock_guard<std::mutex> lock(mutex_);
			
			if(!listeners_.count(T::EVENT_NAME))
			{
				listeners_.emplace(T::EVENT_NAME, new typename TemplateCallbackList<T>());
			}

			static_cast<typename TemplateCallbackList<T>*>(listeners_[T::EVENT_NAME])->functions.push_back(callback);
		}

		/// <summary>
		/// Emits event data.
		/// </summary>
		/// <param name="t">Event data.</param>
		/// <typeparam name="T">Event type.</typeparam>
		template<typename T>
		static void emit(const T& e)
		{
			NOVA_DIAG_FUNCTION();

			if(listeners_.count(T::EVENT_NAME))
			{
				for(const auto& func : static_cast<typename TemplateCallbackList<T>*>(listeners_[T::EVENT_NAME])->functions)
				{
					func(e);
				}
			}
		}
	private:
		static std::mutex mutex_;
		static std::unordered_map<const char*, CallbackList*> listeners_;
	};
}