#pragma once

#include <cstdint>
#include <string>
#include <memory>

#if defined(_WIN32)

/// <summary>
/// Defined when built on a windows device.
/// </summary>
#define NOVA_PLATFORM_WIN
#define NOVA_API_EXPORT __declspec(dllexport)
#define NOVA_API_IMPORT __declspec(dllimport)

#elif defined(__linux__)

/// <summary>
/// Defined when built on a linux device.
/// </summary>
#define NOVA_PLATFORM_LINUX
#define NOVA_API_EXPORT __attribute__((visibility("default")))

#else
#error "Currently unsupport platform detected"
#endif

#if defined(NOVA_SHARED_LIB)

#if defined(NOVA_API_EXPORT) && defined(NOVA_BUILD)
#define NOVA_API NOVA_API_EXPORT
#elif defined(NOVA_API_IMPORT)
#define NOVA_API NOVA_API_IMPORT
#endif

#endif

#if !defined(NOVA_API)
#define NOVA_API
#endif

#if !defined(NDEBUG)
/// <summary>
/// Defined when the program is in debug mode.
/// </summary>
#define NOVA_DEBUG
#endif

#if defined(NOVA_DEBUG)
/// <summary>
/// Is enabled when asserts are enabled.
/// </summary>
#define NOVA_ASSERT 1

/// <summary>
/// Is enabled when diagnostics are enabled.
/// </summary>
#define NOVA_DIAG 1
#else
#define NOVA_ASSERT 0
#define NOVA_DIAG 0
#endif

/// <summary>
/// Gets number with bit at <paramref name="x"/> set.
/// </summary>
/// <param name="x">Bit to set.</param>
/// <returns>Number with only bit <paramref name="x"/> set.</returns>
#define BIT(x) (1 << (x))

namespace nova
{
	/// <summary>
	/// Version triple.
	/// </summary>
	struct NOVA_API Version
	{
		uint32_t major, minor, patch;
	};

	/// <summary>
	/// Graphics APIs: drawing and displaying.
	/// </summary>
	enum class RendererApi
	{
		None,

		/// <value>OpenGL Core</value>
		OpenGL
	};

	/// <summary>
	/// Platform APIs: windows, input, etc.
	/// </summary>
	enum class PlatformApi
	{
		None,

		/// <value>Glfw library, non-native Api.</value>
		Glfw,

		/// <value>Win32 api, windows native.</value>
		Win32
	};


	/// <summary>
	/// Reference counted pointer. Created with <see cref="make_ref"/>.
	/// </summary>
	/// <typeparam name="T">Type of reference.</typeparam>
	template<typename T>
	using Ref = std::shared_ptr<T>;

	/// <summary>
	/// Creates a <see cref="Ref"/> of type <typeparamref name="T"/>.
	/// </summary>
	/// <param name="args"><typeparamref name="T"/> constructor arguments.</param>
	/// <typeparam name="T">Reference type.</typeparam>
	/// <typeparam name="Args">Variadic arguments for <typeparamref name="T"/></typeparam>
	/// <returns>Created <see cref="Ref"/> of type <typeparamref name="T"/></returns>
	template<typename T, typename... Args>
	inline Ref<T> make_ref(Args&&... args)
	{
		return std::make_shared<T>(std::forward<Args>(args)...);
	}

	/// <summary>
	/// Casts <typeparamref name="ref"/> from type <typeparamref name="U"/> to type <typeparamref name="T"/>.
	/// </summary>
	/// <param name="ref">Reference to cast.</param>
	/// <typeparam name="T">Type to cast to.</typeparam>
	/// <typeparam name="U">Type to cast from.</typeparam>
	/// <returns>Casted <see cref="ref"/> of type <typeparamref name="T"/></returns>
	template<typename T, typename U>
	inline Ref<T> cast_ref(const Ref<U>& ref)
	{
		return std::dynamic_pointer_cast<T>(ref);
	}

	/// <summary>
	/// Memory managed pointer. Cannot be copied. Created with <see cref="make_box"/>.
	/// </summary>
	/// <typeparam name="T">Type of pointer.</typeparam>
	template<typename T>
	using Box = std::unique_ptr<T>;

	/// <summary>
	/// Creates a <see cref="Box"/> of type <typeparamref name="T"/>.
	/// </summary>
	/// <param name="args"><typeparamref name="T"/> constructor arguments.</param>
	/// <typeparam name="T">Pointer type.</typeparam>
	/// <typeparam name="Args">Variadic arguments for <typeparamref name="T"/></typeparam>
	/// <returns>Created <see cref="Box"/> of type <typeparamref name="T"/></returns>
	template<typename T, typename... Args>
	inline Box<T> make_box(Args&&... args)
	{
		return std::make_unique<T>(std::forward<Args>(args)...);
	}
}