#pragma once

#include <optional>
#include <filesystem>
#include <vector>

#include "nova/core/base.h"
#include "nova/core/event.h"

namespace nova
{
	class Window;

	class Platform
	{
	public:
		/// <summary>
		/// System monitor.
		/// </summary>
		struct Monitor
		{
			int32_t x, y;
			uint32_t w, h;

			struct
			{
				int32_t x, y;
				uint32_t w, h;
			} workarea;

			float dpi;
		};

		/// <summary>
		/// Emitted on a change in the monitor setup.
		/// </summary>
		struct MonitorEvent
		{
			NOVA_EVENT("Monitor");
			std::vector<Monitor> monitors;
		};
	public:
		/// <summary>
		/// Fetches the system's monitor information.
		/// </summary>
		/// <returns>Vector of <see cref="Monitor"/>s.</returns>
		virtual std::vector<Monitor> monitors() const = 0;

		/// <summary>
		/// Opens an open file dialogue.
		/// </summary>
		/// <param name="filter">Allowed file extensions.</param>
		/// <returns>Selected file location, if successful.</returns>
		virtual std::optional<std::filesystem::path> openFileDialogue(const Window& window, const char* filter) const = 0;

		/// <summary>
		/// Opens a save file dialogue.
		/// </summary>
		/// <param name="filter">Allowed file extensions.</param>
		/// <returns>Selected file location, if successful.</returns>
		virtual std::optional<std::filesystem::path> saveFileDialogue(const Window& window, const char* filter) const = 0;

		/// <summary>
		/// Gets the current platform from the <see cref="Program"/>'s <see cref="PlatformApi"/>.
		/// </summary>
		/// <returns>Instance of the platform.</returns>
		static Platform& get();
	};
}