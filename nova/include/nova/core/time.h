#pragma once

#include <chrono>

#include "base.h"

namespace nova
{
	class NOVA_API Time
	{
		friend class Program;
		using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
	public:
		/// <summary>
		/// Gets the current time.
		/// </summary>
		/// <returns>Current time.</returns>
		static time_t time();

		/// <summary>
		/// Gets the time since the last frame.
		/// </summary>
		/// <returns>Delta time.</returns>
		static double deltaTime() { return dt_; }

		/// <summary>
		/// Gets the fixed time in between fixedUpdates.
		/// </summary>
		/// <returns>Fixed delta time.</returns>
		constexpr static double fixedDeltaTime() { return 1.0 / 120; }

		/// <summary>
		/// Gets the current framerate.
		/// </summary>
		/// <returns>Framerate.</returns>
		static int frameRate() { return frameRate_; }
	private:
		static void tick();

		static double dt_;
		static int frameRate_;

		static TimePoint lastFrame_;
		static TimePoint lastSecond_;
		static int counter_;
	};
}