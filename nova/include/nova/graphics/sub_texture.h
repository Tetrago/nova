#pragma once

#include <array>
#include <glm/vec2.hpp>

#include "nova/core/base.h"
#include "texture.h"

namespace nova
{
	/// <summary>
	/// Holds a portion of a <see cref="Texture"/>.
	/// </summary>
	class NOVA_API SubTexture
	{
	public:
		/// <summary>
		/// Constructs a subtexture around a <see cref="Texture"/>.
		/// </summary>
		/// <param name="texture">Base texture.</param>
		/// <param name="uv0">Bottom-left UV position.</param>
		/// <param name="uv1">Top-right UV position.</param>
		SubTexture(const Ref<Texture>& texture, const glm::vec2 uv0 = { 0, 0 }, const glm::vec2& uv1 = { 1, 1 });


		const Texture& base() const { return *texture_; }

		/// <summary>
		/// Gets UV coordinates for quad vertices in a counterclockwise direction starting from the bottom-left.
		/// </summary>
		/// <returns>Array of UV coordinates.</returns>
		const std::array<glm::vec2, 4>& uv() const { return uv_; }

		static Ref<SubTexture> from(const Ref<Texture>& texture) { return make_box<SubTexture>(texture); }
		static Ref<SubTexture> from(const Ref<Texture>& texture, const glm::vec2& pos, const glm::vec2& size);
	private:
		const Ref<Texture> texture_;
		std::array<glm::vec2, 4> uv_;
	};
}