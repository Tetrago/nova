#pragma once

#include "nova/core/base.h"

namespace nova
{
	struct ProgramInfo;

	class Buffer;
	struct BufferCreateInfo;

	class Framebuffer;
	struct FramebufferCreateInfo;

	class Pipeline;
	struct PipelineCreateInfo;

	class VertexArray;
	struct VertexArrayCreateInfo;

	class GraphicsContext;
	class Window;

	class Material;
	class Texture;

	/// <summary>
	/// Serves as the method of creating Api objects. Instantiated in <see cref="Program"/>.
	/// </summary>
	class NOVA_API RenderDevice
	{
	public:
		virtual ~RenderDevice() {}

		virtual Ref<Buffer> createBuffer(const BufferCreateInfo& info) = 0;
		virtual Ref<Framebuffer> createFrambuffer(const FramebufferCreateInfo& info) = 0;
		virtual Box<GraphicsContext> createGraphicsContext(Window* window) = 0;
		virtual Ref<Material> createMaterial(const Ref<Pipeline>& pipeline) = 0;
		virtual Ref<Pipeline> createPipeline(const PipelineCreateInfo& info) = 0;
		virtual Ref<Texture> createTexture() = 0;

		/// <summary>
		/// Creates a render device based on the <see cref="RendererApi"/>.
		/// </summary>
		/// <returns><see cref="Ref"/> to a platform agnostic render device.</returns>
		static Box<RenderDevice> create();
	};
}