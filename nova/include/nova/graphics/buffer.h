#pragma once

#include <cstdint>
#include <unordered_set>

namespace nova
{
	enum class BufferView
	{
		Array,
		ElementArray,
		Uniform
	};

	enum class BufferUsage
	{
		Static,
		Dynamic
	};

	struct BufferCreateInfo
	{
		void* data = nullptr;
		size_t size;
		std::unordered_set<BufferView> views;
		BufferUsage usage = BufferUsage::Static;
	};

	/// <summary>
	/// Graphics buffer created by <see cref="RenderDevice"/>.
	/// </summary>
	class Buffer
	{
	public:
		virtual ~Buffer() {}

		/// <summary>
		/// Loads data into pre-existing buffer, preserving any unwritten data.
		/// </summary>
		/// <param name="data">Data to load.</param>
		/// <param name="size">Size of data to load in bytes.</param>
		/// <param name="offset">Offset of data within array in bytes.</param>
		virtual void load(const void* data, size_t size, uint32_t offset = 0) = 0;

		/// <summary>
		/// Gets the size of the buffer.
		/// </summary>
		/// <returns>Size in bytes.</returns>
		virtual size_t size() const = 0;
	};
}