#pragma once

#include <stack>
#include "glm/glm.hpp"

#include "nova/core/base.h"

namespace nova
{
	class GraphicsContext;
	class Material;
	class SubTexture;

	/// <summary>
	/// Stack of <see cref="GraphicsContext"/> pointers that are used by the renderers.
	/// </summary>
	/// <para>
	/// The purpose is to ensure that the engine could support working on multiple windows at the same time.
	/// Each <see cref="Window"/> has its own <see cref="GraphicsContext"/>, which is used to perform Api instructions.
	/// By having a stack, draw commands can go to the top (the currently focued window).
	/// </para>
	class NOVA_API RenderStack
	{
	public:
		/// <summary>
		/// Pushes a <see cref="GraphicsContext"/> to the top of the stack.
		/// </summary>
		/// <param name="ctx">Context to add.</param>
		static void push(GraphicsContext* ctx);

		/// <summary>
		/// Pops a <see cref="GraphicsContext"/> from the top of the stack.
		/// </summary>
		static void pop();

		/// <summary>
		/// Gets the <see cref="GraphicsContext"/> on the top of stack.
		/// </summary>
		static GraphicsContext* top() { return contexts_.top(); }
	private:
		static std::stack<GraphicsContext*> contexts_;
	};

	class NOVA_API Renderer2D
	{
		friend class Program;
	public:
		struct NOVA_API Statistics
		{
			uint32_t drawCalls = 0;
			uint32_t quadCount = 0;
		};
	public:
		/// <summary>
		/// Used by 2d render calls to place draws.
		/// </summary>
		struct DrawDesc
		{
			glm::vec3 position{ 0 };
			float rotation = 0;
			glm::vec2 size{ 1, 1 };
			glm::vec2 tiling{ 1, 1 };
		};
	public:
		/// <summary>
		/// Prepairs the renderer for the environment (<paramref name="matrix"/>).
		/// </summary>
		static void prepair(const glm::mat4& matrix);

		/// <summary>
		/// Draws and flushes the current render batch.
		/// </summary>
		static void flush();

		/// <summary>
		/// Draws colored quad.
		/// </summary>
		/// <param name="color">Draw color.</param>
		/// <param name="desc">Transform information.</param>
		static void drawQuad(const glm::vec4& color, const DrawDesc& desc = {});

		/// <summary>
		/// Draws textured quad.
		/// </summary>
		/// <param name="texture"><see cref="SubTexture"/> to draw.</param>
		/// <param name="desc">Transform information.</param>
		static void drawQuad(const SubTexture& texture, const DrawDesc& desc = {});

		/// <summary>
		/// Draws tinted textured quad.
		/// </summary>
		/// <param name="texture"><see cref="SubTexture"/> to draw.</param>
		/// <param name="color">Draw color.</param>
		/// <param name="desc">Transform information.</param>
		static void drawQuad(const SubTexture& texture, const glm::vec4& color, const DrawDesc& desc = {});

		/// <summary>
		/// Resets stored <see cref="Statistics"/> data.
		/// </summary>
		static void mark();
		static const Statistics& stats();
	private:
		static void init();
		static void free();
	};
}