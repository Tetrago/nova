#pragma once

#include <initializer_list>

#include "nova/core/base.h"

namespace nova
{
	class Texture;

	enum class AttachmentType
	{
		Color,
		Depth
	};

	struct FramebufferAttachment
	{
		AttachmentType type;
		Ref<Texture> texture;
		uint32_t index = 0;
	};

	struct FramebufferCreateInfo
	{
		std::initializer_list<FramebufferAttachment> attachments;
	};

	/// <summary>
	/// Framebuffer created by <see cref="RenderDevice"/>. Can be set as render target by <see cref="GraphicsContext"/>.
	/// </summary>
	class NOVA_API Framebuffer
	{
	public:
		virtual ~Framebuffer() {}
	};
}