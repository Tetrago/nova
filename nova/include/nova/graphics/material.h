#pragma once

#include <string>
#include <glm/glm.hpp>

#include "nova/core/base.h"

namespace nova
{
	class Pipeline;

	/// <summary>
	/// Manages and stores <see cref="Pipeline"/> data.
	/// </summary>
	class NOVA_API Material
	{
	public:
		virtual ~Material() {}

		virtual void set(const std::string& name, const glm::mat3& v) = 0;
		virtual void set(const std::string& name, const glm::mat4& v) = 0;

		virtual void set(const std::string& name, float v) = 0;
		virtual void set(const std::string& name, const glm::vec2& v) = 0;
		virtual void set(const std::string& name, const glm::vec3& v) = 0;
		virtual void set(const std::string& name, const glm::vec4& v) = 0;

		virtual void set(const std::string& name, int v) = 0;
		virtual void set(const std::string& name, const glm::ivec2& v) = 0;
		virtual void set(const std::string& name, const glm::ivec3& v) = 0;
		virtual void set(const std::string& name, const glm::ivec4& v) = 0;

		virtual void setTexture(const std::string& name, int v) = 0;
		virtual void setTexture(const std::string& name, int* v, uint32_t count) = 0;

		virtual const Pipeline& pipeline() const = 0;
	};
}