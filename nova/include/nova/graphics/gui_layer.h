#pragma once

#include "nova/core/base.h"

namespace nova
{
	/// <summary>
	/// Layer managing Gui processes.
	/// </summary>
	class NOVA_API GuiLayer
	{
	public:
		virtual void begin() = 0;
		virtual void end() = 0;

		/// <summary>
		/// Creates a Gui layer based on the <see cref="RendererApi"/>.
		/// </summary>
		/// <returns><see cref="Ref"/> to a platform agnostic Gui layer.</returns>
		static Ref<GuiLayer> create();
	};
}