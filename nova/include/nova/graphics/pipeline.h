#pragma once

#include <string>
#include <initializer_list>

#include "nova/core/base.h"
#include "nova/core/log.h"
#include "nova/graphics/buffer.h"
#include "nova/resource/resource.h"

namespace nova
{
	/// <summary>
	/// Face relative to <see cref="FrontFace"/> to cull.
	/// </summary>
	enum class CullMode
	{
		None,
		Back,
		Front,
		Both
	};

	enum class FillMode
	{
		Fill,
		Line,
		Point
	};

	/// <summary>
	/// Order of indices that determines the front face.
	/// </summary>
	enum class FrontFace
	{
		Clockwise,
		Counterclockwise
	};

	struct NOVA_API RasterizerDesc
	{
		CullMode cullMode = CullMode::Back;
		FillMode fillMode = FillMode::Fill;
		FrontFace frontFace = FrontFace::Counterclockwise;
	};

	/// <summary>
	/// Order of indices that builds mesh data.
	/// </summary>
	enum class PrimitiveTopologyType
	{
		PointList,
		LineList,
		LineStrip,
		TriangleList,
		TriangleStrip
	};

	/// <summary>
	/// Api level value types.
	/// </summary>
	enum class ValueType
	{
		Float,
		Int
	};

	/// <summary>
	/// Describes the layout of a stage input.
	/// </summary>
	struct NOVA_API LayoutElement
	{
		/// <value>Input name.</value>
		std::string name;
		/// <value>Input data type.</value>
		ValueType valueType;
		/// <value>How many of the data type are there.</value>
		uint32_t count;

		/// <summary>
		/// Calculates the size of the input.
		/// </summary>
		/// <returns>Size of the input in bytes.</returns>
		constexpr size_t size() const
		{
			switch(valueType)
			{
			default:
				nova_logger() << Log::Error << "Unsupported value type" << Log::Done;
				return 0;
			case nova::ValueType::Float: return sizeof(float) * count;
			case nova::ValueType::Int: return sizeof(int) * count;
			}
		}
	};

	struct NOVA_API PipelineCreateInfo
	{
		ResourceID shader;
		RasterizerDesc rasterizer;
		PrimitiveTopologyType primitiveTopology = PrimitiveTopologyType::TriangleList;
	};

	class NOVA_API Pipeline
	{
	public:
		virtual ~Pipeline() {}
	};
}