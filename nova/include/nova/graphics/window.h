#pragma once

#include <tuple>
#include <string>

#include "nova/core/base.h"
#include "nova/core/input.h"
#include "nova/graphics/graphics_context.h"

namespace nova
{
	class Window;

	struct NOVA_API WindowCreateInfo
	{
		int32_t x = 50;
		int32_t y = 50;
		uint32_t width = 1024;
		uint32_t height = 576;
		std::string title_ = "Unnamed";
		bool maximized = false;
		bool resizable = false;
		bool visible = true;
		bool focused = true;
		bool decorated = true;
		Window* share = nullptr;
	};

	class NOVA_API Window
	{
	public:
		virtual ~Window() {}

		/// <summary>
		/// Updates all windows.
		/// </summary>
		virtual void update() = 0;

		virtual bool isFocused() const = 0;
		virtual bool isHovered() const = 0;
		virtual std::tuple<int, int> position() const = 0;
		virtual std::tuple<uint32_t, uint32_t> size() const = 0;

		virtual void focus() = 0;
		virtual void position(int x, int y) = 0;
		virtual void size(uint32_t w, uint32_t h) = 0;
		virtual void title(const std::string& title) = 0;
		virtual void visible(bool v) = 0;

#if defined(NOVA_MODULE_OPENGL)
		virtual void gl_MakeCurrent() = 0;
		virtual void gl_Swap() = 0;
#endif

		/// <summary>
		/// Gets this window's <see cref="GraphicsContext"/>, used for drawing to the display.
		/// </summary>
		GraphicsContext& context();

		/// <summary>
		/// Creates a window based on the <see cref="PlatformApi"/>.
		/// </summary>
		/// <returns><see cref="Ref"/> to a platform agnostic window.</returns>
		static Box<Window> create(const WindowCreateInfo& createInfo);
	private:
		Box<GraphicsContext> ctx_;
	};

	struct NOVA_API WindowEvent
	{
		NOVA_EVENT("Window");
		Window* window;

		enum class Type
		{
			Close,
			Key,
			Button,
			Mouse,
			Resize,
			Move,
			Maximize,
			Scroll,
			Character
		};

		Type type;

		union
		{
			struct { KeyCode key; bool status; } key;
			struct { uint32_t button; bool status; } button;
			struct { double x, y; } mouse;
			struct { uint32_t width, height; } resize;
			struct { int32_t x, y; } move;
			struct { bool maximized; } maximize;
			struct { double delta; } scroll;
			struct { unsigned int c; } character;
		};
	};
}