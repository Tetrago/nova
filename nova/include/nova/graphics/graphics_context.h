#pragma once

#include "nova/core/base.h"

namespace nova
{
	class Buffer;
	class Framebuffer;
	class Pipeline;
	class Texture;

	/// <summary>
	/// Manages a graphics context and is used to interface with the graphics api.
	/// </summary>
	class NOVA_API GraphicsContext
	{
	public:
		virtual ~GraphicsContext() {}

		/// <summary>
		/// Called on push/pop to <see cref="RenderStack"/>.
		/// </summary>
		virtual void use() {}

		/// <summary>
		/// Set a <see cref="Framebuffer"/> as the render target.
		/// </summary>
		/// <param name="target">Framebuffer to target.</param>
		virtual void setRenderTarget(const Framebuffer& target) = 0;

		/// <summary>
		/// Clears assigned render target, setting it back to the display.
		/// </summary>
		virtual void clearRenderTarget() = 0;

		/// <summary>
		/// Changes the viewport.
		/// </summary>
		/// <param name="x">X position.</param>
		/// <param name="y">Y position.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		/// <remarks>
		/// All values are in pixels.
		/// </remarks>
		virtual void viewport(int x, int y, int width, int height) = 0;

		/// <summary>
		/// Sets the current <see cref="Pipeline"/>.
		/// </summary>
		/// <param name="pipeline">Pipeline to use.</param>
		virtual void setPipeline(const Pipeline& pipeline) = 0;

		/// <summary>
		/// Sets the current vertex <see cref="Buffer"/>.
		/// </summary>
		/// <param name="buffer">Buffer to use.</param>
		virtual void setVertexBuffer(const Buffer& buffer) = 0;

		/// <summary>
		/// Sets the current index <see cref="Buffer"/>.
		/// </summary>
		/// <param name="buffer">Buffer to use.</param>
		virtual void setIndexBuffer(const Buffer& buffer) = 0;

		/// <summary>
		/// Sets the clear color.
		/// </summary>
		/// <param name="r">Red.</param>
		/// <param name="g">Green.</param>
		/// <param name="b">Blue.</param>
		/// <param name="a">Alpha</param>
		/// <remarks>
		/// All values are in the range [0, 1].
		/// </remarks>
		virtual void setClearColor(float r, float g, float b, float a) = 0;

		/// <summary>
		/// Sets the active textures.
		/// </summary>
		/// <param name="textures">Texture array.</param>
		/// <param name="count">Number of textures in <paramref name="textures"/>.</param>
		virtual void applyTextures(const Texture** textures, uint32_t count) = 0;

		/// <summary>
		/// Presents to screen.
		/// </summary>
		virtual void present() = 0;

		/// <summary>
		/// Clears screen using the current clear color, set by <see cref="setClearColor"/>.
		/// </summary>
		virtual void clear() = 0;

		/// <summary>
		/// Draws indexed buffers.
		/// </summary>
		/// <param name="count">Number of indices to draw.</param>
		virtual void drawIndexed(uint32_t count) = 0;
	};
}