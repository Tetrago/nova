#pragma once

#include "nova/core/base.h"
#include "nova/resource/resource.h"

namespace nova
{
	class Image;

	enum class TextureFormat
	{
		Depth,
		Rgb8,
		Rgba8
	};

	struct NOVA_API TextureCreateInfo
	{
		uint32_t width, height;
		TextureFormat format = TextureFormat::Rgb8;
		void* data = nullptr;
	};

	class NOVA_API Texture
	{
	public:
		virtual ~Texture() {}

		/// <summary>
		/// Creates a texture from raw data.
		/// </summary>
		/// <param name="info">Texture info.</param>
		virtual void create(const TextureCreateInfo& info) = 0;

		/// <summary>
		/// Creates a texture from an <see cref="Image"/>.
		/// </summary>
		/// <param name="image"><see cref="ResourceID"/> of a loader image.</param>
		virtual void load(ResourceID image) = 0;

		virtual uint32_t width() const = 0;
		virtual uint32_t height() const = 0;
	};
}