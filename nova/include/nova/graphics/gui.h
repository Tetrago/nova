#pragma once

#include <optional>
#include <filesystem>
#include <glm/glm.hpp>

#include "nova/core/base.h"

struct ImGuiViewport;

namespace nova
{
	class Texture;

	/// <summary>
	/// Immediate mode Gui wrapper.
	/// </summary>
	class NOVA_API Gui
	{
	public:
		/// <summary>
		/// Viewport properties container.
		/// </summary>
		class NOVA_API Viewport
		{
			friend class Gui;
		public:
			glm::vec2 position() const;
			glm::vec2 size() const;
			int id() const;
		private:
			Viewport(ImGuiViewport* viewport);

			ImGuiViewport* viewport_;
		};


		/// <summary>
		/// Gui flags for various elements.
		/// </summary>
		enum Flags : uint32_t
		{
			None = 0,

			// Window
			/// <summary>Disables window collapsing.</summary>
			NoCollapse = BIT(0),
			/// <summary>Auto resizes windows, disables manual resizing.</summary>
			AutoResize = BIT(1),
			/// <summary>Reserves space in window for a menu bar.</summary>
			MenuBar = BIT(2),
			/// <summary>Disables docking.</summary>
			NoDocking = BIT(3),
			/// <summary>Disables resizing.</summary>
			NoResize = BIT(4),
			/// <summary>Disables moving.</summary>
			NoMove = BIT(5),
			/// <summary>Disables titlebar.</summary>
			NoTitleBar = BIT(6),
			/// <summary>Disables focusing.</summary>
			NoNavFocus = BIT(7),
			/// <summary>Disables bring to front.</summary>
			NoBringToFront = BIT(8),
			/// <summary>Hides the scrollbar.</summary>
			NoScrollbar = BIT(9),

			// Input Text
			/// <summary>Sets input as read only.</summary>
			ReadOnly = BIT(0),
			/// <summary>Input returns true on enter.</summary>
			Enter = BIT(1),

			// Tree
			/// <summary>Open on selecting the arrow.</summary>
			OpenOnArrow = BIT(0),
			/// <summary>Sets whether the node is selected.</summary>
			Selected = BIT(1),
			/// <summary>Open on double click.</summary>
			OpenOnDoubleClick = BIT(2),
			/// <summary>Sets leaf node (no children).</summary>
			Leaf = BIT(3)
		};

		/// <summary>
		/// Styles variables set by <see cref="pushStyleVar"/> and <see cref="popStyleVar"/>.
		/// </summary>
		enum class StyleVar
		{
			/// Inner window padding.
			WindowPadding,
			/// Window corner roundsing.
			WindowRounding,
			/// Size of the window's border.
			WindowBorderSize,
			/// Padding around upper bar.
			FramePadding,
			/// Rounding on upper bar.
			FrameRounding,
			/// Border size of upper bar.
			FrameBorderSize
		};

		/// <summary>
		/// Sets condition for <see cref="setNextWindowSize"/>.
		/// </summary>
		enum class Condition
		{
			None,
			/// Every time opening window.
			Always,
			/// First time opening window.
			First
		};

		inline const static float NO_LIMIT = FLT_MAX / INT_MAX;
	public:
		/// <summary>
		/// Showcases Gui system's capabilities.
		/// </summary>
		/// <param name="v">Pointer to bool expressing whether the window is or isn't shown.</param>
		static void showDemoWindow(bool* v = nullptr);

		/// <summary>
		/// Sets the Gui cursor's position. This is how elements can be centered.
		/// </summary>
		static void setCursorPosition(float x, float y);

		/// <summary>
		/// Get the Gui cursor's position.
		/// </summary>
		/// <returns>Position of cursor.</returns>
		static glm::vec2 getCursorPosition();

		/// <summary>
		/// Set the next window's position.
		/// </summary>
		/// <param name="x">X position.</param>
		/// <param name="y">Y position.</param>
		/// <param name="px">What point on the window [0, 1] will be set to <paramref name="x"/></param>
		/// <param name="py">What point on the window [0, 1] will be set to <paramref name="y"/></param>
		static void setNextWindowPosition(float x, float y, float px = 0, float py = 0);

		/// <summary>
		/// Set the next window's size.
		/// </summary>
		/// <param name="w">Width.</param>
		/// <param name="h">Height.</param>
		/// <param name="condition">The <see cref="Condition"/> to set the size on.</param>
		static void setNextWindowSize(float w, float h, Condition condition = Condition::None);

		/// <summary>
		/// Set the next window's viewport.
		/// </summary>
		/// <param name="viewport"><see cref="Viewport"/> to use.</param>
		static void setNextWindowViewport(const Viewport& viewport);

		/// <summary>
		/// Gets the main viewport.
		/// </summary>
		/// <returns>Main <see cref="Viewport"/>.</returns>
		static Viewport getMainViewport();

		/// <summary>
		/// Gets the available space within the window for elements.
		/// </summary>
		/// <returns>Width and height of region.</returns>
		static glm::vec2 getAvailableSpace();

		/// <summary>
		/// Checks if the active window is focused.
		/// </summary>
		/// <returns>Is focused.</returns>
		static bool isFocused();

		/// <summary>
		/// Checks if the active window is hovered.
		/// </summary>
		/// <returns>Is hovered.</returns>
		static bool isHovered();

		/// <summary>
		/// Begins a new window.
		/// </summary>
		/// <param name="name">Title.</param>
		/// <param name="open">Pointer to bool allowing closing.</param>
		/// <param name="flags">Set of <see cref="Flags"/> to customize window.</param>
		/// <returns>Whether the window is collapsed.</returns>
		/// <remarks>
		/// <see cref="end"/> must be called regardless of if the window is expanded.
		/// </remarks>
		static bool begin(const char* name, bool* open = nullptr, uint32_t flags = None);

		/// <summary>
		/// Ends window, should be called after <see cref="begin"/>.
		/// </summary>
		static void end();

		/// <summary>
		/// Begins a child element, an element within a window.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="w">Width of child, or maximum.</param>
		/// <param name="h">Height of child, or maximum.</param>
		/// <param name="border">If the child has a border.</param>
		/// <param name="flags">Set of <see cref="Flags"/> to custom child.</param>
		/// <returns>Whether the child is being displayed.</returns>
		/// <remarks>
		/// <see cref="endChild"/> must be called if <c>true</c> is returned.
		/// </remarks>
		static bool beginChild(const char* name, float w = 0, float h = 0, bool border = false, uint32_t flags = None);

		/// <summary>
		/// Ends child, should be called after <see cref="beginChild"/> returns <c>true</c>.
		/// </summary>
		static void endChild();

		/// <summary>
		/// Begins modal that can be shown with <see cref="openPopup"/>.
		/// </summary>
		/// <param name="name">Title.</param>
		/// <param name="flags">Set of <see cref="Flags"/> to customize modal.</param>
		/// <returns>If the modal will be shown.</returns>
		/// <remarks>
		/// <see cref="endPopup"/> must be called if <c>true</c> is returned.
		/// </remarks>
		static bool beginModal(const char* name, uint32_t flags = None);

		/// <summary>
		/// Begins popup that can be shown with <see cref="openPopup"/>.
		/// </summary>
		/// <param name="name">Title.</param>
		/// <param name="flags">Set of <see cref="Flags"/> to customize popup.</param>
		/// <returns>If the popup will be shown.</returns>
		/// <remarks>
		/// <see cref="endPopup"/> must be called if <c>true</c> is returned.
		/// </remarks>
		static bool beginPopup(const char* name, uint32_t flags = None);

		/// <summary>
		/// Ends popup, should be called after <see cref="beginModal"/> or <see cref="beginPopup"/> if <c>true</c> is returned.
		/// </summary>
		static void endPopup();

		/// <summary>
		/// Begins a menu bar.
		/// </summary>
		/// <returns>Whether the menu bar will be shown.</returns>
		/// <remarks>
		/// <see cref="endMenuBar"/> must be called if <c>true</c> is returned.
		/// </remarks>
		static bool beginMenuBar();

		/// <summary>
		/// Ends a menu bar, should be called after <see cref="beginMenuBar"/> if <c>true</c> is returned.
		/// </summary>
		static void endMenuBar();

		/// <summary>
		/// Begins menu bar drop down inside a menu bar.
		/// </summary>
		/// <param name="name">Drop down name.</param>
		/// <returns>Whether the drop down will be shown.</returns>
		/// <remarks>
		/// Must be called in a menu bar and <see cref="endMenu"/> must be called if <c>true</c> is returned.
		/// </remarks>
		/// <seealso cref="beginMenuBar"/>
		static bool beginMenu(const char* name);

		/// <summary>
		/// Ends menu bar drop down, should be called after <see cref="beginMenu"/> if <c>true</c> is returned.
		/// </summary>
		static void endMenu();

		/// <summary>
		/// Menu item used under menu drop down.
		/// </summary>
		/// <param name="name">Name of item.</param>
		/// <param name="shortcut">Keyboard shortcut.</param>
		/// <param name="selected">Whether the item is selected/selectable.</param>
		/// <seealso cref="beginMenuBar"/>
		/// <seealso cref="beginMenu"/>
		static bool menuItem(const char* name, const char* shortcut = nullptr, bool* selected = nullptr);

		/// <summary>
		/// Pushes style changes.
		/// </summary>
		/// <param name="var">The specific <see cref="StyleVar"/> to set.</param>
		/// <param name="a">The value to set var to.</param>
		static void pushStyleVar(const StyleVar& var, float a);

		/// <summary>
		/// Pushes style changes.
		/// </summary>
		/// <param name="var">The specific <see cref="StyleVar"/> to set.</param>
		/// <param name="a">The value to set the first part of var to.</param>
		/// <param name="b">The value to set the second part of var to.</param>
		static void pushStyleVar(const StyleVar& var, float a, float b);

		/// <summary>
		/// Removes pushed <see cref="StyleVar"/>(s).
		/// </summary>
		/// <param name="count">Number to remove.</param>
		static void popStyleVar(int count = 1);

		/// <summary>
		/// Seperates elements with the same name.
		/// </summary>
		/// <param name="id">Unique ID.</param>
		static void pushID(int id);
		
		/// <summary>
		/// Seperates elements with the same name.
		/// </summary>
		/// <param name="id">Unique ID.</param>
		static void pushID(const char* id);

		/// <summary>
		/// Removes ID space.
		/// </summary>
		/// <seealso cref="pushID"/>
		static void popID();

		/// <summary>
		/// Collapsible header.
		/// </summary>
		/// <param name="name">Name of header.</param>
		/// <returns>Whether the header is open.</returns>
		static bool collapsible(const char* name);

		/// <summary>
		/// Horizontal sperator.
		/// </summary>
		static void separator();

		/// <summary>
		/// Puts the next element on the same line as the previous.
		/// </summary>
		static void sameLine();

		/// <summary>
		/// Puts the next element on the next line.
		/// </summary>
		static void newLine();

		/// <summary>
		/// Increases indent level, can be called multiple times.
		/// </summary>
		/// <seealso cref="unindent"/>
		static void indent();

		/// <summary>
		/// Decreases indent, can be called once for every call of <see cref="indent"/>.
		/// </summary>
		static void unindent();

		/// <summary>
		/// Opens a popup.
		/// </summary>
		/// <param name="name">Name to open.</param>
		/// <seealso cref="beginModal"/>
		/// <seealso cref="beginPopup"/>
		static void openPopup(const char* name);

		/// <summary>
		/// Closes a popup.
		/// </summary>
		/// <seealso cref="openPopup"/>
		static void closePopup();

		/// <summary>
		/// Creates dockspace in the current space.
		/// </summary>
		/// <param name="name">Name of dockspace.</param>
		static void dockSpace(const char* name);

		/// <summary>
		/// Combo input box.
		/// </summary>
		/// <para>
		/// <paramref name="options"/> is a string consisting of each option with a '\0' at the end. For example: "A\0B\0C\0"
		/// </para>
		/// <param name="label">Element label.</param>
		/// <param name="i">Pointer to <c>int</c> index of the selected option.</param>
		/// <param name="options">String of options.</param>
		/// <returns>If the value changed.</returns>
		static bool combo(const char* label, int* i, const char* options);

		/// <summary>
		/// Slider input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Pointer to the slider's value.</param>
		/// <param name="range">Vector range of slider.</param>
		/// <param name="format">Display format of value, for example "%.3f".</param>
		/// <returns>If the value changed.</returns>
		static bool slider(const char* label, float* v, glm::vec2 range, const char* format = "%.3f");

		/// <summary>
		/// Drag input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Value to modify.</param>
		/// <param name="step">Step speed, how much the value is adjusted.</param>
		/// <returns>If the value changed.</returns>
		static bool drag(const char* label, float& v, float step = 0.01f);

		/// <summary>
		/// Drag vector input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Value to modify.</param>
		/// <param name="step">Step speed, how much the value is adjusted.</param>
		/// <param name="min">Minimum value.</param>
		/// <param name="max">Maximum value.</param>
		/// <returns>If the value changed.</returns>
		/// <remarks>
		/// When using either <paramref name="min"/> or <paramref name="max"/>, both must be set.
		/// To only limit one, use <see cref="MAX_LIMIT"/>.
		/// </remarks>
		static bool drag(const char* label, glm::vec2& v, float step = 0.01f, float min = 0, float max = 0);

		/// <summary>
		/// Drag vector input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Value to modify.</param>
		/// <param name="step">Step speed, how much the value is adjusted.</param>
		/// <param name="min">Minimum value.</param>
		/// <param name="max">Maximum value.</param>
		/// <returns>If the value changed.</returns>
		/// <remarks>
		/// When using either <paramref name="min"/> or <paramref name="max"/>, both must be set.
		/// To only limit one, use <see cref="MAX_LIMIT"/>.
		/// </remarks>
		static bool drag(const char* label, glm::vec3& v, float step = 0.01f, float min = 0, float max = 0);

		/// <summary>
		/// Text input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Text to modify.</param>
		/// <param name="size">Maximum size of input text.</param>
		/// <param name="flags">A set of <see cref="Flags"/>.</param>
		/// <returns>If the value changed.</returns>
		static bool input(const char* label, char* v, size_t size, uint32_t flags = None);

		/// <summary>
		/// Float direct input.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Float to modify.</param>
		/// <returns>If the value changed.</returns>
		static bool input(const char* label, float* v);

		/// <summary>
		/// Color input and wheel.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Color vector to modify.</param>
		/// <returns>If the value changed.</returns>
		static bool color(const char* label, glm::vec4& v);

		/// <summary>
		/// Draws a texture to the space.
		/// </summary>
		/// <param name="texture">A <see cref="Texture"/> to draw.</param>
		/// <param name="w">Width to display it at.</param>
		/// <param name="h">Height to display it at.</param>
		/// <param name="scale">Scale to draw at.</param>
		static void texture(const Ref<Texture>& texture, float w, float h, float scale = 1);

		/// <summary>
		/// Button element.
		/// </summary>
		/// <param name="name">Button text.</param>
		/// <param name="w">Width or maximized.</param>
		/// <param name="h">Height or maximized.</param>
		/// <returns>If the button is pressed.</returns>
		static bool button(const char* name, float w = 0, float h = 0);

		/// <summary>
		/// Text element.
		/// </summary>
		/// <param name="fmt">Standard format string.</param>
		static void text(const char* fmt, ...);

		/// <summary>
		/// Checkbox element.
		/// </summary>
		/// <param name="label">Element label.</param>
		/// <param name="v">Pointer to value of checkbox.</param>
		/// <returns>If the value changed.</returns>
		static bool checkbox(const char* label, bool* v);

		/// <summary>
		/// Selectable element.
		/// </summary>
		/// <param name="name">Name of selectable.</param>
		/// <param name="v">Pointer to value of checkbox.</param>
		/// <returns>If the value changed.</returns>
		static bool selectable(const char* name, bool* v);

		/// <summary>
		/// Checks if an item was clicked.
		/// </summary>
		/// <param name="button">Mouse button to check.</param>
		/// <returns>If the previous item was clicked..</returns>
		static bool isItemClicked(int button = 0);

		/// <summary>
		/// Checks if a mouse button is down.
		/// </summary>
		/// <param name="button">Mouse button to check.</param>
		/// <returns>If the <paramref name="button"/> is down.</returns>
		static bool isMouseDown(int button = 0);

		/// <summary>
		/// Tree node item.
		/// </summary>
		/// <param name="label">Node label.</param>
		/// <param name="flags">A set of <see cref="Flags"/>.</param>
		/// <returns>If the tree is open.</returns>
		/// <remarks>
		/// Needs to be removed with <see cref="popTree"/>.
		/// </remarks>
		static bool tree(const char* label, uint32_t flags = None);

		/// <summary>
		/// Pops tree node.
		/// </summary>
		/// <seealso cref="tree"/>
		static void popTree();

		static std::optional<std::filesystem::path> openFile(const char* ext);
		static std::optional<std::filesystem::path> saveFile(const char* ext);
	};
}