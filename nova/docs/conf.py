# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Nova'
copyright = '2020, Tetrago'
author = 'Tetrago'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "breathe",
    "sphinx.ext.autosectionlabel"
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    "logo_only": True,
    "collapse_navigation": False
}

html_logo = "img/logo.png"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Breathe options ---------------------------------------------------------
breathe_default_project = "Nova"

# -- Read the Docs -----------------------------------------------------------
import subprocess, os

def configure_doxyfile(dir):
    with open('Doxyfile.in', 'r') as file:
        data = file.read()
    
    data = data.replace('@DOXYGEN_OUTPUT_DIR@', dir)

    with open('Doxyfile', 'w') as file:
        file.write(data)

breathe_projects = {}

if os.environ.get('READTHEDOCS', None) == 'True':
    configure_doxyfile('build')
    subprocess.call('doxygen', shell=True)
    breathe_projects['Nova'] = 'build/xml'
