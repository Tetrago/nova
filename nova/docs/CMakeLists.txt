project(nova_docs)

find_package(Doxygen REQUIRED)
find_program(SPHINX_EXE
	NAMES "sphinx-build"
	REQUIRED
	DOC "Path to sphinx-build")

set(DOXYGEN_OUTPUT_DIR "${PROJECT_SOURCE_DIR}/build/doxygen")
set(SPHINX_OUTPUT_DIR "${PROJECT_SOURCE_DIR}/build/sphinx")

configure_file("${PROJECT_SOURCE_DIR}/Doxyfile.in" "${PROJECT_SOURCE_DIR}/Doxyfile" @ONLY)

file(GLOB_RECURSE DOC_HEADERS "../include/**/*.h")
file(GLOB_RECURSE DOC_RST "**/*.rst")

add_custom_command(OUTPUT "${DOXYGEN_OUTPUT_DIR}"
	COMMAND ${DOXYGEN_EXECUTABLE}
	WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
	DEPENDS ${DOC_HEADERS}
	MAIN_DEPENDENCY "${PROJECT_SOURCE_DIR}/Doxyfile"
	COMMENT "Generating docs")

add_custom_command(OUTPUT "${SPHINX_OUTPUT_DIR}"
	COMMAND ${SPHINX_EXE} -b html -Dbreathe_projects.Nova=${DOXYGEN_OUTPUT_DIR}/xml . ${SPHINX_OUTPUT_DIR}
	WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
	DEPENDS ${DOXYGEN_OUTPUT_DIR} ${DOC_HEADERS} ${DOC_RST}
	MAIN_DEPENDENCY "conf.py"
	COMMENT "Generating sphinx")

add_custom_target(nova_docs ALL DEPENDS "${SPHINX_OUTPUT_DIR}")