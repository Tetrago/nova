.. _reference_cpp_core_base:

Base
====

Defines
-------

.. doxygendefine:: NOVA_DEBUG
.. doxygendefine:: NOVA_ASSERT
.. doxygendefine:: NOVA_DIAG

API
---

.. doxygenenum:: nova::RendererApi
.. doxygenenum:: nova::PlatformApi

Ref
---

.. doxygentypedef:: nova::Ref
.. doxygenfunction:: nova::make_ref
.. doxygenfunction:: nova::cast_ref

Box
---

.. doxygentypedef:: nova::Box
.. doxygenfunction:: nova::make_box