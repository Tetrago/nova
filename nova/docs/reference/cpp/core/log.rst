.. _reference_cpp_core_log:

Log
===

Log
---

.. doxygenenum:: nova::Log

LogEvent
--------

.. doxygenstruct:: nova::LogEvent
    :members:

Logger
------

.. doxygenclass:: nova::Logger
    :members: