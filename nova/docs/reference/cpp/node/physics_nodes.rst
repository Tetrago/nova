.. _reference_cpp_node_physics_nodes:

Physics Nodes
=============

Collider2D
----------

.. doxygenclass:: nova::Collider2D
	:members:

BoxCollider2D
-------------

.. doxygenclass:: nova::BoxCollider2D
	:members:

Rigidbody2D
-----------

.. doxygenclass:: nova::Rigidbody2D
	:members: