.. _reference_cpp_node_basic_nodes:

Basic Nodes
===========

Node2D
------

.. doxygenclass:: nova::Node2D
	:members:

SpriteRenderer
--------------

.. doxygenclass:: nova::SpriteRenderer
	:members: