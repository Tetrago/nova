.. _reference_cpp_node_node:

Node
====

.. doxygendefine:: NOVA_NODE

.. doxygenclass:: nova::NodeRegistry
    :members:

.. doxygenclass:: nova::Node
    :members: