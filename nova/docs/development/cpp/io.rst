.. _development_cpp_io:

IO
==

Logging
------

Basic logging is done through the :ref:`Logger <reference_cpp_core_log>`.
This can be instantiated on its own, however there is an instance intended for use at:

.. code-block:: cpp
    :linenos:

    nova::Program::get().logger()

The logger overloads the left bit shift operator to output logs.
Start a line with a :ref:`Log <reference_cpp_core_log>`, then write information like you would to ``std::cout``.
To end a line, simply write ``nova::Log::Done``.

This is shown below:

.. code-block:: cpp
    :linenos:

    nova::Program::get().logger() << nova::Log::Error << "There is an issue (code: " << varA << ") at: " << varB << nova::Log::Done;

Refer to :ref:`Log <reference_cpp_core_log>` for specific levels.

This can be simplified using the ``NOVA_PROGRAM_LOG`` macro like so:

.. code-block:: cpp
    :linenos:

    NOVA_PROGRAM_LOG(nova::Log::Error, "There is an issue (code: " << varA << ") at: " << varB);

Diagnostics
-----------