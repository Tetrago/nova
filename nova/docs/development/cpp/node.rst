.. _development_cpp_nodes:

Nodes
=====

Writing Logic
-------------

All logic is implemented by:

#. :ref:`Writing the Node`
#. :ref:`Naming the Node`
#. :ref:`Registering the Node`

Writing the Node
^^^^^^^^^^^^^^^^

Writing a node simply involves inheriting from a node.
It is imortant to note that this does not mean ``Node`` specifically, but any subclasses too.
A common example of this is inheriting from ``Rigidbody2D`` if you want 2D physics on that node and its ``Node2D`` children.

.. code-block:: cpp
    :linenos:

    class MyNode : public nova::Node2D
    {
    public:
        void update() override
        {
            // Per-frame logic
        }
    }

Refer to :ref:`Node <reference_cpp_node_node>` for overridable methods.

Naming the Node
^^^^^^^^^^^^^^^

.. important::
	``NOVA_NODE`` must be called for a node to work properly.

Simply add a call to the ``NOVA_NODE`` macro within the class body.

.. code-block:: cpp
    :linenos:
    :emphasize-lines: 3

    class MyNode : public nova::Node2D
    {
        NOVA_NODE("MyNode")
    public:
        void update() override
        {
            // Per-frame logic
        }
    }

Registering the Node
^^^^^^^^^^^^^^^^^^^^

.. code-block:: cpp

	nova::NodeRegistry::registerNode<MyNode>();

Drawing an Inspector
--------------------

In order to add adjustable GUI parameters within Kilonova, you need to design the drawer.
This means overriding the base ``onInspectorDraw`` method and making two calls:

- The base class ``onInspectorDraw``
- The ``NOVA_NODE_INSPECTOR`` macro, which enforces formatting.

.. warning::
    ``onInspectorDraw`` will be replaced with an out of engine alternative in the future.

.. code-block:: cpp
    :linenos:
    :emphasize-lines: 7,8

    class MyNode : public nova::Node
    {
        // ...

        void onInspectorDraw() override
        {
            Node2D::onInspectorDraw();
            NOVA_NODE_INSPECTOR();

            Gui::input("My Number", &myNumber_);
        }
    private:
        float myNumber_;
    }

Gui functions can be found in the :ref:`reference <reference_cpp_graphics_gui>`.