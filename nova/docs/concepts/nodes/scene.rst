.. _concepts_nodes_scene:

Scene
=====

Scenes are the containing structure for :ref:`Nodes <concepts_nodes_node>`.
They contain a singular root node and hold the memory and Api for creating them.
Nodes are not added directly through scenes, however, but added via there creation methods found in the :ref:`reference <reference_node_node>`.