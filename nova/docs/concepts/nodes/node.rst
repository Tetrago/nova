.. _concepts_nodes_node:

Node
====

Basics
------

Nova uses a node and scene system for logic, where scenes contain nodes and nodes contain child nodes.
Nodes can be inherited from, and a scene has one "root" node and all other objects are children of it.
Any number of nodes can be added to a parent node, given each are uniquely named.