#include "npch.h"
#include "nova/core/bootstrap.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <cstring>

#include "nova/core/log.h"

namespace nova
{
	namespace
	{
		constexpr spdlog::level::level_enum find_spdlog_level(const Log& log)
		{
			switch(log)
			{
			default:
			case Log::Warn: return spdlog::level::warn;
			case Log::Trace: return spdlog::level::trace;
			case Log::Info: return spdlog::level::info;
			case Log::Error: return spdlog::level::err;
			case Log::Fatal: return spdlog::level::critical;
			}
		}

#define CMP(x, y) (strcmpi(x, y) == 0)
		void parse_platform_api(const char* name, PlatformApi& api)
		{
#if defined(NOVA_MODULE_GLFW)
			if(CMP(name, "glfw"))
			{
				api = PlatformApi::Glfw;
			}
#endif
#if defined(NOVA_MODULE_WIN32)
			if(CMP(name, "win32"))
			{
				api = PlatformApi::Win32;
			}
#endif
		}

		void parse_renderer_api(const char* name, RendererApi& api)
		{
#if defined(NOVA_INCLUDE_OPENGL)
			if(CMP(name, "opengl"))
			{
				api = RendererApi::OpenGL;
			}
#endif
		}
#undef CMP

		PlatformApi determine_optimal_platform()
		{
#if defined(NOVA_PLATFORM_WIN) && defined(NOVA_MODULE_WIN32)
			return PlatformApi::Win32;
#else
			return PlatformApi::Glfw;
#endif
		}

		RendererApi determine_optimal_renderer()
		{
			return RendererApi::OpenGL;
		}
	}

	EngineInfo Bootstrap::parseArguments(int argc, char** argv)
	{
		EngineInfo info
		{
			determine_optimal_platform(),
			determine_optimal_renderer()
		};

		for(int i = 1; i < argc; ++i)
		{
#define ARGUMENT(compact, full) (strcmpi(compact, argv[i]) == 0 || strcmpi(full, argv[i]) == 0)
#define HAS_NEXT() (i + 1 < argc)

			if(ARGUMENT("-p", "--platform") && HAS_NEXT())
			{
				parse_platform_api(argv[++i], info.platformApi);
			}
			else if(ARGUMENT("-r", "--renderer") && HAS_NEXT())
			{
				parse_renderer_api(argv[++i], info.rendererApi);
			}

#undef HAS_NEXT
#undef ARGUMENT
		}

		return info;
	}

	void Bootstrap::setupLogging()
	{
		static std::vector<spdlog::sink_ptr> sinks
		{
			std::make_shared<spdlog::sinks::basic_file_sink_mt>("nova.log", true),
#if defined(NOVA_DEBUG)
			std::make_shared<spdlog::sinks::stdout_color_sink_mt>()
#endif
		};

		static std::shared_ptr<spdlog::logger> base = std::make_shared<spdlog::logger>("base", sinks.begin(), sinks.end());
#if defined(NOVA_DEBUG)
		base->set_level(spdlog::level::trace);
#endif
		base->set_pattern("%^[%T] [%n] %v%$");

		EventBus::subscribe<LogEvent>([](const LogEvent& e)
			{
				if(e.name == "nova")
				{
					static auto nova_logger = base->clone("nova");
					nova_logger->log(find_spdlog_level(e.log), e.msg);
				}
				else
				{
					static auto program_logger = base->clone("program");
					program_logger->log(find_spdlog_level(e.log), "{}: {}", e.name, e.msg);
				}

				if(e.log == Log::Error)
				{
					throw std::exception("Error occured");
				}
				else if(e.log == Log::Fatal)
				{
					throw std::exception("Fatal error occured");
				}
			});
	}
}