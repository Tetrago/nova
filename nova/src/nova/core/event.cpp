#include "npch.h"
#include "nova/core/event.h"

namespace nova
{
	std::mutex EventBus::mutex_;
	std::unordered_map<const char*, EventBus::CallbackList*> EventBus::listeners_;
}