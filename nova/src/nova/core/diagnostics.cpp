#include "npch.h"
#include "nova/core/diagnostics.h"

#include <thread>

#include "nova/core/log.h"

namespace nova
{
	namespace diag
	{
		void Instrumentor::begin(const std::filesystem::path& path)
		{
			std::lock_guard lock{ mutex_ };

			if(output_.is_open())
			{
				nova_logger() << Log::Warn << "Attempting to begin instrumentor session in pre-existing session, closing..." << Log::Done;
				output_.close();
			}

			output_.open(path);
			if(output_.is_open())
			{
				count_ = 0;
				writeHeader();
			}
			else
			{
				nova_logger() << Log::Warn << "Could not open instrumentor file: " << path << Log::Done;
			}
		}

		void Instrumentor::end()
		{
			if(output_.is_open())
			{
				std::lock_guard lock{ mutex_ };

				writeFooter();
				output_.close();
			}
		}

		void Instrumentor::write(const Result& result)
		{
			std::lock_guard lock{ mutex_ };

			if(count_++ > 0)
			{
				output_ << ',';
			}

			std::string name{ result.name };
			std::replace(name.begin(), name.end(), '"', '\'');

			output_ << "{"
				<< "\"cat\":\"function\","
				<< "\"dur\":" << (result.end - result.start) << ','
				<< "\"name\":\"" << name << "\","
				<< "\"ph\":\"X\","
				<< "\"pid\":0,"
				<< "\"tid\":" << std::this_thread::get_id() << ','
				<< "\"ts\":" << result.start
				<< "}";

			output_.flush();
		}

		void Instrumentor::writeHeader()
		{
			output_ << "{\"otherData\":{},\"traceEvents\":[";
			output_.flush();
		}

		void Instrumentor::writeFooter()
		{
			output_ << "]}";
			output_.flush();
		}

		Instrumentor& Instrumentor::get()
		{
			static Instrumentor instance;
			return instance;
		}

		ScopeInstrumentor::ScopeInstrumentor(const char* name)
			: name_(name)
			, startTimePoint_(std::chrono::high_resolution_clock::now())
		{}

		ScopeInstrumentor::~ScopeInstrumentor()
		{
			auto endTimepoint = std::chrono::high_resolution_clock::now();

			auto begin = std::chrono::time_point_cast<std::chrono::microseconds>(startTimePoint_).time_since_epoch().count();
			auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint).time_since_epoch().count();

			Instrumentor::get().write({ name_, begin, end });
		}
	}
}