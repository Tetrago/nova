#include "npch.h"
#include "nova/core/program.h"

#include "nova/core/diagnostics.h"
#include "nova/core/engine_prefs.h"
#include "nova/core/event.h"
#include "nova/core/input.h"
#include "nova/core/time.h"
#include "nova/graphics/renderer.h"
#include "nova/graphics/window.h"

namespace nova
{
	Program* Program::instance_ = nullptr;

	Program::~Program()
	{
		if(window_)
		{
			Renderer2D::free();
			RenderStack::pop();
		}
	}

	void Program::start()
	{
		if(running_) return;
		running_ = true;

		while(running_)
		{
			Time::tick();

			if(!paused_ && window_)
			{
				auto& ctx = window_->context();
				ctx.clear();

				tick();

				ctx.present();
			}

			Mouse::get().update();

			if(window_)
			{
				window_->update();
			}
		}
	}

	void Program::stop()
	{
		running_ = false;
	}

	Program& Program::get()
	{
		NOVA_CORE_ASSERT(instance_, "Tried to access program before creating a instance");
		return *instance_;
	}

	void Program::setupGraphics()
	{
		NOVA_CORE_ASSERT(!window_, "Attempting to reassign program window");

		WindowCreateInfo createInfo{};
		createInfo.x = EnginePrefs::DISPLAY_X.get();
		createInfo.y = EnginePrefs::DISPLAY_Y.get();
		createInfo.width = EnginePrefs::DISPLAY_WIDTH.get();
		createInfo.height = EnginePrefs::DISPLAY_HEIGHT.get();
		createInfo.title_ = programInfo_.name;
		createInfo.maximized = EnginePrefs::DISPLAY_MAXIMIZED.get();
		createInfo.resizable = true;

		window_ = std::move(Window::create(createInfo));
		renderDevice_ = RenderDevice::create();

		RenderStack::push(&window_->context());
		Renderer2D::init();

		EventBus::subscribe<WindowEvent>([&](const WindowEvent& e)
			{
				if(e.window == &window())
				{
					switch(e.type)
					{
					case WindowEvent::Type::Resize:
						EnginePrefs::DISPLAY_WIDTH.set(e.resize.width);
						EnginePrefs::DISPLAY_HEIGHT.set(e.resize.height);

						if(e.resize.width == 0 || e.resize.height == 0)
						{
							paused_ = true;
							break;
						}

						paused_ = false;
						break;
					case WindowEvent::Type::Move:
						EnginePrefs::DISPLAY_X.set(e.move.x);
						EnginePrefs::DISPLAY_Y.set(e.move.y);
						break;
					case WindowEvent::Type::Maximize:
						EnginePrefs::DISPLAY_MAXIMIZED.set(e.maximize.maximized);
						break;
					case WindowEvent::Type::Close:
						stop();
						break;
					}
				}
			});
	}

	Program::Program(const EngineInfo& engineInfo, const ProgramInfo& programInfo)
		: programInfo_(programInfo)
		, engineInfo_(engineInfo)
		, logger_(make_box<Logger>(programInfo_.name))
		, running_(false)
	{
		register_nova_nodes();
	}
}