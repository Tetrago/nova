#include "npch.h"
#include "nova/core/input.h"

#include "nova/core/event.h"
#include "nova/core/program.h"
#include "nova/graphics/window.h"

namespace nova
{
	Keyboard::Keyboard()
	{
		EventBus::subscribe<WindowEvent>([&](const WindowEvent& e)
			{
				if(e.type == WindowEvent::Type::Key)
				{
					keys_[e.key.key] = e.key.status;
				}
			});
	}

	Keyboard& Keyboard::get()
	{
		static Keyboard instance;
		return instance;
	}

	Mouse::Mouse()
	{
		EventBus::subscribe<WindowEvent>([&](const WindowEvent& e)
			{
				switch(e.type)
				{
				case WindowEvent::Type::Button:
					if(e.button.button < buttons_.size())
					{
						buttons_[e.button.button] = e.button.status;
					}
					break;
				case WindowEvent::Type::Mouse:
					mouseX_ = e.mouse.x;
					mouseY_ = e.mouse.y;
					break;
				case WindowEvent::Type::Scroll:
					delta_ = e.scroll.delta;
					break;
				}
			});
	}

	Mouse& Mouse::get()
	{
		static Mouse instance;
		return instance;
	}
}