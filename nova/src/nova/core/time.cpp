#include "nova/core/time.h"

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"

namespace nova
{
	namespace
	{
		inline std::chrono::time_point<std::chrono::high_resolution_clock> now()
		{
			return std::chrono::high_resolution_clock::now();
		}

		inline time_t milli(const std::chrono::time_point<std::chrono::high_resolution_clock> timePoint)
		{
			return std::chrono::time_point_cast<std::chrono::milliseconds>(timePoint).time_since_epoch().count();
		}
	}

	double Time::dt_ = 0;
	int Time::frameRate_ = 0;

	Time::TimePoint Time::lastFrame_ = now();
	Time::TimePoint Time::lastSecond_ = now();
	int Time::counter_ = 0;
	
	time_t Time::time()
	{
		return milli(now());
	}

	void Time::tick()
	{
		NOVA_DIAG_FUNCTION();

		auto thisFrame = now();

		auto last = milli(lastFrame_);
		auto current = milli(thisFrame);

		lastFrame_ = thisFrame;
		dt_ = (current - last) * 0.001;

		if(current - milli(lastSecond_) >= 1000)
		{
			frameRate_ = counter_;
			lastSecond_ = thisFrame;
			counter_ = 0;
		}
		else
		{
			++counter_;
		}
	}
}