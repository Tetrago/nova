#include "npch.h"
#include "nova/core/platform.h"

#include "nova/core/program.h"
#include "nova/core/log.h"

#if defined(NOVA_MODULE_GLFW)
#include "platform/glfw.h"
#endif
#if defined(NOVA_MODULE_WIN32)
#include "platform/win32.h"
#endif

namespace nova
{
	Platform& Platform::get()
	{
		static Box<Platform> instance = []() -> Box<Platform>
		{
			switch(Program::get().engineInfo().platformApi)
			{
			default:
				nova_logger() << Log::Fatal << "Unsupported platform api" << Log::Done;
				break;
#if defined(NOVA_MODULE_GLFW)
			case PlatformApi::Glfw:
				return make_box<platform::GlfwPlatform>();
#endif
#if defined(NOVA_MODULE_WIN32)
			case PlatformApi::Win32:
				return make_box<platform::Win32Platform>();
#endif
			}
		}();

		return *instance;
	}
}