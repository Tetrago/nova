#include "npch.h"
#include "nova/physics/world.h"

#include <box2d/box2d.h>

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"
#include "nova/core/time.h"
#include "nova/physics/physics_nodes.h"
#include "nova/physics/raycast.h"

namespace nova
{
	namespace
	{
		b2World& get_world()
		{
			static b2World world{ { 0, GRAVITY } };
			return world;
		}

		class Nearest : public b2RayCastCallback
		{
		public:
			Nearest(float distance)
				: distance_(distance)
			{}

			float ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float fraction) override
			{
				hit_.point = { point.x, point.y };
				hit_.distance = distance_ * fraction;
				hit_.body = static_cast<Rigidbody2D*>(fixture->GetBody()->GetUserData());

				found_ = true;

				return fraction;
			}

			bool hit(RaycastHit2D& hit)
			{
				if(found_)
				{
					hit = hit_;

					return true;
				}

				return false;
			}
		private:
			RaycastHit2D hit_{};
			float distance_;
			bool found_ = false;
		};
	}

	bool World2D::raycast(RaycastHit2D& hit, const glm::vec2& origin, const glm::vec2& direction, float distance)
	{
		NOVA_DIAG_FUNCTION();

		auto point = origin + glm::normalize(direction) * distance;

		b2Vec2 p1 = { origin.x, origin.y };
		b2Vec2 p2 = { point.x, point.y };

		Nearest nearest{ distance };
		get_world().RayCast(&nearest, p1, p2);

		return nearest.hit(hit);
	}

	void World2D::step(int32_t velocityIter, int32_t positionIter)
	{
		NOVA_DIAG_FUNCTION();

		for(b2Body* body = get_world().GetBodyList(); body = body->GetNext();)
		{
			static_cast<Rigidbody2D*>(body->GetUserData())->preStep();
		}

		get_world().Step(Time::fixedDeltaTime(), velocityIter, positionIter);

		for(b2Body* body = get_world().GetBodyList(); body; body = body->GetNext())
		{
			static_cast<Rigidbody2D*>(body->GetUserData())->postStep();
		}
	}

	b2Body* World2D::create(const b2BodyDef* def)
	{
		return get_world().CreateBody(def);
	}

	void World2D::destroy(b2Body* body)
	{
		get_world().DestroyBody(body);
	}
}