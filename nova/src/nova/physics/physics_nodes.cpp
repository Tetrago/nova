#include "npch.h"
#include "nova/physics/physics_nodes.h"

#include <box2d/box2d.h>

#include "nova/physics/world.h"

namespace nova
{
	void BoxCollider2D::onInspectorDraw()
	{
		Collider2D::onInspectorDraw();
		NOVA_NODE_INSPECTOR();

		if(Gui::drag("Extent", extent, 0.01f, 0.01f, Gui::NO_LIMIT))
		{
			if(Rigidbody2D* rb = dynamic_cast<Rigidbody2D*>(parent()))
			{
				rb->dirty();
			}
		}
	}

	void BoxCollider2D::serialize(Json::Value& json) const
	{
		Collider2D::serialize(json);

		json["extent"] = extent;
	}

	void BoxCollider2D::deserialize(const Json::Value& json)
	{
		Collider2D::deserialize(json);

		json.at("extent").get_to(extent);
	}

	Box<b2Shape> BoxCollider2D::shape() const
	{
		Box<b2PolygonShape> shape = make_box<b2PolygonShape>();
		shape->SetAsBox(extent.x, extent.y);
		return shape;
	}

	Rigidbody2D::~Rigidbody2D()
	{
		if(body_)
		{
			World2D::destroy(body_);
		}
	}

	void Rigidbody2D::start()
	{
		dirty();
	}

	void Rigidbody2D::stop()
	{
		if(body_)
		{
			World2D::destroy(body_);
			body_ = nullptr;
		}
	}

	void Rigidbody2D::dirty()
	{
		if(body_)
		{
			World2D::destroy(body_);
		}

		b2BodyDef def{};
		def.type = [&]()
		{
			switch(type)
			{
			default:
			case Type::Dynamic: return b2_dynamicBody;
			case Type::Kinematic: return b2_kinematicBody;
			case Type::Static: return b2_staticBody;
			}
		}();

		def.fixedRotation = freezeRoation;
		def.userData = this;
		def.allowSleep = false;

		def.position.Set(localPosition.x, localPosition.y);
		def.angle = glm::radians(localRotation);
		def.linearVelocity.Set(velocity.x, velocity.y);

		body_ = World2D::create(&def);

		b2FixtureDef fix;
		fix.density = density;
		fix.friction = friction;

		for(Node* node : nodes())
		{
			if(Collider2D* collider = dynamic_cast<Collider2D*>(node))
			{
				auto shape = collider->shape();
				fix.shape = shape.get();
				body_->CreateFixture(&fix);
			}
		}
	}

	void Rigidbody2D::onInspectorDraw()
	{
		Node2D::onInspectorDraw();
		NOVA_NODE_INSPECTOR();

		int i = static_cast<int>(type);

		bool changed = false;
		changed |= Gui::combo("Type", &i, "Dynamic\0Kinematic\0Static\0");
		changed |= Gui::checkbox("Freeze Rotation", &freezeRoation);
		changed |= Gui::drag("Density", density);
		changed |= Gui::drag("Friction", friction);

		if(changed)
		{
			type = static_cast<Type>(i);

			dirty();
		}
	}

	void Rigidbody2D::serialize(Json::Value& json) const
	{
		Node2D::serialize(json);

		json["type"] = type;
		json["freeze_rotation"] = freezeRoation;
		json["density"] = density;
		json["friction"] = friction;
		json["velocity"] = velocity;
	}

	void Rigidbody2D::deserialize(const Json::Value& json)
	{
		Node2D::deserialize(json);

		json.at("type").get_to(type);
		json.at("freeze_rotation").get_to(freezeRoation);
		json.at("density").get_to(density);
		json.at("friction").get_to(friction);
		json.at("velocity").get_to(velocity);
	}

	void Rigidbody2D::preStep()
	{
		body_->SetTransform({ localPosition.x, localPosition.y }, glm::radians(localRotation));
		body_->SetLinearVelocity({ velocity.x, velocity.y });
	}

	void Rigidbody2D::postStep()
	{
		auto pos = body_->GetPosition();
		localPosition.x = pos.x;
		localPosition.y = pos.y;
		localRotation = glm::degrees(body_->GetAngle());

		auto vel = body_->GetLinearVelocity();
		velocity.x = vel.x;
		velocity.y = vel.y;
	}
}