#include "npch.h"
#include "nova/node/node.h"

namespace nova
{
	Node::~Node()
	{
		for(Node* node : nodes_)
		{
			destroy(node);
		}
	}

	Node* Node::get(const std::string& name)
	{
		for(Node* node : nodes_)
		{
			if(node->name() == name)
			{
				return node;
			}
		}

		return nullptr;
	}

	void Node::remove(Node* node)
	{
		for(auto i = nodes_.begin(); i != nodes_.end(); ++i)
		{
			if(*i == node)
			{
				node->onDestroy();
				scene_->destroy(node);

				nodes_.erase(i);
				break;
			}
		}
	}

	Node* Node::addByName(const std::string& name)
	{
		return addByName(name, name);
	}

	Node* Node::addByName(const std::string& registryName, const std::string& name)
	{
		Node* node = scene_->create(registryName);
		node->name_ = name;
		node->parent_ = this;

		node->onCreate();

		nodes_.push_back(node);
		return node;
	}

	void Node::onInspectorDraw()
	{
		Gui::text("%s", name_.c_str());
	}

	void Node::destroy(Node* node)
	{
		node->scene_->destroy(node);
	}
}