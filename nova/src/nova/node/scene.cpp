#include "npch.h"
#include "nova/node/scene.h"

#include "nova/core/time.h"
#include "nova/node/node.h"
#include "nova/node/node_registry.h"
#include "nova/physics/world.h"

namespace nova
{
	namespace
	{
		void recursive_add_node(const Json::Value& json, Node* node)
		{
			Node* n = node->addByName(
				json.at("registry_name").get<std::string>(),
				json.at("name").get<std::string>());

			n->deserialize(json.at("data").get<Json::Value>());
			auto& nodes = json.at("nodes").get<Json::Value>();

			for(auto& item : nodes)
			{
				recursive_add_node(item, n);
			}
		}

		void recursive_save_node(Json::Value& json, const Node* node)
		{
			json["name"] = node->name();
			json["registry_name"] = node->registryName();

			node->serialize(json["data"] = Json::Value::object());
			auto& nodes = json["nodes"] = Json::Value::array();

			for(Node* n : node->nodes())
			{
				Json::Value value{};
				recursive_save_node(value, n);
				nodes.push_back(value);
			}
		}
	}

	Scene::Scene()
	{
		root_ = create(Node::NODE_NAME);
		root_->name("root");
	}

	Scene::~Scene()
	{
		destroy(root_);
	}

	void Scene::load(const Json::Value& json)
	{
		NOVA_DIAG_FUNCTION();

		if(root_)
		{
			destroy(root_);
		}

		auto& root = json.at("root_node").get<Json::Value>();

		root_ = create(root.at("registry_name").get<std::string>());
		root_->name(root.at("name").get<std::string>());

		auto& nodes = root.at("nodes").get<Json::Value>();
		for(auto& item : nodes)
		{
			recursive_add_node(item, root_);
		}
	}

	Json::Value Scene::save() const
	{
		NOVA_DIAG_FUNCTION();

		auto json = Json::Value::object();
		recursive_save_node(json["root_node"] = Json::Value::object(), root_);
		return json;
	}

	void Scene::start()
	{
		NOVA_DIAG_FUNCTION();

		current_ = Time::time();
		accumulator_ = 0;

		storage_.each([](Node* node) { node->awake(); });
		storage_.each([](Node* node) { node->start(); });
	}

	void Scene::stop()
	{
		NOVA_DIAG_FUNCTION();

		storage_.each([](Node* node) { node->stop(); });
	}

	void Scene::update()
	{
		NOVA_DIAG_FUNCTION();

		size_t time = Time::time();
		double frameTime = (time - current_) / 1000.0;
		current_ = time;

		accumulator_ += frameTime;

		while(accumulator_ >= Time::fixedDeltaTime())
		{
			storage_.each([](Node* node) { node->fixedUpdate(); });
			World2D::step();

			accumulator_ -= Time::fixedDeltaTime();
		}

		storage_.each([](Node* node) { node->update(); });
		storage_.each([](Node* node) { node->lateUpdate(); });
	}

	void Scene::render()
	{
		NOVA_DIAG_FUNCTION();

		storage_.each([](Node* node) { node->render(); });
	}

	Node* Scene::create(const std::string& name)
	{
		NOVA_DIAG_FUNCTION();

		const auto& map = NodeRegistry::get();

		auto res = map.find(name);
		if(res != map.end())
		{
			Node* node = storage_.create(name, res->second);
			node->scene_ = this;
			return node;
		}

		nova_logger() << Log::Error << "Node does not exist: " << name << Log::Done;
		return nullptr;
	}

	void Scene::destroy(Node* node)
	{
		NOVA_DIAG_FUNCTION();

		storage_.destroy(node);
	}
}