#include "nova/node/node_registry.h"

#include "nova/core/log.h"
#include "nova/node/basic_nodes.h"
#include "nova/physics/physics_nodes.h"

namespace nova
{
	NodeRegistry::Registry NodeRegistry::map_;

	void register_nova_nodes()
	{
		NodeRegistry::registerNode<Node>();

		NodeRegistry::registerNode<Node2D>();
		NodeRegistry::registerNode<SpriteRenderer>();

		NodeRegistry::registerNode<BoxCollider2D>();
		NodeRegistry::registerNode<Rigidbody2D>();
	}
}