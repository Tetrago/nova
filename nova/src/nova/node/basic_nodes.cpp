#include "npch.h"
#include "nova/node/basic_nodes.h"

#include "nova/graphics/renderer.h"

namespace nova
{
	void Node2D::onInspectorDraw()
	{
		Node::onInspectorDraw();
		NOVA_NODE_INSPECTOR();

		Gui::drag("Position", localPosition);
		Gui::drag("Rotation", localRotation);
		Gui::drag("Scale", localScale);
	}

	void Node2D::serialize(Json::Value& json) const
	{
		Node::serialize(json);

		json["local_position"] = localPosition;
		json["local_rotation"] = localRotation;
		json["local_scale"] = localScale;
	}

	void Node2D::deserialize(const Json::Value& json)
	{
		Node::deserialize(json);

		json.at("local_position").get_to(localPosition);
		json.at("local_rotation").get_to(localRotation);
		json.at("local_scale").get_to(localScale);
	}

	glm::vec2 Node2D::position()
	{
		if(Node2D* p = dynamic_cast<Node2D*>(parent()))
		{
			glm::vec2 local = glm::quat({ 0, 0, glm::radians(localRotation) }) * glm::vec3(localPosition, 0);
			return p->position() + local * localScale;
		}

		return localPosition;
	}

	float Node2D::rotation()
	{
		if(Node2D* p = dynamic_cast<Node2D*>(parent()))
		{
			return p->rotation() + localRotation;
		}

		return localRotation;
	}

	glm::vec2 Node2D::scale()
	{
		if(Node2D* p = dynamic_cast<Node2D*>(parent()))
		{
			return p->scale() * localScale;
		}

		return localScale;
	}

	void SpriteRenderer::render()
	{
		Renderer2D::DrawDesc desc{};
		desc.position = glm::vec3(position(), 0);
		desc.rotation = glm::radians(rotation());
		desc.size = scale();
		desc.tiling = tiling;

		if(texture)
		{
			Renderer2D::drawQuad(*texture, color, desc);
		}
		else
		{
			Renderer2D::drawQuad(color, desc);
		}
	}

	void SpriteRenderer::onInspectorDraw()
	{
		Node2D::onInspectorDraw();
		NOVA_NODE_INSPECTOR();

		Gui::color("Color", color);
		Gui::drag("Tiling", tiling);
	}

	void SpriteRenderer::serialize(Json::Value& json) const
	{
		Node2D::serialize(json);

		json["color"] = color;
		json["tiling"] = tiling;
	}

	void SpriteRenderer::deserialize(const Json::Value& json)
	{
		static auto warning = []() { nova_logger() << Log::Warn << "Issue with texture serialization" << Log::Done; return 0; }();

		Node2D::deserialize(json);

		json.at("color").get_to(color);
		json.at("tiling").get_to(tiling);
	}
}