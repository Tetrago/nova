#include "npch.h"
#include "nova/node/node_storage.h"

#include "nova/core/log.h"
#include "nova/node/node.h"

namespace nova
{
	NodeStorage::NodeStorage()
		: pools_(make_box<std::unordered_map<std::string, Box<NodePool>>>())
	{}

	Node* NodeStorage::create(const std::string& name, Box<NodePool>(*create)())
	{
		if(!pools_->count(name))
		{
			pools_->emplace(name, create());
		}

		return (*pools_)[name]->create();
	}

	void NodeStorage::destroy(Node* node)
	{
		NOVA_CORE_ASSERT(pools_->count(node->registryName()), "Storage missing pool to destroy node on");
		(*pools_)[node->registryName()]->release(node);
	}

	void NodeStorage::each(void(*func)(Node*))
	{
		for(auto& [name, pool] : *pools_)
		{
			pool->each(func);
		}
	}
}