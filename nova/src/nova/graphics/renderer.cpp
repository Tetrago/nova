#include "npch.h"
#include "nova/graphics/renderer.h"

#include <numeric>
#include <array>
#include <glm/gtc/matrix_transform.hpp>

#include "nova/core/diagnostics.h"
#include "nova/core/program.h"
#include "nova/graphics/graphics_context.h"
#include "nova/graphics/buffer.h"
#include "nova/graphics/material.h"
#include "nova/graphics/pipeline.h"
#include "nova/graphics/sub_texture.h"
#include "nova/resource/shader.h"

namespace nova
{
	namespace
	{
		struct QuadVertex
		{
			glm::vec3 position;
			glm::vec2 uv;
			glm::vec4 color;
			glm::vec2 tiling;
			float tex;
		};

		struct Storage2D
		{
			Ref<Texture> whiteTexture;

			struct
			{
				static const uint32_t maxTextures = 32;
				static const uint32_t maxQuads = 10000;
				static const uint32_t maxVertices = maxQuads * 4;
				static const uint32_t maxIndices = maxQuads * 6;

				Ref<Buffer> vertex, index;
				Ref<Material> textureMaterial;

				std::array<const Texture*, maxTextures> textures;
				uint32_t textureIndex = 1;

				uint32_t drawCount = 0;
				QuadVertex* base = nullptr;
				QuadVertex* ptr = nullptr;
			} quad;

			Renderer2D::Statistics stats;
		};

		Storage2D* storage_2d;

		int find_texture(const SubTexture& texture)
		{
			NOVA_DIAG_FUNCTION();

			int index = 0;
			for(int i = 1; i < storage_2d->quad.textureIndex; ++i)
			{
				if(storage_2d->quad.textures[i] == &texture.base())
				{
					index = i;
					break;
				}
			}

			if(index == 0)
			{
				if(storage_2d->quad.textureIndex >= storage_2d->quad.maxTextures)
				{
					Renderer2D::flush();
				}

				index = storage_2d->quad.textureIndex++;
				storage_2d->quad.textures[index] = &texture.base();
			}

			return index;
		}

		void batch_quad(const glm::vec4& color, int tex, const Renderer2D::DrawDesc& desc, const std::array<glm::vec2, 4>& uv)
		{
			NOVA_DIAG_FUNCTION();

			if(storage_2d->quad.drawCount >= storage_2d->quad.maxIndices)
			{
				Renderer2D::flush();
			}

			glm::mat4 matrix = glm::translate(glm::mat4(1), desc.position)
				* glm::rotate(glm::mat4(1), desc.rotation, { 0, 0, 1 })
				* glm::scale(glm::mat4(1), { desc.size.x, desc.size.y, 1 });

			constexpr glm::vec4 verts[4]
			{
				{ -0.5f, -0.5f, 0, 1 },
				{ 0.5f, -0.5f, 0, 1 },
				{ 0.5f, 0.5f, 0, 1 },
				{ -0.5f, 0.5f, 0, 1 }
			};

			auto* ptr = storage_2d->quad.ptr;

			ptr->position = matrix * verts[0];
			ptr->uv = uv[0];
			ptr->color = color;
			ptr->tiling = desc.tiling;
			ptr->tex = tex;
			ptr++;

			ptr->position = matrix * verts[1];
			ptr->uv = uv[1];
			ptr->color = color;
			ptr->tiling = desc.tiling;
			ptr->tex = tex;
			ptr++;

			ptr->position = matrix * verts[2];
			ptr->uv = uv[2];
			ptr->color = color;
			ptr->tiling = desc.tiling;
			ptr->tex = tex;
			ptr++;

			ptr->position = matrix * verts[3];
			ptr->uv = uv[3];
			ptr->color = color;
			ptr->tiling = desc.tiling;
			ptr->tex = tex;
			ptr++;

			storage_2d->quad.ptr = ptr;
			storage_2d->quad.drawCount += 6;

			storage_2d->stats.quadCount++;
		}
	}

	std::stack<GraphicsContext*> RenderStack::contexts_;

	void RenderStack::push(GraphicsContext* ctx)
	{
		contexts_.push(ctx);
		ctx->use();
	}

	void RenderStack::pop()
	{
		contexts_.pop();

		if(contexts_.size() > 0)
		{
			contexts_.top()->use();
		}
	}

	void Renderer2D::prepair(const glm::mat4& matrix)
	{
		NOVA_DIAG_FUNCTION();

		RenderStack::top()->setPipeline(storage_2d->quad.textureMaterial->pipeline());
		storage_2d->quad.textureMaterial->set("viewProjection_", matrix);
	}

	void Renderer2D::flush()
	{
		NOVA_DIAG_FUNCTION();

		if(storage_2d->quad.drawCount == 0)
		{
			return;
		}

		size_t size = (uint8_t*)storage_2d->quad.ptr - (uint8_t*)storage_2d->quad.base;
		storage_2d->quad.vertex->load(storage_2d->quad.base, size);

		auto ctx = RenderStack::top();

		ctx->setPipeline(storage_2d->quad.textureMaterial->pipeline());

		ctx->setVertexBuffer(*storage_2d->quad.vertex);
		ctx->setIndexBuffer(*storage_2d->quad.index);

		ctx->applyTextures(storage_2d->quad.textures.data(), storage_2d->quad.textureIndex);
		ctx->drawIndexed(storage_2d->quad.drawCount);

		storage_2d->stats.drawCalls++;

		storage_2d->quad.ptr = storage_2d->quad.base;
		storage_2d->quad.drawCount = 0;
		storage_2d->quad.textureIndex = 1;
	}

	void Renderer2D::drawQuad(const glm::vec4& color, const DrawDesc& desc)
	{
		NOVA_DIAG_FUNCTION();

		constexpr std::array<glm::vec2, 4> uv
		{
			glm::vec2{ 0, 0 },
			glm::vec2{ 1, 0 },
			glm::vec2{ 1, 1 },
			glm::vec2{ 0, 1 }
		};

		batch_quad(color, 0, desc, uv);
	}

	void Renderer2D::drawQuad(const SubTexture& texture, const DrawDesc& desc)
	{
		NOVA_DIAG_FUNCTION();

		constexpr glm::vec4 white{ 1 };
		batch_quad(white, find_texture(texture), desc, texture.uv());
	}

	void Renderer2D::drawQuad(const SubTexture& texture, const glm::vec4& color, const DrawDesc& desc)
	{
		NOVA_DIAG_FUNCTION();

		batch_quad(color, find_texture(texture), desc, texture.uv());
	}

	void Renderer2D::mark()
	{
		memset(&storage_2d->stats, 0, sizeof(Renderer2D::Statistics));
	}

	const Renderer2D::Statistics& Renderer2D::stats()
	{
		return storage_2d->stats;
	}

	void Renderer2D::init()
	{
		NOVA_DIAG_FUNCTION();

#if defined(NOVA_DEBUG)
		static bool initialized = false;
		NOVA_CORE_ASSERT(!initialized, "Attempted to reinitialize renderer");
		initialized = true;
#endif

		storage_2d = new Storage2D{};

		auto& device = Program::get().renderDevice();

		{
			NOVA_DIAG_SCOPE("White Texture");

			uint8_t pixels[]{ 255, 255, 255, 255 };

			storage_2d->whiteTexture = device.createTexture();
			storage_2d->whiteTexture->create({ 1, 1, TextureFormat::Rgba8, pixels });

			storage_2d->quad.textures[0] = storage_2d->whiteTexture.get();
		}

		{
			NOVA_DIAG_SCOPE("Buffers");

			storage_2d->quad.ptr = storage_2d->quad.base = new QuadVertex[storage_2d->quad.maxVertices];

			storage_2d->quad.vertex = device.createBuffer({ nullptr, sizeof(QuadVertex) * storage_2d->quad.maxVertices, { BufferView::Array }, BufferUsage::Dynamic });

			uint32_t* quadIndices = new uint32_t[storage_2d->quad.maxIndices];

			uint32_t offset = 0;
			for(uint32_t i = 0; i < storage_2d->quad.maxIndices; i += 6)
			{
				quadIndices[i + 0] = offset + 0;
				quadIndices[i + 1] = offset + 1;
				quadIndices[i + 2] = offset + 2;

				quadIndices[i + 3] = offset + 0;
				quadIndices[i + 4] = offset + 2;
				quadIndices[i + 5] = offset + 3;

				offset += 4;
			}

			storage_2d->quad.index = device.createBuffer({ quadIndices, sizeof(uint32_t) * storage_2d->quad.maxIndices, { BufferView::ElementArray } });
			delete[] quadIndices;
		}

		{
			NOVA_DIAG_SCOPE("Material");

			PipelineCreateInfo createInfo{};
			createInfo.shader = ResourceManager::load<Shader>("assets/texture.shader");

			storage_2d->quad.textureMaterial = device.createMaterial(device.createPipeline(createInfo));

			int samplers[storage_2d->quad.maxTextures];
			std::iota(samplers, samplers + storage_2d->quad.maxTextures, 0);

			RenderStack::top()->setPipeline(storage_2d->quad.textureMaterial->pipeline());
			storage_2d->quad.textureMaterial->setTexture("tex_", samplers, storage_2d->quad.maxTextures);
		}
	}

	void Renderer2D::free()
	{
		NOVA_DIAG_FUNCTION();

		delete[] storage_2d->quad.base;
		delete storage_2d;
	}
}