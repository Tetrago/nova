#include "npch.h"
#include "nova/graphics/sub_texture.h"

namespace nova
{
	SubTexture::SubTexture(const Ref<Texture>& texture, const glm::vec2 uv0, const glm::vec2& uv1)
		: texture_(texture)
	{
		uv_[0] = uv0;
		uv_[1] = { uv1.x, uv0.y };
		uv_[2] = uv1;
		uv_[3] = { uv0.x, uv1.y };
	}
	

	Ref<SubTexture> SubTexture::from(const Ref<Texture>& texture, const glm::vec2& pos, const glm::vec2& size)
	{
		glm::vec2 dimensions{ texture->width(), texture->height() };

		glm::vec2 uv0 = pos / dimensions;
		glm::vec2 uv1 = (pos + size) / dimensions;

		return make_box<SubTexture>(texture, uv0, uv1);
	}
}