#include "npch.h"
#include "nova/graphics/gui.h"

#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

#include "nova/core/log.h"
#include "nova/core/platform.h"
#include "nova/core/program.h"

#if defined(NOVA_MODULE_OPENGL)
#include "gl/gl_texture.h"
#endif

namespace nova
{
	namespace
	{
		constexpr ImGuiWindowFlags build_window_flags(uint32_t flags)
		{
			ImGuiWindowFlags options = ImGuiWindowFlags_None;
			if(flags & Gui::NoCollapse)			options |= ImGuiWindowFlags_NoCollapse;
			if(flags & Gui::AutoResize)			options |= ImGuiWindowFlags_AlwaysAutoResize;
			if(flags & Gui::MenuBar)			options |= ImGuiWindowFlags_MenuBar;
			if(flags & Gui::NoDocking)			options |= ImGuiWindowFlags_NoDocking;
			if(flags & Gui::NoResize)			options |= ImGuiWindowFlags_NoResize;
			if(flags & Gui::NoMove)				options |= ImGuiWindowFlags_NoMove;
			if(flags & Gui::NoTitleBar)			options |= ImGuiWindowFlags_NoTitleBar;
			if(flags & Gui::NoNavFocus)			options |= ImGuiWindowFlags_NoNavFocus;
			if(flags & Gui::NoBringToFront)		options |= ImGuiWindowFlags_NoBringToFrontOnFocus;
			if(flags & Gui::NoScrollbar)		options |= ImGuiWindowFlags_NoScrollbar;

			return options;
		}

		constexpr ImGuiInputTextFlags build_input_text_flags(uint32_t flags)
		{
			ImGuiInputTextFlags options = ImGuiInputTextFlags_None;
			if(flags & Gui::ReadOnly)			options |= ImGuiInputTextFlags_ReadOnly;
			if(flags & Gui::Enter)				options |= ImGuiInputTextFlags_EnterReturnsTrue;

			return options;
		}

		constexpr ImGuiTreeNodeFlags build_tree_flags(uint32_t flags)
		{
			ImGuiTreeNodeFlags options = ImGuiTreeNodeFlags_None;
			if(flags & Gui::OpenOnArrow)		options |= ImGuiTreeNodeFlags_OpenOnArrow;
			if(flags & Gui::Selected)			options |= ImGuiTreeNodeFlags_Selected;
			if(flags & Gui::OpenOnDoubleClick)	options |= ImGuiTreeNodeFlags_OpenOnDoubleClick;
			if(flags & Gui::Leaf)				options |= ImGuiTreeNodeFlags_Leaf;

			return options;
		}

		constexpr ImGuiStyleVar find_var(const Gui::StyleVar& var)
		{
			switch(var)
			{
			case Gui::StyleVar::WindowPadding:		return ImGuiStyleVar_WindowPadding;
			case Gui::StyleVar::WindowRounding:		return ImGuiStyleVar_WindowRounding;
			case Gui::StyleVar::WindowBorderSize:	return ImGuiStyleVar_WindowBorderSize;
			case Gui::StyleVar::FramePadding:		return ImGuiStyleVar_FramePadding;
			case Gui::StyleVar::FrameRounding:		return ImGuiStyleVar_FrameRounding;
			case Gui::StyleVar::FrameBorderSize:	return ImGuiStyleVar_FrameBorderSize;
			}
		}

		constexpr ImGuiCond find_conditions(const Gui::Condition& condition)
		{
			switch(condition)
			{
			default:
			case Gui::Condition::None:		return ImGuiCond_None;
			case Gui::Condition::Always:	return ImGuiCond_Always;
			case Gui::Condition::First:		return ImGuiCond_FirstUseEver;
			}
		}
	}

	glm::vec2 Gui::Viewport::position() const
	{
		return { viewport_->Pos.x, viewport_->Pos.y };
	}

	glm::vec2 Gui::Viewport::size() const
	{
		return { viewport_->Size.x, viewport_->Size.y };
	}

	int Gui::Viewport::id() const
	{
		return viewport_->ID;
	}

	Gui::Viewport::Viewport(ImGuiViewport* viewport)
		: viewport_(viewport)
	{}

	void Gui::showDemoWindow(bool* v)
	{
		ImGui::ShowDemoWindow(v);
	}

	void Gui::setCursorPosition(float x, float y)
	{
		ImGui::SetCursorPos({ x, y });
	}

	glm::vec2 Gui::getCursorPosition()
	{
		auto pos = ImGui::GetCursorPos();
		return { pos.x, pos.y };
	}

	void Gui::setNextWindowPosition(float x, float y, float px, float py)
	{
		ImGui::SetNextWindowPos({ x, y }, ImGuiCond_None, { px, py });
	}

	void Gui::setNextWindowSize(float w, float h, Condition condition)
	{
		ImGui::SetNextWindowSize({ w, h }, find_conditions(condition));
	}

	void Gui::setNextWindowViewport(const Viewport& viewport)
	{
		ImGui::SetNextWindowViewport(viewport.id());
	}

	Gui::Viewport Gui::getMainViewport()
	{
		return Viewport(ImGui::GetMainViewport());
	}

	glm::vec2 Gui::getAvailableSpace()
	{
		auto size = ImGui::GetContentRegionAvail();
		return { size.x, size.y };
	}

	bool Gui::isFocused()
	{
		return ImGui::IsWindowFocused();
	}

	bool Gui::isHovered()
	{
		return ImGui::IsWindowHovered();
	}

	bool Gui::begin(const char* name, bool* open, uint32_t flags) { return ImGui::Begin(name, open, build_window_flags(flags)); }
	void Gui::end() { ImGui::End(); }

	bool Gui::beginChild(const char* name, float w, float h, bool border, uint32_t flags) { return ImGui::BeginChild(name, { w, h }, border, flags); }
	void Gui::endChild() { ImGui::EndChild(); }

	bool Gui::beginModal(const char* name, uint32_t flags) { return ImGui::BeginPopupModal(name, nullptr, build_window_flags(flags)); }
	bool Gui::beginPopup(const char* name, uint32_t flags) { return ImGui::BeginPopup(name, flags); }
	void Gui::endPopup() { ImGui::EndPopup(); }

	bool Gui::beginMenuBar() { return ImGui::BeginMenuBar(); }
	void Gui::endMenuBar() { ImGui::EndMenuBar(); }
	bool Gui::beginMenu(const char* name) { return ImGui::BeginMenu(name); }
	void Gui::endMenu() { ImGui::EndMenu(); }
	bool Gui::menuItem(const char* name, const char* shortcut, bool* selected) { return ImGui::MenuItem(name, shortcut, selected); }

	void Gui::pushStyleVar(const StyleVar& var, float a) { ImGui::PushStyleVar(find_var(var), a); }
	void Gui::pushStyleVar(const StyleVar& var, float a, float b) { ImGui::PushStyleVar(find_var(var), { a, b }); }
	void Gui::popStyleVar(int count) { ImGui::PopStyleVar(count); }

	void Gui::pushID(int id) { ImGui::PushID(id); }
	void Gui::pushID(const char* id) { ImGui::PushID(id); }
	void Gui::popID() { ImGui::PopID(); }

	bool Gui::collapsible(const char* name) { return ImGui::CollapsingHeader(name); }
	void Gui::separator() { ImGui::Separator(); }
	void Gui::sameLine() { ImGui::SameLine(); }
	void Gui::newLine() { return ImGui::NewLine(); }
	void Gui::indent() { ImGui::Indent(); }
	void Gui::unindent() { ImGui::Unindent(); }

	void Gui::openPopup(const char* name) { ImGui::OpenPopup(name); }
	void Gui::closePopup() { ImGui::CloseCurrentPopup(); }

	void Gui::dockSpace(const char* name) { ImGui::DockSpace(ImGui::GetID(name)); }

	bool Gui::combo(const char* label, int* i, const char* options) { return ImGui::Combo(label, i, options); }
	bool Gui::slider(const char* label, float* v, glm::vec2 range, const char* format) { return ImGui::SliderFloat(label, v, range.x, range.y, format); }
	bool Gui::drag(const char* label, float& v, float step) { return ImGui::DragFloat(label, &v, step); }
	bool Gui::drag(const char* label, glm::vec2& v, float step, float min, float max) { return ImGui::DragFloat2(label, glm::value_ptr(v), step, min, max); }
	bool Gui::drag(const char* label, glm::vec3& v, float step, float min, float max) { return ImGui::DragFloat3(label, glm::value_ptr(v), step, min, max); }
	bool Gui::input(const char* label, char* v, size_t size, uint32_t flags) { return ImGui::InputText(label, v, size, build_input_text_flags(flags)); }
	bool Gui::input(const char* label, float* v) { return ImGui::InputFloat(label, v); }
	bool Gui::color(const char* label, glm::vec4& v) { return ImGui::ColorEdit4(label, glm::value_ptr(v)); }

	void Gui::texture(const Ref<Texture>& texture, float w, float h, float scale)
	{
		void* target = nullptr;

		switch(Program::get().engineInfo().rendererApi)
		{
		default:
			nova_logger() << Log::Error << "Unsupported renderer api" << Log::Done;
			return;
#if defined(NOVA_MODULE_OPENGL)
		case RendererApi::OpenGL:
			target = reinterpret_cast<void*>(cast_ref<gl::GLTexture>(texture)->handle());
			break;
#endif
		}

		float factor = (1 - 1 / scale) * 0.5f;
		ImGui::Image(target, { w, h }, { factor, 1 - factor }, { 1 - factor, factor });
	}

	bool Gui::button(const char* name, float w, float h)
	{
		return ImGui::Button(name, { w, h });
	}

	void Gui::text(const char* fmt, ...)
	{
		va_list args;
		va_start(args, fmt);
		ImGui::TextV(fmt, args);
		va_end(args);
	}

	bool Gui::checkbox(const char* label, bool* v)
	{
		return ImGui::Checkbox(label, v);
	}

	bool Gui::selectable(const char* name, bool* v)
	{
		return ImGui::Selectable(name, v);
	}

	bool Gui::isItemClicked(int buttom)
	{
		return ImGui::IsItemClicked(buttom);
	}

	bool Gui::isMouseDown(int button)
	{
		return ImGui::IsMouseDown(button);
	}

	bool Gui::tree(const char* label, uint32_t flags)
	{
		return ImGui::TreeNodeEx(label, build_tree_flags(flags));
	}

	void Gui::popTree()
	{
		ImGui::TreePop();
	}

	std::optional<std::filesystem::path> Gui::openFile(const char* ext)
	{
		return Platform::get().openFileDialogue(Program::get().window(), ext);
	}

	std::optional<std::filesystem::path> Gui::saveFile(const char* ext)
	{
		return Platform::get().saveFileDialogue(Program::get().window(), ext);
	}
}