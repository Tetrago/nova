#include "npch.h"
#include "nova/graphics/render_device.h"

#include "nova/core/log.h"
#include "nova/core/program.h"

#if defined(NOVA_MODULE_OPENGL)
#include "gl/gl_render_device.h"
#endif

namespace nova
{
	Box<RenderDevice> RenderDevice::create()
	{
		switch(Program::get().engineInfo().rendererApi)
		{
		default:
			nova_logger() << Log::Fatal << "Unsupported renderer api" << Log::Done;
			break;
#if defined(NOVA_MODULE_OPENGL)
		case RendererApi::OpenGL:
			return make_box<gl::GLRenderDevice>();
#endif
		}
	}
}