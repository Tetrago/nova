#include "npch.h"
#include "nova/graphics/window.h"

#include "nova/core/base.h"
#include "nova/core/log.h"
#include "nova/core/program.h"
#include "nova/graphics/render_device.h"

#if defined(NOVA_MODULE_GLFW)
#include "platform/glfw.h"
#endif
#if defined(NOVA_MODULE_WIN32)
#include "platform/win32.h"
#endif

namespace nova
{
	GraphicsContext& Window::context()
	{
		if(!ctx_)
		{
			ctx_ = Program::get().renderDevice().createGraphicsContext(this);
		}

		return *ctx_;
	}

	Box<Window> Window::create(const WindowCreateInfo& createInfo)
	{
		switch(Program::get().engineInfo().platformApi)
		{
		default:
			nova_logger() << Log::Fatal << "Unsupported platform api" << Log::Done;
			break;
#if defined(NOVA_MODULE_GLFW)
		case PlatformApi::Glfw:
			nova_logger() << Log::Info << "Creating Glfw window" << Log::Done;
			return make_box<platform::GlfwWindow>(createInfo);
#endif
#if defined(NOVA_MODULE_WIN32)
		case PlatformApi::Win32:
			nova_logger() << Log::Info << "Creating win32 window" << Log::Done;
			return make_box<platform::Win32Window>(createInfo);
#endif
		}
	}
}