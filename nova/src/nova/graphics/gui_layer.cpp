#include "npch.h"
#include "nova/graphics/gui_layer.h"

#include "nova/core/program.h"
#include "nova/core/log.h"

#if defined(NOVA_MODULE_OPENGL)
#include "gl/gl_gui_layer.h"
#endif

namespace nova
{
	Ref<GuiLayer> GuiLayer::create()
	{
		switch(Program::get().engineInfo().rendererApi)
		{
		default:
			nova_logger() << Log::Fatal << "Unsupported renderer api" << Log::Done;
			break;
#if defined(NOVA_MODULE_OPENGL)
		case RendererApi::OpenGL:
			return make_ref<gl::GLGuiLayer>();
#endif
		}
	}
}