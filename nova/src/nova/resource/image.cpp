#include "npch.h"
#include "nova/resource/image.h"

#include <stb_image.h>

#include "nova/core/log.h"
#include "nova/core/program.h"

namespace nova
{
	namespace
	{
		const stbi_io_callbacks CALLBACKS = []()
		{
			stbi_io_callbacks callbacks{};

			callbacks.read = [](void* user, char* data, int size) -> int
			{
				std::istream* stream = static_cast<std::istream*>(user);

				stream->read(data, size);
				return stream->gcount();
			};

			callbacks.skip = [](void* user, int n)
			{
				static_cast<std::istream*>(user)->seekg(n, std::ios::cur);
			};

			callbacks.eof = [](void* user) -> int
			{
				return static_cast<std::istream*>(user)->eof();
			};

			return callbacks;
		}();
	}

	Image::Image(std::istream& stream)
	{
		NOVA_DIAG_FUNCTION();

#if defined(NOVA_INCLUDE_OPENGL)
		if(Program::get().engineInfo().rendererApi == RendererApi::OpenGL)
		{
			stbi_set_flip_vertically_on_load(true);
		}
#endif

		int width, height, channels;

		data_ = stbi_load_from_callbacks(&CALLBACKS, &stream, &width, &height, &channels, 0);
		if(!data_)
		{
			nova_logger() << Log::Error << "Failed to load image" << Log::Done;
		}

		width_ = width;
		height_ = height;
		channels_ = channels;
	}

	Image::~Image()
	{
		NOVA_DIAG_FUNCTION();

		stbi_image_free(data_);
	}
}