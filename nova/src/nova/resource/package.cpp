#include "npch.h"
#include "nova/resource/package.h"

#include "nova/core/log.h"

namespace nova
{
	void Package::unpack(std::istream& stream)
	{
		NOVA_DIAG_FUNCTION();

		data_.clear();

		std::string line;
		std::getline(stream, line);

		while(line.find_first_of(';') != std::string::npos)
		{
			auto comma = line.find_first_of(',');
			NOVA_CORE_ASSERT(comma != std::string::npos, "Invalid package");

			std::string key = line.substr(0, comma);
			size_t size = std::stoi(line.substr(comma + 1, line.find_first_of(';')));

			std::string contents{};
			contents.resize(size);

			stream.read(contents.data(), size);

			data_.emplace(key, contents);
			line = line.substr(line.find_first_of(';') + 1);
		}
	}

	std::string Package::pack() const
	{
		NOVA_DIAG_FUNCTION();

		std::ostringstream stream{};

		for(const auto& [key, content] : data_)
		{
			stream << key << ',' << content.size() << ';';
		}

		stream << '\n';

		for(const auto& pair : data_)
		{
			stream.write(pair.second.data(), pair.second.size());
		}

		return stream.str();
	}

	std::string& Package::operator[](const std::string& key)
	{
		return data_[key];
	}
}