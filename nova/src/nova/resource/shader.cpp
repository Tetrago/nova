#include "npch.h"
#include "nova/resource/shader.h"

#include "nova/core/log.h"

#include <spirv_glsl.hpp>

namespace nova
{
	namespace
	{
		inline Shader::Language find_language(const std::string& language)
		{
			if(language == "glsl")		return Shader::Language::Glsl;
			else if(language == "spv")	return Shader::Language::Spirv;
			else
			{
				nova_logger() << Log::Warn << "Invalid shader language in package" << Log::Done;
				return Shader::Language::Spirv;
			}
		}

		inline Shader::Stage find_stage(const std::string& stage)
		{
			if(stage == "vert")			return Shader::Stage::Vertex;
			else if(stage == "frag")	return Shader::Stage::Fragment;
			else
			{
				nova_logger() << Log::Warn << "Invalid shader stage in package" << Log::Done;
				return Shader::Stage::Vertex;
			}
		}

		std::vector<uint32_t> get_shader_binary(const std::string& code)
		{
			NOVA_DIAG_FUNCTION();

			std::vector<uint32_t> spv(code.size() / sizeof(uint32_t));
			memcpy(spv.data(), code.data(), code.size());
			return spv;
		}

		Shader::Group compile_glsl(const Shader::Group& spirv)
		{
			NOVA_DIAG_FUNCTION();

			Shader::Group group{};

			for(const auto& mod : spirv)
			{
				spirv_cross::CompilerGLSL compiler(std::move(get_shader_binary(mod.source)));

				spirv_cross::CompilerGLSL::Options options{};
				options.version = 420;
				compiler.set_common_options(options);

				group.push_back({ mod.stage, compiler.compile() });
			}

			return group;
		}
	}

	Shader::Shader(const Package& package)
	{
		NOVA_DIAG_FUNCTION();

		for(const auto& [name, data] : package)
		{
			Stage stage = find_stage(name.substr(name.find('.') + 1));
			Language language = find_language(name.substr(0, name.find('.')));

			groups_[language].push_back({ stage, data });
		}

		NOVA_CORE_ASSERT(groups_.find(Language::Spirv) != groups_.end(), "Shader package missing Spirv");

		populateStorage();
	}

	const Shader::Group& Shader::operator[](const Language& language)
	{
		NOVA_DIAG_FUNCTION();

		auto res = groups_.find(language);
		if(res == groups_.end())
		{
			return compile(language);
		}

		return res->second;
	}

	void Shader::populateStorage()
	{
		NOVA_DIAG_FUNCTION();

		for(const auto& [stage, code] : groups_[Language::Spirv])
		{
			spirv_cross::Compiler reflection(std::move(get_shader_binary(code)));
			spirv_cross::ShaderResources resources = reflection.get_shader_resources();

			if(stage == Stage::Vertex)
			{
				elements_.resize(resources.stage_inputs.size());

				for(const auto& input : resources.stage_inputs)
				{
					auto type = reflection.get_type(input.base_type_id);
					
					LayoutElement element{};
					element.name = input.name;
					element.count = type.vecsize;

					switch(type.basetype)
					{
					case type.Float:
						element.valueType = ValueType::Float;
						break;
					case type.Int:
						element.valueType = ValueType::Int;
						break;
					default:
						nova_logger() << Log::Warn << "Unkown shader input type" << Log::Done;
						break;
					}

					uint32_t location = reflection.get_decoration(input.id, spv::DecorationLocation);
					elements_[location] = element;
				}
			}
			
			for(const auto& resource : resources.uniform_buffers)
			{
				if(reflection.get_decoration(resource.id, spv::DecorationBinding) != 0) continue;

				const auto& type = reflection.get_type(resource.base_type_id);
				for(uint32_t i = 0; i < type.member_types.size(); ++i)
				{
					const std::string& name = reflection.get_member_name(type.self, i);
					uint32_t offset = reflection.type_struct_member_offset(type, i);
					size_t size = reflection.get_declared_struct_member_size(type, i);

					uniforms_.emplace(name, Uniform{ offset, size });
				}
			}

			for(const auto& resource : resources.sampled_images)
			{
				textures_[resource.name] = reflection.get_decoration(resource.id, spv::DecorationLocation);
			}
		}
	}

	const Shader::Group& Shader::compile(const Language& language)
	{
		NOVA_DIAG_FUNCTION();

		NOVA_CORE_ASSERT(language != Language::Spirv, "Attempting to compile to Spirv");
		
		switch(language)
		{
		default:
			nova_logger() << Log::Error << "Unsupported target shader language" << Log::Done;
			break;
		case Language::Glsl:
			return groups_[Language::Glsl] = compile_glsl(groups_[Language::Spirv]);
		}
	}
}