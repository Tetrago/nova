#include "npch.h"
#include "nova/resource/library.h"

#if defined(NOVA_PLATFORM_WIN)
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

#include "nova/core/diagnostics.h"
#include "nova/core/log.h"

namespace nova
{
	namespace
	{
		void* load_lib(const char* name)
		{
#if defined(NOVA_PLATFORM_WIN)
			return LoadLibrary(name);
#else
			return dlopen(name, RTLD_NOW | RTLD_LOCAL);
#endif
		}

		void free_lib(void* handle)
		{
#if defined(NOVA_PLATFORM_WIN)
			FreeLibrary(static_cast<HMODULE>(handle));
#else
			dlclose(handle);
#endif
		}

		bool is_library(const std::string& name, const std::filesystem::path& check)
		{
			if(check.stem().string() == name && check.extension() == ".dll")
			{
				return true;
			}
			else if(check.stem().string().find_first_of("lib") == 0 && check.extension() == ".so")
			{
				return true;
			}
			else if(check.extension() == ".dylib")
			{
				return true;
			}

			return false;
		}
	}

	Library::Library(const std::filesystem::path& dir, const std::string& name)
	{
		NOVA_DIAG_FUNCTION();

		for(auto& file : std::filesystem::directory_iterator(dir))
		{
			if(is_library(name, file))
			{
				path_ = file;
				break;
			}
		}

		NOVA_CORE_ASSERT(!path_.empty(), "Failed to find library: " << name);

		handle_ = load_lib(path_.string().c_str());
		NOVA_CORE_ASSERT(handle_, "Failed to load library: " << path_);
	}

	Library::~Library()
	{
		NOVA_DIAG_FUNCTION();

		free_lib(handle_);
	}

	void Library::reload()
	{
		NOVA_DIAG_FUNCTION();

		free_lib(handle_);

		handle_ = load_lib(path_.string().c_str());
		NOVA_CORE_ASSERT(handle_, "Failed to load library: " << path_);
	}

	void* Library::get(const std::string& name) const
	{
		NOVA_DIAG_FUNCTION();

#if defined(NOVA_PLATFORM_WIN)
		return GetProcAddress(static_cast<HMODULE>(handle_), name.c_str());
#else
		return dlsym(handle_, name.c_str());
#endif
	}
}