#include "npch.h"
#include "nova/resource/resource.h"

#include "nova/resource/package.h"

namespace nova
{
	Box<std::istream> AssetPipeline::resolve(const std::string& location)
	{
		for(const auto& resolver : resolvers_)
		{
			auto box = resolver->resolve(location);
			if(box)
			{
				return box;
			}
		}

		return Box<std::istream>();
	}

	FileResolver::FileResolver(const std::filesystem::path& base)
		: path_(base)
	{}

	Box<std::istream> FileResolver::resolve(const std::string& location)
	{
		if(std::filesystem::exists(path_ / location))
		{
			return make_box<std::ifstream>(path_ / location, std::ios::binary);
		}

		return Box<std::istream>();
	}

	PackageResolver::PackageResolver(ResourceID package)
		: package_(package)
	{}

	Box<std::istream> PackageResolver::resolve(const std::string& location)
	{
		Package* package = ResourceManager::get<Package>(package_);

		for(const auto& [where, what] : *package)
		{
			if(where == location)
			{
				return make_box<std::stringstream>(what);
			}
		}

		return Box<std::istream>();
	}
}