#include "npch.h"
#include "nova/resource/ini.h"

namespace nova
{
	namespace
	{
		inline std::string trim(const std::string& str)
		{
			auto begin = str.find_first_not_of(" \t");
			return str.substr(begin, str.find_last_not_of(" \t") - begin + 1);
		}
	}

	void Ini::load(std::istream& stream)
	{
		std::string section{};

		std::string line;
		while(std::getline(stream, line))
		{
			if(line.find('[') != std::string::npos)
			{
				auto begin = line.find('[');
				section = trim(line.substr(begin + 1, line.find(']') - begin - 1));
			}
			else if(!section.empty() && line.find('=') != std::string::npos)
			{
				std::string key = trim(line.substr(0, line.find('=')));
				std::string value = trim(line.substr(line.find('=') + 1));

				(*this)[section].set(key, value);
			}
		}
	}

	void Ini::save(std::ostream& stream)
	{
		for(const auto& [section, values] : map_)
		{
			stream << '[' << section << ']' << '\n';

			for(const auto& [key, value] : values)
			{
				stream << key << '=' << value << '\n';
			}

			stream << '\n';
		}
	}
}