#include "imgui_backend.h"

#include <unordered_map>
#include <imgui.h>

#include "nova/core/base.h"
#include "nova/core/diagnostics.h"
#include "nova/core/input.h"
#include "nova/core/platform.h"
#include "nova/core/program.h"
#include "nova/core/time.h"
#include "nova/graphics/graphics_context.h"
#include "nova/graphics/renderer.h"
#include "nova/graphics/window.h"

namespace nova
{
	namespace platform
	{
		namespace
		{
			bool buttonJustPressed_[ImGuiMouseButton_COUNT];

			void style_imgui()
			{
				ImGuiStyle* style = &ImGui::GetStyle();
				ImVec4* colors = style->Colors;

				colors[ImGuiCol_Text] = ImVec4(1.000f, 1.000f, 1.000f, 1.000f);
				colors[ImGuiCol_TextDisabled] = ImVec4(0.500f, 0.500f, 0.500f, 1.000f);
				colors[ImGuiCol_WindowBg] = ImVec4(0.180f, 0.180f, 0.180f, 1.000f);
				colors[ImGuiCol_ChildBg] = ImVec4(0.280f, 0.280f, 0.280f, 0.000f);
				colors[ImGuiCol_PopupBg] = ImVec4(0.313f, 0.313f, 0.313f, 1.000f);
				colors[ImGuiCol_Border] = ImVec4(0.266f, 0.266f, 0.266f, 1.000f);
				colors[ImGuiCol_BorderShadow] = ImVec4(0.000f, 0.000f, 0.000f, 0.000f);
				colors[ImGuiCol_FrameBg] = ImVec4(0.160f, 0.160f, 0.160f, 1.000f);
				colors[ImGuiCol_FrameBgHovered] = ImVec4(0.200f, 0.200f, 0.200f, 1.000f);
				colors[ImGuiCol_FrameBgActive] = ImVec4(0.280f, 0.280f, 0.280f, 1.000f);
				colors[ImGuiCol_TitleBg] = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
				colors[ImGuiCol_TitleBgActive] = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
				colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
				colors[ImGuiCol_MenuBarBg] = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
				colors[ImGuiCol_ScrollbarBg] = ImVec4(0.160f, 0.160f, 0.160f, 1.000f);
				colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.277f, 0.277f, 0.277f, 1.000f);
				colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.300f, 0.300f, 0.300f, 1.000f);
				colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_CheckMark] = ImVec4(1.000f, 1.000f, 1.000f, 1.000f);
				colors[ImGuiCol_SliderGrab] = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
				colors[ImGuiCol_SliderGrabActive] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_Button] = ImVec4(1.000f, 1.000f, 1.000f, 0.000f);
				colors[ImGuiCol_ButtonHovered] = ImVec4(1.000f, 1.000f, 1.000f, 0.156f);
				colors[ImGuiCol_ButtonActive] = ImVec4(1.000f, 1.000f, 1.000f, 0.391f);
				colors[ImGuiCol_Header] = ImVec4(0.313f, 0.313f, 0.313f, 1.000f);
				colors[ImGuiCol_HeaderHovered] = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
				colors[ImGuiCol_HeaderActive] = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
				colors[ImGuiCol_Separator] = colors[ImGuiCol_Border];
				colors[ImGuiCol_SeparatorHovered] = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
				colors[ImGuiCol_SeparatorActive] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_ResizeGrip] = ImVec4(1.000f, 1.000f, 1.000f, 0.250f);
				colors[ImGuiCol_ResizeGripHovered] = ImVec4(1.000f, 1.000f, 1.000f, 0.670f);
				colors[ImGuiCol_ResizeGripActive] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_Tab] = ImVec4(0.098f, 0.098f, 0.098f, 1.000f);
				colors[ImGuiCol_TabHovered] = ImVec4(0.352f, 0.352f, 0.352f, 1.000f);
				colors[ImGuiCol_TabActive] = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
				colors[ImGuiCol_TabUnfocused] = ImVec4(0.098f, 0.098f, 0.098f, 1.000f);
				colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
				colors[ImGuiCol_DockingPreview] = ImVec4(1.000f, 0.391f, 0.000f, 0.781f);
				colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.180f, 0.180f, 0.180f, 1.000f);
				colors[ImGuiCol_PlotLines] = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
				colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_PlotHistogram] = ImVec4(0.586f, 0.586f, 0.586f, 1.000f);
				colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_TextSelectedBg] = ImVec4(1.000f, 1.000f, 1.000f, 0.156f);
				colors[ImGuiCol_DragDropTarget] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_NavHighlight] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
				colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);
				colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);

				style->ChildRounding = 4.0f;
				style->FrameBorderSize = 1.0f;
				style->FrameRounding = 2.0f;
				style->GrabMinSize = 7.0f;
				style->PopupRounding = 2.0f;
				style->ScrollbarRounding = 12.0f;
				style->ScrollbarSize = 13.0f;
				style->TabBorderSize = 1.0f;
				style->TabRounding = 0.0f;
				style->WindowRounding = 4.0f;
			}

			struct Container
			{
				Box<Window> window_;
				bool buttons_[ImGuiMouseButton_COUNT]{};

				Container() {}
				Container(Box<Window>&& window)
					: window_(std::move(window))
				{}
			};

			Window& get_window(ImGuiViewport* viewport)
			{
				return *static_cast<Window*>(viewport->PlatformHandle);
			}

			void setup_platform_interface()
			{
				ImGuiPlatformIO& io = ImGui::GetPlatformIO();

				io.Platform_CreateWindow = [](ImGuiViewport* viewport)
				{
					WindowCreateInfo createInfo{};
					createInfo.x = viewport->Pos.x;
					createInfo.y = viewport->Pos.y;
					createInfo.width = viewport->Size.x;
					createInfo.height = viewport->Size.y;
					createInfo.resizable = true;
					createInfo.visible = false;
					createInfo.focused = false;
					createInfo.decorated = !(viewport->Flags & ImGuiViewportFlags_NoDecoration);
					createInfo.share = &Program::get().window();

					Container* container = new Container(std::move(Window::create(createInfo)));
					viewport->PlatformUserData = container;
					viewport->PlatformHandle = container->window_.get();
				};

				io.Platform_DestroyWindow = [](ImGuiViewport* viewport)
				{
					delete static_cast<Container*>(viewport->PlatformUserData);
					viewport->PlatformHandle = viewport->PlatformUserData = nullptr;
				};

				io.Platform_ShowWindow = [](ImGuiViewport* viewport) { get_window(viewport).visible(true); };

				io.Platform_SetWindowPos = [](ImGuiViewport* viewport, ImVec2 pos) { get_window(viewport).position(pos.x, pos.y); };
				io.Platform_GetWindowPos = [](ImGuiViewport* viewport) -> ImVec2
				{
					auto [x, y] = get_window(viewport).position();
					return { (float)x, (float)y };
				};

				io.Platform_SetWindowSize = [](ImGuiViewport* viewport, ImVec2 size) { get_window(viewport).size(size.x, size.y); };
				io.Platform_GetWindowSize = [](ImGuiViewport* viewport) -> ImVec2
				{
					auto [w, h] = get_window(viewport).size();
					return { (float)w, (float)h };
				};

				io.Platform_SetWindowFocus = [](ImGuiViewport* viewport) { get_window(viewport).focus(); };
				io.Platform_GetWindowFocus = [](ImGuiViewport* viewport) { return get_window(viewport).isFocused(); };

				io.Platform_GetWindowMinimized = [](ImGuiViewport* viewport)
				{
					auto [w, h] = get_window(viewport).size();
					return w == 0 || h == 0;
				};

				io.Platform_SetWindowTitle = [](ImGuiViewport* viewport, const char* title) { get_window(viewport).title(title); };
				io.Platform_RenderWindow = [](ImGuiViewport* viewport, void*) { get_window(viewport).context().use(); };

				io.Platform_SwapBuffers = [](ImGuiViewport* viewport, void*)
				{
					
#if defined(NOVA_MODULE_OPENGL)
					if(Program::get().engineInfo().rendererApi == RendererApi::OpenGL)
					{
						GraphicsContext& ctx = get_window(viewport).context();

						ctx.use();
						ctx.present();
					}
#endif
				};
			}

			void update_monitors(const std::vector<Platform::Monitor>& monitors)
			{
				ImGuiPlatformIO& platform = ImGui::GetPlatformIO();

				platform.Monitors.resize(0);
				for(auto& monitor : monitors)
				{
					ImGuiPlatformMonitor imguiMonitor{};

					imguiMonitor.MainPos = imguiMonitor.WorkPos = { (float)monitor.x, (float)monitor.y };
					imguiMonitor.MainSize = imguiMonitor.WorkSize = { (float)monitor.w, (float)monitor.h };

					if(monitor.workarea.w > 0 && monitor.workarea.h > 0)
					{
						imguiMonitor.WorkPos = { (float)monitor.workarea.x, (float)monitor.workarea.y };
						imguiMonitor.WorkSize = { (float)monitor.workarea.w, (float)monitor.workarea.h };
					}

					imguiMonitor.DpiScale = monitor.dpi;

					platform.Monitors.push_back(imguiMonitor);
				}
			}
		}

		ImGuiBackend::ImGuiBackend()
			: clipboard_(nullptr)
		{
			NOVA_DIAG_FUNCTION();

			IMGUI_CHECKVERSION();
			ImGui::CreateContext();

			ImGuiIO& io = ImGui::GetIO();
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
			io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

			ImGui::StyleColorsDark();
			style_imgui();

			io.BackendFlags |= ImGuiBackendFlags_PlatformHasViewports;
			io.BackendPlatformName = "imgui_impl_nova";

#define MAP_KEY(x, y) io.KeyMap[x] = static_cast<int>(y);
			MAP_KEY(ImGuiKey_Tab, KeyCode::Tab);
			MAP_KEY(ImGuiKey_LeftArrow, KeyCode::Left);
			MAP_KEY(ImGuiKey_RightArrow, KeyCode::Right);
			MAP_KEY(ImGuiKey_UpArrow, KeyCode::Up);
			MAP_KEY(ImGuiKey_DownArrow, KeyCode::Down);
			MAP_KEY(ImGuiKey_PageUp, KeyCode::PageUp);
			MAP_KEY(ImGuiKey_PageDown, KeyCode::PageDown);
			MAP_KEY(ImGuiKey_Home, KeyCode::Home);
			MAP_KEY(ImGuiKey_End, KeyCode::End);
			MAP_KEY(ImGuiKey_Insert, KeyCode::Insert);
			MAP_KEY(ImGuiKey_Delete, KeyCode::Delete);
			MAP_KEY(ImGuiKey_Backspace, KeyCode::Backspace);
			MAP_KEY(ImGuiKey_Space, KeyCode::Space);
			MAP_KEY(ImGuiKey_Enter, KeyCode::Enter);
			MAP_KEY(ImGuiKey_Escape, KeyCode::Escape);
			MAP_KEY(ImGuiKey_KeyPadEnter, KeyCode::KeypadEnter);
			MAP_KEY(ImGuiKey_A, KeyCode::A);
			MAP_KEY(ImGuiKey_C, KeyCode::C);
			MAP_KEY(ImGuiKey_V, KeyCode::V);
			MAP_KEY(ImGuiKey_X, KeyCode::X);
			MAP_KEY(ImGuiKey_Y, KeyCode::Y);
			MAP_KEY(ImGuiKey_Z, KeyCode::Z);
#undef MAP_KEY

			io.SetClipboardTextFn = [](void* user, const char* text) { static_cast<ImGuiBackend*>(user)->clipboard_ = text; };
			io.GetClipboardTextFn = [](void* user) { return static_cast<ImGuiBackend*>(user)->clipboard_; };
			io.ClipboardUserData = this;

			ImGuiViewport* viewport = ImGui::GetMainViewport();

			viewport->PlatformUserData = new Container();
			viewport->PlatformHandle = &Program::get().window();

			EventBus::subscribe<WindowEvent>([](const WindowEvent& e)
				{
					if(ImGuiViewport* viewport = ImGui::FindViewportByPlatformHandle(e.window))
					{
						ImGuiIO& io = ImGui::GetIO();

						switch(e.type)
						{
						case WindowEvent::Type::Button:
							if(e.button.button < IM_ARRAYSIZE(io.MouseDown))
							{
								if(e.button.status)
								{
									buttonJustPressed_[e.button.button] = true;
								}

								static_cast<Container*>(viewport->PlatformUserData)->buttons_[e.button.button] = e.button.status;
							}
							break;
						case WindowEvent::Type::Scroll:
							io.MouseWheel += e.scroll.delta;
							break;
						case WindowEvent::Type::Key:
							io.KeysDown[static_cast<int>(e.key.key)] = e.key.status;

							io.KeyCtrl = io.KeysDown[(int)KeyCode::LeftControl] || io.KeysDown[(int)KeyCode::RightControl];
							io.KeyShift = io.KeysDown[(int)KeyCode::LeftShift] || io.KeysDown[(int)KeyCode::RightShift];
							io.KeyAlt = io.KeysDown[(int)KeyCode::LeftAlt] || io.KeysDown[(int)KeyCode::RightAlt];
							io.KeySuper = io.KeysDown[(int)KeyCode::LeftSuper] || io.KeysDown[(int)KeyCode::RightSuper];
							break;
						case WindowEvent::Type::Character:
							io.AddInputCharacter(e.character.c);
							break;
						}

						if(static_cast<Container*>(viewport->PlatformUserData)->window_)	// If this isn't the main viewport.
						{
							switch(e.type)
							{
							case WindowEvent::Type::Close:
								viewport->PlatformRequestClose = true;
								break;
							case WindowEvent::Type::Move:
								viewport->PlatformRequestMove = true;
								break;
							case WindowEvent::Type::Resize:
								viewport->PlatformRequestResize = true;
								break;
							}
						}
					}
				});

			if(io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
			{
				setup_platform_interface();
			}

			EventBus::subscribe<Platform::MonitorEvent>([](const Platform::MonitorEvent& e)
				{
					update_monitors(e.monitors);
				});

			update_monitors(Platform::get().monitors());
		}

		ImGuiBackend::~ImGuiBackend()
		{
			NOVA_DIAG_FUNCTION();

			ImGui::DestroyContext();
		}

		void ImGuiBackend::newFrame()
		{
			NOVA_DIAG_FUNCTION();

			ImGuiIO& io = ImGui::GetIO();

			auto& window = Program::get().window();
			auto [w, h] = window.size();

			io.DisplaySize = { (float)w, (float)h };
			io.DisplayFramebufferScale = { 1, 1 };
			io.DeltaTime = Time::deltaTime();

			updateMouse();
		}

		void ImGuiBackend::updateViewports()
		{
			NOVA_DIAG_FUNCTION();

			if(ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
			{
				ImGui::UpdatePlatformWindows();
				ImGui::RenderPlatformWindowsDefault();
				RenderStack::top()->use();
			}
		}

		void ImGuiBackend::updateMouse()
		{
			ImGuiIO& io = ImGui::GetIO();
			const Mouse& mouse = Mouse::get();

			Container* container = static_cast<Container*>(ImGui::GetMainViewport()->PlatformUserData);
			for(int i = 0; i < IM_ARRAYSIZE(io.MouseDown); ++i)
			{
				io.MouseDown[i] = buttonJustPressed_[i] || container->buttons_[i];
				buttonJustPressed_[i] = false;
			}

			io.MouseHoveredViewport = 0;

			ImGuiPlatformIO& platform = ImGui::GetPlatformIO();
			for(int i = 0; i < platform.Viewports.Size; ++i)
			{
				ImGuiViewport* viewport = platform.Viewports[i];
				Window& window = get_window(viewport);

				if(window.isFocused())
				{
					if(io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
					{
						auto [x, y] = window.position();
						io.MousePos = { x + (float)mouse.x(), y + (float)mouse.y() };
					}
					else
					{
						io.MousePos = { (float)mouse.x(), (float)mouse.y() };
					}

					container = static_cast<Container*>(viewport->PlatformUserData);
					for(int i = 0; i < IM_ARRAYSIZE(io.MouseDown); ++i)
					{
						io.MouseDown[i] |= container->buttons_[i];
					}
				}

				if(window.isHovered() && !(viewport->Flags & ImGuiViewportFlags_NoInputs))
				{
					io.MouseHoveredViewport = viewport->ID;
				}
			}
		}
	}
}