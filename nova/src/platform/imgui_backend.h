#pragma once

namespace nova
{
	namespace platform
	{
		class ImGuiBackend
		{
		public:
			ImGuiBackend();
			~ImGuiBackend();

			ImGuiBackend(const ImGuiBackend&) = delete;
			ImGuiBackend& operator=(const ImGuiBackend&&) = delete;
			ImGuiBackend(ImGuiBackend&&) = delete;
			ImGuiBackend& operator=(ImGuiBackend&&) = delete;

			void newFrame();
			void updateViewports();
		private:
			void updateMouse();

			const char* clipboard_;
		};
	}
}