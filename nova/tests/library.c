#if defined(_WIN32)
#define API __declspec(dllexport)
#elif defined(__linux__)
#define API __attribute__((visibility("default")))
#else
#error "Unknown platform"
#endif

API int get_test_value()
{
	return 5;
}