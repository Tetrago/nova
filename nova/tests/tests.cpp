#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <nova/nova.h>

using namespace nova;

SCENARIO("Librarys can be dynamically loaded", "[library]")
{
	using getTestValueFn = int(*)();

	GIVEN("A loaded library")
	{
		Library lib{ ".", "nova_tests_library" };

		WHEN("A proc address is retrieved")
		{
			getTestValueFn func = lib.get<getTestValueFn>("get_test_value");

			THEN("The call is successful")
			{
				REQUIRE((*func)() == 5);
			}
		}
	}
}

SCENARIO("Events are working properly", "[event]")
{
	GIVEN("An event type and listener.")
	{
		struct TestEvent {};

		bool triggered = false;
		Event<TestEvent>::listen([&](const TestEvent& e)
			{
				triggered = true;
			});

		WHEN("An event is emitted")
		{
			Event<TestEvent>::emit({ });

			THEN("The callback will be executed")
			{
				REQUIRE(triggered);
			}
		}
	}
}

SCENARIO("Packages are working properly", "[package]")
{
	GIVEN("A package with data")
	{
		Package package{};

		std::stringstream stream{ "file1,5;\nwords" };
		package.unpack(stream);

		REQUIRE(package["file1"] == "words");

		WHEN("Files are added")
		{
			package["file1"] = "characters";
			package["file2"] = "chars";

			THEN("They can be read")
			{
				REQUIRE(package["file1"] == "characters");
				REQUIRE(package["file2"] == "chars");
			}
		}

		WHEN("Files are packed")
		{
			std::string packed = package.pack();

			THEN("It is correct")
			{
				REQUIRE(packed == "file1,5;\nwords");
			}
		}
	}
}

SCENARIO("Ini are working properly", "[ini]")
{
	GIVEN("An ini with data")
	{
		Ini ini{};

		std::stringstream stream{ "[ group\t ] \n  val =\t3" };
		ini.load(stream);

		REQUIRE(ini.has("group"));
		REQUIRE(ini["group"].has("val"));

		WHEN("A section and value is added")
		{
			ini["new_section"].set("a_value", 3);

			REQUIRE(ini.has("new_section"));
			REQUIRE(ini["new_section"].has("a_value"));

			THEN("The value can be read")
			{
				REQUIRE(ini["new_section"].get<int>("a_value") == 3);
			}
		}

		WHEN("It is read")
		{
			std::ostringstream stream;
			ini.save(stream);

			THEN("It is correct")
			{
				REQUIRE(stream.str() == "[group]\nval=3\n\n");
			}
		}
	}
}

namespace
{
	constexpr const char* stringify(const Log& log)
	{
		switch(log)
		{
		default:
		case Log::Warn:		return "[warn ] ";
		case Log::Trace:	return "[trace] ";
		case Log::Info:		return "[info ] ";
		case Log::Error:	return "[err  ] ";
		case Log::Fatal:	return "[fatal] ";
		}
	}
}

int main(int argc, char** argv)
{
	Event<LogEvent>::listen([](const LogEvent& e)
		{ std::cout << stringify(e.log) << "(" << e.name << ") " << e.msg << '\n'; });

	return Catch::Session().run(argc, argv);
}