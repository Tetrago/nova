#include <nova/nova.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class OrthographicCamera
{
public:
	OrthographicCamera(float speed)
		: speed_(speed)
		, view_(1)
	{
		auto& wnd = nova::Program::get().window();
		aspectRatio_ = (float)wnd.width() / wnd.height();
		recalculate();

		nova::Event<nova::WindowEvent>::listen([&](const nova::WindowEvent& e)
			{
				if(e.window == &nova::Program::get().window() && e.type == nova::WindowEvent::Type::Resize)
				{
					aspectRatio_ = (float)e.resize.width / e.resize.height;
					recalculate();
				}
			});
	}

	void update()
	{
		auto& keyboard = nova::Keyboard::get();
		auto dt = nova::Time::deltaTime();

		if(keyboard.isKeyDown(nova::KeyCode::W))
		{
			pos_.y += speed_ * dt;
		}
		else if(keyboard.isKeyDown(nova::KeyCode::S))
		{
			pos_.y -= speed_ * dt;
		}

		if(keyboard.isKeyDown(nova::KeyCode::A))
		{
			pos_.x -= speed_ * dt;
		}
		else if(keyboard.isKeyDown(nova::KeyCode::D))
		{
			pos_.x += speed_ * dt;
		}

		float delta = nova::Mouse::get().scrollDelta();
		if(delta != 0)
		{
			zoom_ -= delta * speed_ * dt;
			recalculate();
		}

		view_ = glm::inverse(glm::translate(glm::mat4(1), pos_));
	}

	glm::mat4 matrix() const
	{
		return projection_ * view_;
	}
private:
	void recalculate()
	{
		projection_ = glm::ortho(-aspectRatio_ * zoom_, aspectRatio_ * zoom_, -zoom_, zoom_);
	}

	glm::mat4 projection_, view_;
	glm::vec3 pos_{};

	float speed_;
	float aspectRatio_;
	float zoom_ = 1;
};

class BasicLayer : public nova::Layer
{
public:
	BasicLayer()
		: Layer("Basic")
		, camera_(2)
	{
		NOVA_DIAG_FUNCTION();
		auto& renderDevice = nova::Program::get().renderDevice();

		{
			NOVA_DIAG_SCOPE("Texture Creation");

			texture_ = renderDevice.createTexture();
			texture_->load(nova::ResourceManager::load<nova::Image>(nova::ResourceLocation("res/img.png")));
		}

		{
			NOVA_DIAG_SCOPE("Pipeline Creation");

			nova::PipelineCreateInfo createInfo{};
			createInfo.layoutElements =
			{
				{ "position_", nova::ValueType::Float2, false },
				{ "uv_", nova::ValueType::Float2, false },
				{ "color_", nova::ValueType::Float3, false }
			};
			createInfo.source = nova::ResourceManager::load<nova::Shader>(nova::ResourceLocation("res/shader.nsc"));

			material_ = renderDevice.createMaterial(renderDevice.createPipeline(createInfo));
			material_->set("u_tex", 0);
		}

		{
			NOVA_DIAG_SCOPE("Buffer Creation");

			float data[]
			{
				-0.5f, -0.5f, 0, 0, 1, 0, 0,
				0.5f, -0.5f, 1, 0, 0, 1, 0,
				0.5f, 0.5f, 1, 1, 0, 0, 1,
				-0.5f, 0.5f, 0, 1, 1, 1, 0
			};

			uint32_t indices[]{ 0, 1, 2, 0, 2, 3 };

			vertex_ = renderDevice.createBuffer({ sizeof(data), { nova::BufferView::Array } });
			vertex_->load(data);

			index_ = renderDevice.createBuffer({ sizeof(indices), { nova::BufferView::ElementArray } });
			index_->load(indices);
		}
	}

	void attach() override
	{
		
	}

	void detach() override
	{
		
	}

	void update() override
	{
		NOVA_DIAG_FUNCTION();

		camera_.update();
		material_->set("u_vp", camera_.matrix());

		auto& ctx = nova::Program::get().window().context();

		ctx.applyTexture(texture_);
		
		ctx.setPipeline(material_->pipeline());
		ctx.setVertexBuffer(vertex_);
		ctx.setIndexBuffer(index_);

		ctx.drawIndexed(6);
	}

	void onGui() override
	{
		static bool v = true;
		if(v) nova::Gui::showDemoWindow(&v);

		static bool open = true;
		if(open)
		{
			if(nova::Gui::begin("Program", &open))
			{
				if(nova::Gui::collapsible("Information"))
				{
					nova::Gui::text("Frame Rate: %d", nova::Time::frameRate());
					nova::Gui::text("Frame Time: %.3f ms", nova::Time::deltaTime());
				}
			}

			nova::Gui::end();
		}
	}
private:
	nova::Ref<nova::Material> material_;
	nova::Ref<nova::Buffer> vertex_, index_;
	nova::Ref<nova::Texture> texture_;
	OrthographicCamera camera_;
};

class Supernova : public nova::Program
{
public:
	Supernova() : Program({ nova::PlatformApi::Glfw, nova::RendererApi::OpenGL, "Supernova", { 0, 1, 0 } })
	{
		nova::WindowCreateInfo createInfo{};
		createInfo.resizable = true;

		use(nova::Window::create(createInfo));

		layers().push(nova::make_ref<BasicLayer>());
		layers().push(nova::GuiLayer::create(), true);
	}
};

NOVA_PROVIDE_PROGRAM(Supernova);